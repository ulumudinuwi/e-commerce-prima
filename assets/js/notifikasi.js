$(() => {
    let _SHOW_COUNT = 5,
        _PAGE_ = 1,
        _BADGE_NUMBER_ = 0;

    function setHeader(xhr) {
        xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
    }
    /** Untuk Membuat Element jQuery **/
    let getJqMessage = (msg) => {
        let card = $("<div/>")
            .data('id', msg.id)
            .data('read_flag', msg.read_flag)
            .addClass('card p-1')
            let cardBody = $("<div/>")
                .addClass('mb-0 card-body')
                .addClass(msg.read_flag == 1 ? '' : 'bg-soft-danger')
                .appendTo(card);
                let link = $("<a/>")
                    .prop('href', msg.link ? siteUrl + msg.link : '#')
                    .addClass('text-primary')
                    .html(`<h5 class='mb-0'>${msg.title}</h5>
                               ${msg.description}<br/>
                               <small class="text-muted">
                                  ${msg.created_at}  
                              </small>`)
                    .appendTo(cardBody);

        card.on('read', () => {
            read(msg.id,
                (res) => {
                    card.off('mouseenter');
                    card.data('read_flag', 1);
                    _BADGE_NUMBER_ -= 1;
                    updateBadgeNumber(_BADGE_NUMBER_);
                }, 
                (error) => {
                    //
                });
        });
        
        if (msg.read_flag == 1) {
            //
        } else {
            card.on('mouseenter', () => {
                card.trigger('read');
            });
        }

        return card;
    }

    let fetch = () => {
        $.ajax({
            url: url.getNotifikasi + `?id=${member_id}&page=${_PAGE_}&limit=${_SHOW_COUNT}`,
            type: 'GET',
            dataType: "json",
            beforeSend: setHeader,
            success: function (res) {
                _BADGE_NUMBER_ = res.badge;
                updateBadgeNumber(_BADGE_NUMBER_);

                let list = $(NOTIF_DROPDOWN).find(NOTIF_CONTENT);
                if(_PAGE_ == 1) list.empty();
                for (let msg of res.data) {
                    getJqMessage(msg)
                        .appendTo(list);
                }

                if (_PAGE_ == 1 && res.data.length == 0) {
                    list.html(`
                        <div class="notif-empty mb-3 text-center">
                            <span class="btn btn-icon btn-soft-primary rounded-circle mb-3">
                                <span class="fas fa-bell-slash btn-icon__inner"></span>
                            </span>
                            <span class="d-block">You don't have any notifications</span>
                        </div>
                    `);
                }
            },
            error: function (error) {
                console.log('error');
            }
        });
    };

    let read = (id, onSuccess, onError) => {
        $.ajax({
            url: url.readNotifikasi,
            type: 'POST',
            dataType: "json",
            data: {
                id: id
            },
            beforeSend: setHeader,
            success: function (res) {
                if (onSuccess) {
                    onSuccess(res);
                }
            },
            error: function (error) {
                if (onError) {
                    onError(error);
                }
            },
            complete: function () {
                // ....
            }
        });
    }

    $(NOTIF_DROPDOWN).find('.notif-more').click((e) => {
        e.preventDefault();
        _PAGE_ += 1;
        fetch();
    });

    $(NOTIF_DROPDOWN).find('[data-toggle="readall"]').click((e) => {
        e.preventDefault();

        $.ajax({
            url: url.readNotifikasiAll,
            type: 'POST',
            dataType: "json",
            data: {},
            beforeSend: setHeader,
            success: function (res) {
                let list = $(NOTIF_DROPDOWN).find(NOTIF_CONTENT);

                list.find('.card').each((x, media) => {
                    $(media).data('read_flag', 1);
                    $(media).off('mouseenter');
                });
                _BADGE_NUMBER_ = 0;
                updateBadgeNumber(_BADGE_NUMBER_);
            },
            error: function (error) {
                // 
            },
            complete: function () {
                // ....
            },
        });
    });

    let listen = (q) => {
        eventSource = new EventSource(url.listenNotifikasi.replace(':UID', q));
        eventSource.addEventListener('notifikasi_event', function(e) {
            _PAGE_ = 1;
            fetch();
        }, false);
    }

    let updateBadgeNumber = (badge) => {
        if(badge) {
            $('.badgeNotif').removeClass('d-none');
            $('.badgeNotif').html(badge);
        } else {
            if(!$('.badgeNotif').hasClass('d-none'))
                $('.badgeNotif').addClass('d-none');
        }
    }

    if(userdata) listen(btoa(member_id));
});