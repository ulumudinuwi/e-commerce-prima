<script type="text/javascript">
  var userdata = JSON.parse(window.localStorage.getItem('prima-userdata')),
      token = JSON.parse(window.localStorage.getItem('prima-token')),
      member_id   = '',
      offset   = 0,
      form = $('#form'),
      formProfile = $('#formProfile'),
      formAddress = $('#formAddress'),
      formPassword = $('#formPassword'),
      totalHarga = 0,
      base_url = '<?php echo base_url() ?>',
      site_url = '<?php echo site_url() ?>',
      api_base_uri = '<?php echo $this->config->item('api_base_uri')?>',
      dataUrl = {};

  let NOTIF_DROPDOWN =  '#notifDropdown',
      NOTIF_CONTENT =  '.notif-content';

  let url = {
    getKelompokbarang: "<?php echo $this->config->item('api_uri').'/v1/gudang_farmasi/kelompok_barang/list' ?>",
    getImageKelompokBarang: "<?php echo $this->config->item('api_base_uri').'/uploads/kelompok_barang/' ?>",
    getPromo: "<?php echo $this->config->item('api_uri').'/v1/promo/list' ?>",
    login: "<?php echo $this->config->item('api_uri').'/v1/auth/login' ?>",
    getCart: "<?php echo $this->config->item('api_uri').'/v1/transaksi/add_tocart/list' ?>",
    deleteCart: "<?php echo $this->config->item('api_uri').'/v1/transaksi/add_tocart/delete' ?>",
    getPromo: "<?php echo $this->config->item('api_uri').'/v1/promo/list' ?>",
    urlImage : '<?php echo $this->config->item('api_base_uri')?>'+'/uploads/kelompok_barang/',
    getProduct: "<?php echo isset($url) ? $url : $this->config->item('api_uri').'/v1/gudang_farmasi/barang/list' ?>",
    saveProfile: "<?php echo $this->config->item('api_uri').'/v1/auth/akun/change_profile' ?>",
    saveAddress: "<?php echo $this->config->item('api_uri').'/v1/auth/akun/change_address' ?>",
    savePassword: "<?php echo $this->config->item('api_uri').'/v1/auth/akun/change_password' ?>",
    getProfile: "<?php echo $this->config->item('api_uri').'/v1/auth/akun/profile_detail' ?>",
    addCheckout: "<?php echo $this->config->item('api_uri').'/v1/transaksi/checkout/insert' ?>",
    getCheckout: "<?php echo $this->config->item('api_uri').'/v1/transaksi/checkout/list' ?>",
    listenCheckout: "<?php echo $this->config->item('api_uri').'/v1/event/checkout/listen?uid=:UID' ?>",
    getCheckoutBadge: "<?php echo $this->config->item('api_uri').'/v1/transaksi/checkout/list_badge' ?>",
    deleteCheckout: "<?php echo $this->config->item('api_uri').'/v1/transaksi/checkout/delete_checkout' ?>",
    saveCheckout: "<?php echo $this->config->item('api_uri').'/v1/transaksi/checkout/upload_image' ?>",
    getCheckoutDetail: "<?php echo $this->config->item('api_uri').'/v1/transaksi/checkout/list_detail' ?>",
    orderReceived: "<?php echo $this->config->item('api_uri').'/v1/transaksi/checkout/order_received' ?>",
    getNotifikasi: "<?php echo $this->config->item('api_uri').'/v1/notifikasi/list' ?>",
    readNotifikasi: "<?php echo $this->config->item('api_uri').'/v1/notifikasi/read' ?>",
    readAllNotifikasi: "<?php echo $this->config->item('api_uri').'/v1/notifikasi/read_all' ?>",
    listenNotifikasi: "<?php echo $this->config->item('api_uri').'/v1/event/notifikasi/listen?uid=:UID' ?>",

    //sales
    getSales: "<?php echo $this->config->item('api_uri').'/v1/pos/sales/list_sales' ?>",
    registermember: "<?php echo $this->config->item('api_uri').'/v1/auth/akun/register' ?>",

    //alamat
    getProvince : "https://pro.rajaongkir.com/api/province",
    getCity : "https://pro.rajaongkir.com/api/city",
    getCost : "https://pro.rajaongkir.com/api/cost",
    getSubdistrict : "https://pro.rajaongkir.com/api/subdistrict?city=39",
    getWaybill : "https://pro.rajaongkir.com/api/waybill",
  };

  /*function listenNotifikasi(q) {
    eventSource = new EventSource(url.listenNotifikasi.replace(':UID', q));
    eventSource.addEventListener('notifikasi_event', function(e) {
      getNotifikasi();
    }, false);
  }*/

  $(document).ready(function() {
    $('.appendCart').addClass('text-center');
    if(userdata){
      member_id = userdata.dokter_id;
      $('#member_id').val(userdata.dokter_id);

      // Notifikasi
      $('.notif-section').removeClass('d-none');
      //listenNotifikasi(btoa(member_id));
    
      //Cart
      fillCart();
      getListCart();

      $('.sideLogin').css("display", "none");
      $('.btnProfile1').css("display", "none");
      $('.usernameIcon').html(userdata.name);
      $('.usernameBoard').html(userdata.name);
      $('.pUsername').html(userdata.name);
      $('.pEmail').html(userdata.email);
      console.log(userdata.image_profile);
      if(userdata.image_profile) {
        $('.img-profile').prop('src', userdata.image_profile);
      }
      dataUrl = {
        'limit' : 6,
        'offset' : 0,
      };
    }else{
      dataUrl = {
        'bpom' : 1,
      };
      $('.sideProfile').css("display", "none");
      $('.btnProfile2').css("display", "none");
      $('.notif-section').addClass('d-none');
    }

    //Form Search
    $('#searchForm').submit( function(e){
      e.preventDefault();
      let term = $('.inputSearch').val();
      location.assign("<?php echo site_url('product') ?>"+'?term='+term+'#shopItemsContent');
    });

    //Form Login
    $("#formLogin").submit(function (e){
      e.preventDefault();
      var method    = $(this).attr('method');
      var data      = $(this).serialize(),
      email     = $('#signinEmail').val(),
      password  = $('#signinPassword').val();

      $('.btnLogin').prop('disabled', true);
      $('.btnLogin').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');

      $.ajax({
        url:url.login,
        method:'POST',
        dataType: 'json',
        data: {email: email, password: password},
        beforeSend: setHeader,
        success: function(res) { 
          $('.alert').removeClass('hide');
          $('.alert').addClass('show');
          $('.alert').addClass('alert-success');
          $('#notif').html(res.message);

                //set localstorage
                localStorage.setItem('prima-userdata', JSON.stringify(res.data_user)); 
                localStorage.setItem('prima-token', JSON.stringify(res.token)); 

                setTimeout(()=>{
                  window.location.reload();
                },1500);
              },
              error: function(res) {
                $('.alert').removeClass('hide');
                $('.alert').addClass('show');
                $('.alert').addClass('alert-danger');
                $('#notif').html(res.responseJSON.message);

                setTimeout(()=>{
                  $('.alert').addClass('hide');
                  $('.alert').removeClass('alert-danger');
                  $('.alert').removeClass('show');
                  
                  $('.btnLogin').prop('disabled', false);
                  $('.btnLogin').html('Login');

                },2000);
              },  
            });
    });

    //btn daftar
    $('#signin').click(function(){
      setTimeout(()=>{
        location.assign('<?php echo site_url('daftar'); ?>');
      },500);
    });

    //btn Logout
    $('.logout').click(function(){
      $(this).append(' <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>')
      deleteItem();

      setTimeout(()=>{
        location.assign('<?php echo base_url(); ?>');
      },1500);
    });

    //btn delete cart
    $('body').on('click', '.deleteCart', function(){
      let uid = $(this).data('uid');

      $('#dispDelete').html('Are you sure you want to delete this product?');
      $('.btnDelete').data('uid', uid);
      $('.btnDelete').data('mode', 'notifCart');
      $('#confirmationModal').modal('show');  
    });

    //btn delete cart
    $('body').on('click', '.btnDelete', function(){
      let uid = $(this).data('uid'),
          mode = $(this).data('mode');

      deleteListCart(uid, mode);
    });

    $('.showCart').click(function(e){
      if (userdata) {
        window.location.href = "<?php echo site_url('cart') ?>";
      }else{
        e.stopPropagation();
        $('#sidebarNavToggler').click();
      }
    });
  });


  function setHeader(xhr) {
    xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
  }

  /*function getNotifikasi(){
    $.ajax({
      url: url.getNotifikasi + `?id=${member_id}`,
      type: 'GET',
      dataType: 'json',
      success: function(res) {
        if(res.badge) {
          $('.badgeNotif').removeClass('d-none');
          $('.badgeNotif').html(res.badge);
          if(!$('.notif-empty').hasClass('d-none'))
            $('.notif-empty').addClass('d-none');
        } else {
          if(!$('.badgeNotif').hasClass('d-none'))
            $('.badgeNotif').addClass('d-none');
          $('.notif-empty').removeClass('d-none');
        }
      },
      error: function() { 
        console.log('error');
      },
      beforeSend: setHeader
    });
  };*/

  function fillBarangProduct(param){
    $.ajax({
        url: url.getProduct,
        type: 'GET',
        data: param,
        dataType: 'json',
        success: function(res) { 
          if (res.kelompok) {
            $('.productTitle').html(res.kelompok.nama);
            $('.productDesc').html(res.kelompok.deskripsi);
            $('.productImage').attr('src', url.urlImage+res.kelompok.foto_id);
          }

          if (res.list.length > 0) {
            for (var i = 0; i < res.list.length; i++) {
            fillBarangItem(res.list[i], i);
          }

          }else{
          $('.btnLoadmore').hide();
          $('.notfound').append('<div class="col-md-12 space-2">'
                        +'<span class="u-divider u-divider--text text-dark">Tidak ada product yang tersedia. </span>'
                      +'</div>');
          }
          
          // initialization of cubeportfolio
          if (userdata) {
            setTimeout(()=>{
              $('.appenItem').each(function() {
                  var divNum = $(this).find('div.itemList').length;
                  if (divNum < 6 || res.sisa_data == 0) {
                      $('.btnLoadmore').hide();
                  }
              });
              $.HSCore.components.HSCubeportfolio.init('.cbp');
            },3000);
          }else{
            $('.btnLoadmore').hide();
            $.HSCore.components.HSCubeportfolio.init('.cbp');
          }
        },
        error: function() { 
        },
        beforeSend: setHeader
    });
  }

  function fillBarangItem(data, index){
    let appentHtml = '',
    lblDiskon = '';

    diskon = parseInt(data.harga) * parseInt(data.diskon) / 100;
    totalDiskon = parseInt(data.harga) - diskon;
    
    if (data.diskon > 0) {
      lblDiskon = '<span class="badge badge-danger labelDiskon ml-2">'+numeral(data.diskon).format('00') +' % OFF </span>'
    }

    if (data) {
      appentHtml += '<div class="cbp-item rounded graphic itemList" style="border:groove">'
                  +'<a class="cbp-caption" data-id="'+data.id+'" href="<?php echo site_url('product/detail') ?>/'+data.uid+'">'
                    +'<div class="cbp-caption-defaultWrap">'
                      +'<img src="'+ (data.foto.length > 0 ? data.foto[0].original : "<?php echo base_url('assets/img/others/no_image_available.png'); ?>" ) +'" >'
                    +'</div>'
                    +'<div class="cbp-caption-activeWrap bg-header">'
                      +'<div class="cbp-l-caption-alignCenter">'
                        +'<div class="cbp-l-caption-body">'
                          +'<h4 class="h6 text-white mb-0">'+ data.nama +'</h4>'
                          +'<p class="small text-white-70 mb-0">'
                            +(!data.deskripsi_title ? '' : data.deskripsi_title)
                          +'</p>'
                        +'</div>'
                      +'</div>'
                    +'</div>'
                  +'</a>'
                +'</div>';
    }
  
    $('.appenItem').append(appentHtml);
  }

  function fillBarangProductLogin(param){
    $.ajax({
        url: url.getProduct,
        type: 'GET',
        data: param,
        dataType: 'json',
        success: function(res) { 
          if (res.list.length > 0) {
            for (var i = 0; i < res.list.length; i++) {
            fillBarangItemLogin(res.list[i], i);
          }

          }else{
          $('.btnLoadmore').hide();
          $('.notfound').append('<div class="col-md-12 space-2">'
                        +'<span class="u-divider u-divider--text text-dark">Tidak ada product yang tersedia. </span>'
                      +'</div>');
          }
          $.HSCore.components.HSCubeportfolio.init('.cbp');
        },
        error: function() { 
        },
        beforeSend: setHeader
    });
  }  

  function fillBarangItemLogin(data, index){
    let appentHtml = '',
    lblDiskon = '';

    diskon = parseInt(data.harga) * parseInt(data.diskon) / 100;
    totalDiskon = parseInt(data.harga) - diskon;
    
    if (data.diskon > 0) {
      lblDiskon = '<span class="badge badge-danger labelDiskon ml-2">'+numeral(data.diskon).format('00') +' % OFF </span>'
    }
    // console.log(data);
    if (data) {
      appentHtml += '<div class="cbp-item rounded graphic itemList" style="border:groove">'
                  +'<a class="cbp-caption" data-id="'+data.id+'" href="<?php echo site_url('product/detail') ?>/'+data.uid+'">'
                    +'<div class="cbp-caption-defaultWrap">'
                      +'<img src="'+ (data.foto.length > 0 ? data.foto[0].original : "<?php echo base_url('assets/img/others/no_image_available.png'); ?>" ) +'" >'
                    +'</div>'
                    +'<div class="cbp-caption-activeWrap bg-header">'
                      +'<div class="cbp-l-caption-alignCenter">'
                        +'<div class="cbp-l-caption-body">'
                          +'<h4 class="h6 text-white mb-0">'+ data.nama +'</h4>'
                          +'<p class="small text-white-70 mb-0">'
                            +(!data.deskripsi_title ? '' : data.deskripsi_title)
                          +'</p>'
                        +'</div>'
                      +'</div>'
                    +'</div>'
                  +'</a>'
                +'</div>';
    }
    
    $('.appenItemLogin').append(appentHtml);
  }

  //Kelompok Barang
  /*$.ajax({
    url: url.getKelompokbarang,
    type: 'GET',
    dataType: 'json',
    success: function(res) { 
      if (res.list.length > 0) {
        for (var i = 0; i < res.list.length; i++) {
          fillNav(res.list[i], i);
        }
      }
    },
    error: function() { 
      console.log('error');
    },
    beforeSend: setHeader
  });

  function fillNav(data, index){
    let appentHtml = ''

    if (userdata) {
      if (data) {
        appentHtml += '<div class="u-header__promo-item">'
        +'<a class="u-header__promo-link" data-id="'+data.id+'" href="<?php echo site_url('product') ?>'+'?uid='+data.uid+'">'
        +'<div class="media align-items-center">'
        +'<img class="js-svg-injector u-header__promo-icon" src="'+url.getImageKelompokBarang + data.foto_id +'" alt="SVG">'
        +'<div class="media-body">'
        +'<span class="u-header__promo-title">'
        + data.nama 
        +'</span>'
        +'<small class="u-header__promo-text">'+data.deskripsi+'</small>'
        +'</div>'
        +'</div>'
        +'</a>'
        +'</div>'
      }
    }else{
      if (data.bpom == 1) {
        appentHtml += '<div class="u-header__promo-item">'
        +'<a class="u-header__promo-link" data-id="'+data.id+'" href="<?php echo site_url('product') ?>'+'?uid='+data.uid+'">'
        +'<div class="media align-items-center">'
        +'<img class="js-svg-injector u-header__promo-icon" src="'+url.getImageKelompokBarang + data.foto_id +'" alt="SVG">'
        +'<div class="media-body">'
        +'<span class="u-header__promo-title">'
        + data.nama 
        +'</span>'
        +'<small class="u-header__promo-text">'+data.deskripsi+'</small>'
        +'</div>'
        +'</div>'
        +'</a>'
        +'</div>'
      }
    }
    
    $('.appenNav').append(appentHtml);
  }*/

  function fillCart(){
    $.ajax({
      url: url.getCart,
      type: 'GET',
      data: {member_id: member_id},
      dataType: 'json',
      success: function(res) { 
        if (res.list.length > 0) {
          $('.appendCart').removeClass('text-center');
          appentHtml = '<span class="u-header__promo-title p-1 text-center">'
          +'List Cart'
          +'</span>';
          $('.appendCart').html(appentHtml);
          $('.badgeCart').html(res.sisa_data);

          for (var i = 0; i < res.list.length; i++) {
            fillListCart(res.list[i], i);
          }
        }else{
          $('.badgeCart').html(res.sisa_data);
          appentHtml = '<span class="btn btn-icon btn-soft-primary rounded-circle mb-3">'
          +'<span class="fas fa-shopping-basket btn-icon__inner"></span>'
          +'</span>'
          +'<span class="d-block">Your Cart is Empty</span>';
          $('.appendCart').html(appentHtml);
        } 

      },
      error: function() { 
        console.log('error');
      },
      beforeSend: setHeader
    });
  }

  function fillListCart(data, index){
    let appentHtml = '', 

    diskon = parseInt(data.harga) * parseInt(data.product_diskon) / 100;
    totalDiskon = parseInt(data.harga) - diskon;

    harga = 'Rp. '+ numeral(totalDiskon).format();
    deskripsi = data.product_deskripsi.substring(0, 30);

    if (data) {
      appentHtml += '<!-- Promo Item -->'
      +'<div class="u-header__promo-item p-3">'
      +'<div class="media align-items-center">'
      +'<img class="js-svg-injector u-header__promo-icon" src="'+ (data.foto.length > 0 ? data.foto[0].original : "<?php echo base_url('assets/img/others/no_image_available.png'); ?>")  +'">'
      +'<div class="col-md-8">'
      +'<div class="media-body">'
      +'<span class="u-header__promo-title">'
      + data.product_nama
      +'</span>'
      +'<strong class="u-header__promo-text">'+ harga +'</strong>'
      +'<small class="u-header__promo-text">'+ deskripsi +' ...</small>'
      +'</div>'
      +'</div>'
      +'<div class="col-md-2">'
      +'<div class="media-body">'
      +'<button type="button" class="btn btn-icon btn-danger btn-sm transition-3d-hover deleteCart" data-uid="'+data.uid+'">'
      +'<span class="fa fa-trash btn-icon__inner"></span>'
      +'</button>'
      +'</div>'
      +'</div>'
      +'</div>'
      +'</div>'
      +'<!-- End Promo Item -->'
    }
    
    $('.appendCart').append(appentHtml);
  }

  function getListCart(){
    $.ajax({
      url: url.getCart,
      type: 'GET',
      data: {member_id: member_id},
      dataType: 'json',
      success: function(res) { 
        var tbody = $("#tableCart tbody"),
          tfoot = $("#tableCart tfoot");
        $('.checkoutAddress').hide();
        if (res.list.length > 0) {
          tbody.empty();
          tfoot.empty();
          for (var i = 0; i < res.list.length; i++) {
            fillListCarts(res.list[i], i, tbody);
          }

          fillFooter(tbody, tfoot);

          //$('#tableCart').find('.checkAll').click();
        }else{
          tbody.empty()
          tfoot.empty()
          var tr = $("<tr/>")
          .appendTo(tfoot);

          var tdOngkir = $("<td/>")
          .attr('colspan', '6')
          .addClass('text-center')
          .appendTo(tr);
          var dispOngkir = $("<span/>")
          .html('Your cart is Empty .')
          .appendTo(tdOngkir);
        } 
      },
      error: function() { 
        console.log('error');
      },
      beforeSend: setHeader
    });
  }

  function deleteItem() {
    localStorage.removeItem("prima-userdata");
    localStorage.removeItem("prima-token");
  }

  function blockPage() {
    $.blockUI({
      message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left fa-2x"></i>&nbsp;Loading...</span>',
      overlayCSS: {
        backgroundColor: '#000',
        opacity: 0.6,
        cursor: 'wait'
      },
      css: {
        border: 0,
        padding: '10px 15px',
        color: '#fff',
        '-webkit-border-radius': 2,
        '-moz-border-radius': 2,
        backgroundColor: '#333'
      }
    });
  }

  function deleteListCart(uid, mode){
    $('#confirmationModal').modal('hide');  
    blockPage('Loading ...');
    $.ajax({
      url:url.deleteCart,
      method:'POST',
      dataType: 'json',
      data: {uid: uid},
      beforeSend: setHeader,
      success: function(res) { 
        setTimeout(()=>{
          $.unblockUI();
          if (mode == 'notifCart') {
            fillCart();
            getListCart();
          }else{
            getListCart();
            fillCart();
          }
        },1000);
      },
      error: function(res) {
        setTimeout(()=>{
          $.unblockUI();
          fillCart();
        },2000);
      },  
    });
  };

  function ajaxProv(){
    let provinsi = $('#aProvinsi').val();

    $.ajax({
        type : 'GET',
        url : site_url+'/akun/profile/getProvince',
        data : 'province=' + provinsi,
        dataType : 'json',
        success: function (data) {
          
          let results = data.rajaongkir.results;
            console.log(results);
        }
    });
  }

  $('#provinsi').change(function(){
    var prov = $('#provinsi').val();
    var prov_text = $(this).find('option:selected').text();
        $('#provinsi_text').val(prov_text);
    var kota = $('#kota_tmp').val();
    

    ajaxKab(prov, kota);

  });

  function ajaxKab(prov, kota){
    $.ajax({
        type : 'GET',
        url : site_url+'/akun/profile/getKab',
        data :  'prov_id=' + prov,
        dataType : 'json',
        success: function (data) {
          
          let results = data.rajaongkir.results;
          
          $('#kota').html('<option value="">Pilih</option>');
          
          for (var i = 0; i < results.length; i++) {
            let option = '<option value="'+results[i].city_id+'">'+results[i].city_name+'</option>'
            $('#kota').append(option);
          }

          if (kota) {
            $('#kota').val(kota).trigger('change');
          }
        }
    });
  }

  $('#kota').change(function(){
    var city = $('#kota').val();
    var kota_text = $(this).find('option:selected').text();
        $('#kota_text').val(kota_text);
    var kecamatan = $('#kecamatan_tmp').val();

    ajaxKec(city, kecamatan);
    
  });

  function ajaxKec(city, kecamatan){
    $.ajax({
        type : 'GET',
        url : site_url+'/akun/profile/getKota',
        data : 'city_id=' + city,
        dataType : 'json',
        success: function (data) {
          
          let results = data.rajaongkir.results;
          
          $('#kecamatan').html('<option value="">Pilih</option>');
          
          for (var i = 0; i < results.length; i++) {
            let option = '<option value="'+results[i].subdistrict_id+'">'+results[i].subdistrict_name+'</option>'
            $('#kecamatan').append(option);
          }

          if (kecamatan) {
            $('#kecamatan').val(kecamatan).trigger('change');
          }
        }
    });
  }

  $('#kecamatan').change(function(){
    var kecamatan_text = $(this).find('option:selected').text();
        $('#kecamatan_text').val(kecamatan_text);
  });
</script>