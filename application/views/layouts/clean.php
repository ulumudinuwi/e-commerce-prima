<!DOCTYPE html>
<html lang="<?php echo $this->session->userdata('lang') ?>">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Prima Estetika Raksa">
    <?php echo $template['metas']; ?>

    <title><?php echo $template['title'] .' - '. $page_title ?> </title>

    <link rel="shortcut icon" href="<?php echo assets_url('img/logos/prima.png') ?>">

    <!-- Bootstrap Core CSS -->
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

	<!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?php echo assets_url('vendor/font-awesome/css/fontawesome-all.min.css')?>">
    <link rel="stylesheet" href="<?php echo assets_url('vendor/animate.css/animate.min.css')?>">
    <link rel="stylesheet" href="<?php echo assets_url('vendor/hs-megamenu/src/hs.megamenu.css')?>">
    <link rel="stylesheet" href="<?php echo assets_url('vendor/fancybox/jquery.fancybox.css')?>">
    <link rel="stylesheet" href="<?php echo assets_url('vendor/flatpickr/dist/flatpickr.min.css')?>">
    <link rel="stylesheet" href="<?php echo assets_url('vendor/slick-carousel/slick/slick.css')?>">
    <link rel="stylesheet" href="<?php echo assets_url('vendor/cubeportfolio/css/cubeportfolio.min.css')?>">

    <!-- Theme --> 
    <link  rel="stylesheet" href="<?php echo script_url('assets/css/theme.css'); ?>">

    <?php echo $template['css']; ?>

	  <?php echo $template['js_header']; ?>

    <!-- JS Global Compulsory -->
    <script src="<?php echo assets_url('vendor/jquery/dist/jquery.min.js')?>"></script>
    <script src="<?php echo assets_url('vendor/jquery-migrate/dist/jquery-migrate.min.js')?>"></script>
    <script src="<?php echo assets_url('vendor/popper.js/dist/umd/popper.min.js')?>"></script>
    <script src="<?php echo assets_url('vendor/bootstrap/bootstrap.min.js')?>"></script>
    <script src="<?php echo assets_url('vendor/datatables/media/js/jquery.dataTables.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo script_url('assets/vendor/dropzone/dropzone.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('loaders/blockui.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo js_url('numeral/numeral.js') ?>"></script>
</head>

<body class="clean">
  <style type="text/css">
    #shoppingCartDropdown, #notifDropdown {
      min-width: 430px;
    }
    .bg-img-hero {
      background-size: contain;
    }
    @media (max-width: 999px) {
      body {
        font-size: 0.975rem;
      }
      h1, .h1 {
        font-size: 2.2rem;
      }

      h2, .h2 {
        font-size: 1.7rem;
      }

      h3, .h3 {
        font-size: 1.5rem;
      }

      h4, .h4 {
        font-size: 1.3rem;
      }

      h5, .h5 {
        font-size: 1.1rem;
      }

      h6, .h6 {
        font-size: 0.975rem;
      }
      #shoppingCartDropdown, #notifDropdown {
        min-width: 300px;
      }
    }
    @media (max-width: 576px) {
      body {
        font-size: 0.875rem;
      }
      h1, .h1 {
        font-size: 2rem;
      }

      h2, .h2 {
        font-size: 1.5rem;
      }

      h3, .h3 {
        font-size: 1.3rem;
      }

      h4, .h4 {
        font-size: 1.1rem;
      }

      h5, .h5 {
        font-size: 0.975rem;
      }

      h6, .h6 {
        font-size: 0.875rem;
      }
      #shoppingCartDropdown, #notifDropdown {
        min-width: 200px;
      }
      #profile-navbar-brand {
        font-size: 1rem;
      }
    }
  </style>
  <!-- ========== HEADER ========== -->
  <header id="header" class="u-header u-header-center-aligned-nav u-header--bg-transparent u-header--white-nav-links-md u-header--sub-menu-white-bg-md u-header--abs-top" data-header-fix-moment="500" data-header-fix-effect="slide">
      <!-- Search -->
      <div id="searchPushTop" class="u-search-push-top">
        <div class="container position-relative">
          <div class="u-search-push-top__content">
            <!-- Close Button -->
            <button type="button" class="close u-search-push-top__close-btn"
                    aria-haspopup="true"
                    aria-expanded="false"
                    aria-controls="searchPushTop"
                    data-unfold-type="jquery-slide"
                    data-unfold-target="#searchPushTop">
              <span aria-hidden="true">&times;</span>
            </button>
            <!-- End Close Button -->

            <!-- Input -->
            <form class="js-focus-state input-group input-group-md" id="searchForm">
              <input type="search" class="form-control inputSearch" placeholder="Search Product" aria-label="Search Product">
              <div class="input-group-append">
                <button type="submit" class="btn btn-primary searchButton">Search</button>
              </div>
            </form>
            <!-- End Input -->

          </div>
        </div>
      </div>
      <!-- End Search -->

      <div class="u-header__section">
        <!-- Topbar -->
        <div class="u-header__hide-content pt-2 bg-header">
          <div class="container d-flex align-items-center">
            <!-- Language -->
            <div class="position-relative">
                <a class="btn btn-sm btn-icon text-white" href="https://www.facebook.com/primeaest/" target="_blank">
                  <span class="fab fa-facebook-f btn-icon__inner"> </span>
                </a>
                <a class="btn btn-sm btn-icon text-white" href="https://instagram.com/optimelife?igshid=3hi1gasgmkic" target="_blank">
                  <span class="fab fa-instagram btn-icon__inner"></span>
                </a>
                <a class="btn btn-sm btn-icon text-white" href="https://wa.me/62895365290732" alt="Whatsapp" target="_blank">
                  <span class="fab fa-whatsapp btn-icon__inner"></span> 
                </a>
                  <!-- <small class="text-white"><a href="https://wa.me/62895365290732" style="color: #fff;" target="_blank">+62895365290732</a></small> --> 
            </div>
            <!-- End Language -->

            <div class="ml-auto">
              <!-- Jump To -->
              
            </div>

            <ul class="list-inline ml-2 mb-0">
              <!-- Search -->
              <li class="list-inline-item">
                <a class="btn btn-xs btn-icon btn-text-secondary" href="javascript:;" role="button"
                        aria-haspopup="true"
                        aria-expanded="false"
                        aria-controls="searchPushTop"
                        data-unfold-type="jquery-slide"
                        data-unfold-target="#searchPushTop">
                  <span class="fas fa-search btn-icon__inner"></span>
                </a>
              </li>
              <!-- End Search -->


              <!-- Shopping Cart -->
              <li class="list-inline-item position-relative">
                <a id="shoppingCartDropdownInvoker" class="btn btn-xs btn-icon btn-text-secondary" href="javascript:;" role="button"
                        aria-controls="shoppingCartDropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        data-unfold-event="hover"
                        data-unfold-target="#shoppingCartDropdown"
                        data-unfold-type="css-animation"
                        data-unfold-duration="300"
                        data-unfold-delay="300"
                        data-unfold-hide-on-scroll="true"
                        data-unfold-animation-in="slideInUp"
                        data-unfold-animation-out="fadeOut">
                  <span class="fas fa-shopping-cart btn-icon__inner"></span>
                  <span class="badge badge-sm badge-outline-success badge-pos rounded-circle badgeCart">0</span>
                </a>

                <div id="shoppingCartDropdown" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="shoppingCartDropdownInvoker">
                  <div class="appendCart mb-3">
                      <span class="btn btn-icon btn-soft-primary rounded-circle mb-3">
                      <span class="fas fa-shopping-basket btn-icon__inner"></span>
                      </span>'
                      <span class="d-block">Your Cart is Empty</span>
                      <br>
                  </div>

                  <div class="text-right border-top pb-0 pt-3">
                    <!-- List -->
                    <div class="row">
                      <div class="col-12">
                        <div class="u-header__promo-footer-item">
                          <button class="btn btn-primary btn-xs showCart" style="cursor: pointer;">Show shopping Cart</button>
                        </div>
                      </div>
                    </div>
                    <!-- End List -->
                  </div>
                </div>
              </li>
              <!-- End Shopping Cart -->

              <!-- Notification -->
              <li class="list-inline-item position-relative notif-section">
                <a id="notifDropdownInvoker" class="btn btn-xs btn-icon btn-text-secondary" href="javascript:;" role="button"
                        aria-controls="notifDropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        data-unfold-event="click"
                        data-unfold-target="#notifDropdown"
                        data-unfold-type="css-animation"
                        data-unfold-duration="300"
                        data-unfold-delay="300"
                        data-unfold-animation-in="slideInUp"
                        data-unfold-animation-out="fadeOut">
                  <span class="fas fa-bell btn-icon__inner"></span>
                  <span class="badge badge-sm badge-outline-success badge-pos rounded-circle badgeNotif d-none">0</span>
                </a>

                <div id="notifDropdown" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="notifDropdownInvoker">
                  <div class="notif-content" style="max-height: 340px; overflow-y: auto;">
                    <!-- <div class="notif-empty mb-3 text-center">
                      <span class="btn btn-icon btn-soft-primary rounded-circle mb-3">
                        <span class="fas fa-bell-slash btn-icon__inner"></span>
                      </span>
                      <span class="d-block">You don't have any notifications</span>
                    </div> -->
                    <!-- <div class="card p-1">
                      <div class="mb-0 card-body">
                        <a href="#" class="text-primary">
                          Pesanan O - PER.202008.0001 telah dikirim.<br/>
                          <small class="text-muted">
                            14-08-2020 14:00
                          </small>
                        </a>
                      </div>
                    </div> -->
                  </div>
                  <div class="notif-more text-center text-primary border-top pb-0 pt-3" style="cursor: pointer;">
                    Show More
                  </div>
                </div>
              </li>
              <!-- End Notification -->
              
              <!-- Account Login -->
              <li class="list-inline-item">
                <!-- Account Sidebar Toggle Button -->
                <!-- <a id="sidebarNavToggler" class="btn btn-xs btn-icon btn-text-secondary btnProfile1" href="javascript:;" role="button"
                   aria-controls="sidebarContent"
                   aria-haspopup="true"
                   aria-expanded="false"
                   data-unfold-event="click"
                   data-unfold-hide-on-scroll="false"
                   data-unfold-target="#sidebarContent"
                   data-unfold-type="css-animation"
                   data-unfold-animation-in="fadeInRight"
                   data-unfold-animation-out="fadeOutRight"
                   data-unfold-duration="500">
                  <span class="fas fa-user-circle btn-icon__inner font-size-1"></span>
                </a>

                <a id="sidebarNavToggler" class="btn btn-xs btn-icon btn-text-secondary btnProfile1" href="javascript:;" role="button"
                   aria-controls="sidebarContent"
                   aria-haspopup="true"
                   aria-expanded="false"
                   data-unfold-event="click"
                   data-unfold-hide-on-scroll="false"
                   data-unfold-target="#sidebarContent"
                   data-unfold-type="css-animation"
                   data-unfold-animation-in="fadeInRight"
                   data-unfold-animation-out="fadeOutRight"
                   data-unfold-duration="500">
                  <span class="btn-icon__inner font-size-1 text-white">Login</span>
                </a> -->

                <a id="sidebarNavToggler" class="btn btn-xs btn-text-secondary u-sidebar--account__toggle-bg ml-1 btnProfile2" href="javascript:;" role="button"
                   aria-controls="sidebarContent"
                   aria-haspopup="true"
                   aria-expanded="false"
                   data-unfold-event="click"
                   data-unfold-hide-on-scroll="false"
                   data-unfold-target="#sidebarContent"
                   data-unfold-type="css-animation"
                   data-unfold-animation-in="fadeInRight"
                   data-unfold-animation-out="fadeOutRight"
                   data-unfold-duration="500">
                  <span class="position-relative">
                    <span class="u-sidebar--account__toggle-text text-white usernameIcon"></span>
                    <img class="u-sidebar--account__toggle-img img-profile" src="<?php echo assets_url()?>/img/others/no_image_available.png" alt="Image Description">
                    <!-- <span class="badge badge-sm badge-success badge-pos rounded-circle">0</span> -->
                  </span>
                </a>
                <!-- End Account Sidebar Toggle Button -->
              </li>
              <!-- End Account Login -->
            </ul>
          </div>
        </div>
        <!-- End Topbar -->

        <div id="logoAndNav" class="bg-white">
          <!-- Nav -->
          <nav class="container js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space mt-0 mt-md-2 mt-lg-2">
            <!-- Logo -->
            <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center" href="<?php echo base_url() ?>" aria-label="Front">
              <img class="js-svg-injector " src="<?php echo assets_url('img/logos/prima.png')?>" alt="-">
            </a>
            <!-- End Logo -->

            <!-- Responsive Toggle Button -->
            <button type="button" class="navbar-toggler btn u-hamburger"
                    aria-label="Toggle navigation"
                    aria-expanded="false"
                    aria-controls="navBar"
                    data-toggle="collapse"
                    data-target="#navBar">
              <span id="hamburgerTrigger" class="u-hamburger__box">
                <span class="u-hamburger__inner"></span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Navigation -->
            <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
              <ul class="navbar-nav u-header__navbar-nav">
                <!-- Home -->
                <li class="nav-item u-header__nav-item">
                  <a  class="nav-link u-header__nav-link" href="<?php echo base_url() ?>" aria-haspopup="true" aria-expanded="false">Home</a>
                </li>
                <!-- End Home -->

                <!-- About Us -->
                <li class="nav-item u-header__nav-item">
                  <a class="nav-link u-header__nav-link" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url('#aboutus') ?>">About Us</a>
                </li>
                <!-- End About Us -->

                <!-- News & Promotions -->
                <li class="nav-item u-header__nav-item">
                  <a  class="nav-link u-header__nav-link " href="<?php echo site_url('news_promo') ?>" aria-haspopup="true" aria-expanded="false">News & Promotions</a>
                </li>
                <!-- End News & Promotions -->

                <!-- Product -->
                <li class="nav-item u-header__nav-item">
                  <a  class="nav-link u-header__nav-link" href="<?php echo site_url('product') ?>" aria-haspopup="true" aria-expanded="false">Product</a>
                </li>
                
                <!-- Contact Us -->
                <li class="nav-item u-header__nav-item">
                  <a  class="nav-link u-header__nav-link " href="<?php echo site_url('contact_us') ?>" aria-haspopup="true" aria-expanded="false">Contact Us</a>
                </li>
                <!-- End Contact Us -->

                <!-- Contact Us -->
                <li class="nav-item u-header__nav-item">
                  <a  id="sidebarNavToggler" class="nav-link u-header__nav-link btnProfile1" href="javascript:;" role="button"
                  aria-controls="sidebarContent"
                   aria-haspopup="true"
                   aria-expanded="false"
                   data-unfold-event="click"
                   data-unfold-hide-on-scroll="false"
                   data-unfold-target="#sidebarContent"
                   data-unfold-type="css-animation"
                   data-unfold-animation-in="fadeInRight"
                   data-unfold-animation-out="fadeOutRight"
                   data-unfold-duration="500">
                 Login</a>
                </a>
                </li>
                <!-- End Contact Us -->

              </ul>
            </div>
            <!-- End Navigation -->
          </nav>
          <!-- End Nav -->
        </div>
      </div>
  </header>
  <!-- ========== END HEADER ========== -->

	<div class="">
		<?php echo $template['content']; ?>
	</div>
	
  <!-- ========== FOOTER ========== -->
  <footer class="container space-top-1 space-top-md-1">
      <div class="border-bottom border-top">
        <div class="row mb-7 space-top-1">
          <div class="col-lg-2">
            <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center" href="<?php echo base_url() ?>" aria-label="Front">
              <img class="js-svg-injector " src="<?php echo assets_url('img/logos/prima.png')?>" alt="-">
            </a>
          </div>
          <div class="col-lg-10">
            <!-- Market Capitalization List -->
            <ul class="list-inline row justify-content-lg-end text-secondary text-uppercase space-top-1">
              <li class="list-inline-item col-6 col-sm-4 col-md-auto u-ver-divider u-ver-divider--xs u-ver-divider--none-md px-md-7 mx-0 mb-4 mb-md-0">
                <a class="d-block text-secondary" href="<?php echo base_url('#aboutus') ?>">
                  <small class="d-block">Home</small>
                </a>
              <li class="list-inline-item col-6 col-sm-4 col-md-auto u-ver-divider u-ver-divider--xs u-ver-divider--none-md px-md-7 mx-0 mb-4 mb-md-0">
                <a class="d-block text-secondary" href="<?php echo base_url('#aboutus') ?>">
                  <small class="d-block">About Us</small>
                </a>
              </li>
              <li class="list-inline-item col-6 col-sm-4 col-md-auto u-ver-divider u-ver-divider--xs u-ver-divider--none-md px-md-7 mx-0 mb-4 mb-md-0">
                <a class="d-block text-secondary" href="<?php echo site_url('news_promo') ?>">
                  <small class="d-block"> News & Promotions</small>
                </a>
              <li class="list-inline-item col-6 col-sm-4 col-md-auto pl-md-7 mx-0 mb-4 mb-md-0">
                <a class="d-block text-secondary" href="<?php echo site_url('product') ?>">
                  <small class="d-block">Products</small>
                </a>
              </li>
              <li class="list-inline-item col-6 col-sm-4 col-md-auto pl-md-7 mx-0 mb-4 mb-md-0">
                <a class="d-block text-secondary" href="<?php echo site_url('contact_us') ?>">
                  <small class="d-block">Contact Us</small>
                </a>
              </li>
            </ul>
            <!-- End Market Capitalization List -->
          </div>
        </div>
      </div>

      <div class="d-flex justify-content-between align-items-center py-7">
        <!-- Copyright -->
        <p class="small text-muted mb-0"><?php echo date('Y'); ?> &copy; Prima Estetika Raksa All rights reserved.</p>
        <!-- End Copyright -->

        <!-- Social Networks -->
        <ul class="list-inline mb-0">
          <li class="list-inline-item">
            <a class="btn btn-sm btn-icon btn-soft-primary btn-bg-transparent" href="https://www.facebook.com/primeaest/" target="_blank">
              <span class="fab fa-facebook-f btn-icon__inner"></span> 
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn btn-sm btn-icon btn-soft-primary btn-bg-transparent" href="https://instagram.com/optimelife?igshid=3hi1gasgmkic" target="_blank">
              <span class="fab fa-instagram btn-icon__inner"></span>
            </a>
          </li>
        </ul>
        <!-- End Social Networks -->
      </div>
  </footer>
  <!-- ========== END FOOTER ========== -->
  <?php echo $template['js_footer']; ?>

  <!-- ========== SECONDARY CONTENTS ========== -->
  <!-- Account Sidebar Navigation -->
  <aside id="sidebarContent" class="u-sidebar" aria-labelledby="sidebarNavToggler">
    <div class="u-sidebar__scroller">
      <div class="u-sidebar__container">
        
        <!-- login -->
        <div class="u-header-sidebar__footer-offset sideLogin" >
          <!-- Toggle Button -->
          <div class="d-flex align-items-center pt-4 px-7">
            <button type="button" class="close ml-auto"
                    aria-controls="sidebarContent"
                    aria-haspopup="true"
                    aria-expanded="false"
                    data-unfold-event="click"
                    data-unfold-hide-on-scroll="false"
                    data-unfold-target="#sidebarContent"
                    data-unfold-type="css-animation"
                    data-unfold-animation-in="fadeInRight"
                    data-unfold-animation-out="fadeOutRight"
                    data-unfold-duration="500">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <!-- End Toggle Button -->

          <!-- Content -->
          <div class="js-scrollbar u-sidebar__body">
            <div class="u-sidebar__content u-header-sidebar__content">
              <form class="js-validate" id="formLogin">
                <!-- Login -->
                <div id="login" data-target-group="idForm">
                  <!-- Title -->
                  <header class="text-center">
                    <h2 class="h4 mb-0">Welcome Optimelife!</h2>
                    <p>Login Member.</p>
                  </header>
                  <!-- End Title -->
                  <div class="alert alert-dismissible fade hide" role="alert">
                    <strong id="notif">&nbsp;</strong>
                  </div>
                  <!-- Form Group -->
                  <div class="form-group">
                    <div class="js-form-message js-focus-state">
                      <label class="sr-only" for="signinEmail">Username</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="signinEmailLabel">
                            <span class="fas fa-user"></span>
                          </span>
                        </div>
                        <input type="text" class="form-control" name="email" id="signinEmail" placeholder="Username" aria-label="Username" required
                               data-msg="Please enter a valid Username address."
                               data-error-class="u-has-error"
                               data-success-class="u-has-success">
                      </div>
                    </div>
                  </div>
                  <!-- End Form Group -->

                  <!-- Form Group -->
                  <div class="form-group">
                    <div class="js-form-message js-focus-state">
                      <label class="sr-only" for="signinPassword">Password</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="signinPasswordLabel">
                            <span class="fas fa-lock"></span>
                          </span>
                        </div>
                        <input type="password" class="form-control" name="password" id="signinPassword" placeholder="Password" aria-label="Password" aria-describedby="signinPasswordLabel" required
                               data-msg="Your password is invalid. Please try again."
                               data-error-class="u-has-error"
                               data-success-class="u-has-success">
                      </div>
                    </div>
                  </div>
                  <!-- End Form Group -->

                  <div class="d-flex justify-content-end mb-4">
                    <a class="js-animation-link small link-muted" href="javascript:;"
                       data-target="#forgotPassword"
                       data-link-group="idForm"
                       data-animation-in="slideInUp">Forgot Password?</a>
                  </div>

                  <div class="mb-2">
                    <button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover btnLogin">Login</button>
                  </div>
                </div>
              </form>
              <div class="d-flex mb-1">
                <a class="btn btn-block btn-sm btn-soft-twitter btn-text-white transition-3d-hover mr-1" href="javascript:;" id="signin">
                  Daftar
                </a>
              </div>
              <div class="d-flex mb-1">
                <a class="btn btn-block btn-sm btn-soft-instagram btn-text-white transition-3d-hover mr-1" href="https://wa.me/62895365290732" target="_blank">
                  <span class="fab fa-whatsapp mr-1"></span>
                  Pesan Via Whatsapp
                </a>
              </div>
            </div>
          </div>
          <!-- End Content -->
        </div>

        <!-- Profile -->
        <div class="u-sidebar--account__footer-offset sideProfile">
          <!-- Toggle Button -->
          <div class="d-flex justify-content-between align-items-center pt-4 px-7">
            <h3 class="h6 mb-0">My Account</h3>

            <button type="button" class="close ml-auto"
                    aria-controls="sidebarContent"
                    aria-haspopup="true"
                    aria-expanded="false"
                    data-unfold-event="click"
                    data-unfold-hide-on-scroll="false"
                    data-unfold-target="#sidebarContent"
                    data-unfold-type="css-animation"
                    data-unfold-animation-in="fadeInRight"
                    data-unfold-animation-out="fadeOutRight"
                    data-unfold-duration="500">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <!-- End Toggle Button -->

          <!-- Content -->
          <div class="js-scrollbar u-sidebar__body">
            <!-- Holder Info -->
            <header class="d-flex align-items-center u-sidebar--account__holder mt-3">
              <div class="position-relative">
                <img class="u-sidebar--account__holder-img img-profile" src="<?php echo assets_url()?>/img/others/no_image_available.png" alt="Image Description">
                <span class="badge badge-xs badge-outline-success badge-pos rounded-circle"></span>
              </div>
              <div class="ml-3">
                <span class="font-weight-semi-bold usernameBoard"></span>
                <span class="u-sidebar--account__holder-text">Member</span>
              </div>

              <!-- Settings -->
              <div class="btn-group position-relative ml-auto mb-auto">
                <a id="sidebar-account-settings-invoker" class="btn btn-xs btn-icon btn-text-secondary rounded" href="javascript:;" role="button"
                        aria-controls="sidebar-account-settings"
                        aria-haspopup="true"
                        aria-expanded="false"
                        data-toggle="dropdown"
                        data-unfold-event="click"
                        data-unfold-target="#sidebar-account-settings"
                        data-unfold-type="css-animation"
                        data-unfold-duration="300"
                        data-unfold-delay="300"
                        data-unfold-animation-in="slideInUp"
                        data-unfold-animation-out="fadeOut">
                  <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="sidebar-account-settings" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="sidebar-account-settings-invoker">
                  <label class="dropdown-item logout" style="cursor: pointer;">Log out </label>
                </div>
              </div>
              <!-- End Settings -->
            </header>
            <!-- End Holder Info -->

            <div class="u-sidebar__content--account">
              <!-- List Links -->
              <ul class="list-unstyled u-sidebar--account__list">
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="<?php echo site_url('akun/profile') ?>">
                    <span class="fas fa-user u-sidebar--account__list-icon mr-2"></span>
                    Akun
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="<?php echo site_url('akun/profile/pesanan') ?>">
                    <span class="fas fa-envelope u-sidebar--account__list-icon mr-2"></span>
                    Pesanan
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link logout" href="#">
                    <span class="fas fa-power-off u-sidebar--account__list-icon mr-2"></span>
                    Log out
                  </a>
                </li>
              </ul>
              <!-- End List Links -->

            </div>
          </div>
        </div>

        <!-- Footer -->
        <!-- <footer id="SVGwaveWithDots" class="svg-preloader u-sidebar__footer u-sidebar__footer--account">
          <ul class="list-inline mb-0">
            <li class="list-inline-item pr-3">
              <a class="u-sidebar__footer--account__text" href="#">Privacy</a>
            </li>
            <li class="list-inline-item pr-3">
              <a class="u-sidebar__footer--account__text" href="#">Terms</a>
            </li>
            <li class="list-inline-item">
              <a class="u-sidebar__footer--account__text" href="#">
                <i class="fas fa-info-circle"></i>
              </a>
            </li>
          </ul>

          <div class="position-absolute right-0 bottom-0 left-0">
            <img class="js-svg-injector" src="<?php echo assets_url()?>/img/others/login_wave.png" alt="Image Description" height="160px" width="100%"
                   data-parent="#SVGwaveWithDots">
          </div>
        </footer> -->
        <!-- End Footer -->
        </div>
      </div>
    </div>
  </aside>
  <!-- End Account Sidebar Navigation -->
  <!-- ========== END SECONDARY CONTENTS ========== -->

  <!-- Modal -->
  <div class="modal fade" id="AlertModal" tabindex="-1" role="dialog" aria-labelledby="AlertModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="AlertModalLabel">Peringatan !!!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <span id="dispNotif"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-xs btn-secondary btnEditProfile" style="display: none;">Edit Alamat</button>
          <button type="button" class="btn btn-xs btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="ongkirModal" tabindex="-1" role="dialog" aria-labelledby="AlertModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="AlertModalLabel">Pilih Opsi Pengiriman</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <!-- Accordion -->
        <div id="paymentDetails" class="accordion mb-9">

        </div>
        <!-- End Accordion -->
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="EkspedisiModal" tabindex="-1" role="dialog" aria-labelledby="EkspedisiModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="EkspedisiModalLabel">Pilih Kurir</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <span id="dispNotif"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-xs btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="PrintModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="PrintModalLabel">Cetak Faktur</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <iframe src="" height="450" width="100%" id="modalIframe"></iframe>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-xs btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="confirmationModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="confirmationModalLabel">Peringatan !!!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <span id="dispDelete"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-xs btn-danger btnDelete">Delete</button>
          <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="uploadModalLabel">Upload Bukti Pembayaran .</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group row">
              <!-- Dropzone -->
              <div class="col-12">
                <label class="file-attachment-input py-4" for="floorplanAttachmentInput">
                  <input id="fileUpload" name="images-attachment" type="file" class="js-custom-file-attach file-attachment-input__label"
                         data-result-text-target="#floorplanFileUploadText">
                  <img src="#" height="300" width="100%" id="imgFile">
                  <span id="floorplanFileUploadText" class="mb-5">
                    <span class="d-block mb-2">Klik untuk mengupload file.</span>
                    <small class="d-block text-muted">Maximum file size is 1MB</small>
                  </span>
                  <br>
                  <br>
                  <span class="d-block" id="labelTf"></span>
                </label>
              </div>
              <!-- End File Attachment Input -->
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-xs btn-primary btnSimpan" >Simpan</button>
          <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="listDetailModal" tabindex="-1" role="dialog" aria-labelledby="listDetailModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="listDetailModalLabel">Data Pesanan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container row">
            <div class="col-6">
              <div class="form-group row">
                <label for="noInvoice" class="col-md-6 col-form-label">No Invoice</label>
                <div class="col-md-6">
                  <label type="text" readonly class="col-form-label text-bold" id="no_invoice"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="noInvoice" class="col-md-6 col-form-label">Tanggal Tansaksi</label>
                <div class="col-md-6">
                  <label type="text" readonly class="col-form-label text-bold" id="tanggal_transaksi"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="noInvoice" class="col-md-6 col-form-label">Status</label>
                <div class="col-md-6">
                  <label type="text" readonly class="col-form-label text-bold" id="status_pembayaran"></label>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group row">
                <label for="noInvoice" class="col-md-6 col-form-label">Ekspedisi</label>
                <div class="col-md-6">
                  <label type="text" readonly class="col-form-label text-bold" id="ekspedisi"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="noInvoice" class="col-md-6 col-form-label">Total Harga</label>
                <div class="col-md-6">
                  <label type="text" readonly class="col-form-label text-bold" id="total_harga"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="noInvoice" class="col-md-6 col-form-label">Ongkos Kirim</label>
                <div class="col-md-6">
                  <label type="text" readonly class="col-form-label text-bold" id="ongkos_kirim"></label>
                </div>
              </div>
              <div class="form-group row">
                <label for="noInvoice" class="col-md-6 col-form-label">Total Pembayaran</label>
                <div class="col-md-6">
                  <label type="text" readonly class="col-form-label text-bold" id="total_pembayaran"></label>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-header">
            <h5 class="modal-title" id="listDetailModalLabel">Detail Pesanan</h5>
          </div>

          <!-- Table Content -->
          <div class="table-responsive-sm">
            <table class="table " id="tableDetail">
              <thead  class="thead-dark">
                <tr>
                  <th class="text-center">Product</th>
                  <th class="text-center d-none d-md-table-cell d-lg-table-cell">Quantity</th>
                  <th class="text-center d-none d-md-table-cell d-lg-table-cell">Price (IDR)</th>
                  <th class="text-center d-none d-md-table-cell d-lg-table-cell">Total Price (IDR)</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
              <tfoot>

              </tfoot>
            </table>
            <hr class="my-0">
          </div>
          <!-- End Table Content -->

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div id="detail-modal-lihat" class="modal fade"tabindex="-1" role="dialog" aria-labelledby="listDetailModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white" id="listDetailModalLabel">Pengiriman Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </div>
        <div class="modal-body form-horizontal">
          <div class="row mb-20">
              <div class="col-md-12">
                <h6 class="text-bold"><i class="icon-magazine position-left"></i> <u>Data Pengiriman Pesanan</u></h6>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label for="no_resi" class="col-md-6 col-form-label">Nomor Resi</label>
                      <div class="col-md-6">
                        <label type="text" readonly="" class="col-form-label text-bold" id="no_resi_p">-</label>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="tanggal_pengiriman" class="col-md-6 col-form-label">Tanggal Pengiriman</label>
                      <div class="col-md-6">
                        <label type="text" readonly="" class="col-form-label text-bold" id="tanggal_pengiriman">-</label>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="service" class="col-md-6 col-form-label">Service</label>
                      <div class="col-md-6">
                        <label type="text" readonly="" class="col-form-label text-bold" id="service">-</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group row">
                      <label for="dikirim_oleh" class="col-md-4 col-form-label">Dikirim Oleh</label>
                      <div class="col-md-8">
                        <label type="text" readonly="" class="col-form-label text-bold" id="dikirim_oleh">-</label>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="dikirim_ke" class="col-md-4 col-form-label">Dikirim Ke</label>
                      <div class="col-md-8">
                        <label type="text" readonly="" class="col-form-label text-bold" id="dikirim_ke">-</label>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="status_pengiriman" class="col-md-4 col-form-label">Status</label>
                      <div class="col-md-8">
                        <label type="text" readonly="" class="col-form-label text-bold" id="status_pengiriman">-</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <h6 class="text-bold"><i class="icon-magazine position-left"></i> <u>Proses Pengiriman</u></h6>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table id="table-pengiriman" class="table table-bordered">
                        <thead>
                          <tr class="bg-slate">
                            <th class="text-center" style="width: 30%;">Tanggal</th>
                            <th>Keterangan</th>
                          </tr>
                        </thead>
                        <tbody></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="modal-footer m-t-none">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->

  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#"
      data-position='{"bottom": 15, "right": 15 }'
      data-type="fixed"
      data-offset-top="400"
      data-compensation="#header"
      data-show-effect="slideInUp"
      data-hide-effect="slideOutDown">
      <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->

  <input type="hidden" name="member_id" id="member_id">
  <input type="hidden" name="sales_id" id="sales_id">
  <input type="hidden" name="aProvinsi" id="aProvinsi">
  <input type="hidden" name="aKota" id="aKota">
  <input type="hidden" name="aKecamatan" id="aKecamatan">
  <input type="hidden" name="courier" id="courier">

  <!-- JS Implementing Plugins -->
  <script src="<?php echo assets_url('vendor/hs-megamenu/src/hs.megamenu.js')?>"></script>
  <script src="<?php echo assets_url('vendor/svg-injector/dist/svg-injector.min.js')?>"></script>
  <script src="<?php echo assets_url('vendor/fancybox/jquery.fancybox.min.js')?>"></script>
  <script src="<?php echo assets_url('vendor/slick-carousel/slick/slick.js')?>"></script>
  <script src="<?php echo assets_url('vendor/jquery-validation/dist/jquery.validate.min.js')?>"></script>
  <script src="<?php echo assets_url('vendor/cubeportfolio/js/jquery.cubeportfolio.min.js')?>"></script>
  <script src="<?php echo assets_url('vendor/flatpickr/dist/flatpickr.min.js')?>"></script>

  <!-- JS Front -->
  <script src="<?php echo script_url('assets/js/hs.core.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.header.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.unfold.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.fancybox.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.slick-carousel.js'); ?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.validation.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.focus-state.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.cubeportfolio.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.svg-injector.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.quantity-counter.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.go-to.js')?>"></script>
  <script src="<?php echo script_url('assets/js/components/hs.range-datepicker.js')?>"></script>
  <!-- <script src="<?php echo script_url('assets/js/formatter/formatter.min.js')?>"></script> -->

  <script>
    let siteUplaod = "<?php echo $this->config->item('api_uri')?>";
    let siteUrl = "<?php echo site_url(); ?>";
  </script>

  <script src="<?php echo script_url('assets/js/components/hs.dropzone.js')?>"></script>
  <script type="text/javascript" src="<?php echo script_url('assets/js/components/hs.file-attach.js'); ?>"></script>


  <!-- JS Plugins Init. -->
  <script>
      $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
          event: 'hover',
          pageContainer: $('.container'),
          breakpoint: 767.98,
          hideTimeOut: 0
        });

        // initialization of HSMegaMenu component
        $('.js-breadcrumb-menu').HSMegaMenu({
          event: 'hover',
          pageContainer: $('.container'),
          breakpoint: 991.98,
          hideTimeOut: 0
        });

        // initialization of svg injector module
        $.HSCore.components.HSRangeDatepicker.init('.js-range-datepicker');
        
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
      });

      $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

        // initialization of fancybox
        $.HSCore.components.HSFancyBox.init('.js-fancybox');

        // initialization of forms
        $.HSCore.components.HSFocusState.init();

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');
        
        $.HSCore.components.HSQantityCounter.init('.js-quantity');
        
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

      });
  </script>
  <?php $this->load->view('layouts/clean-script'); ?>
  <?php $this->load->view('website/cart-script'); ?>
  <?php $this->load->view('website/akun/profile-script'); ?>
  <?php $this->load->view('website/akun/pesanan-script'); ?>
  <?php $this->load->view('website/akun/daftar-script'); ?>

  <script type="text/javascript" src="<?php echo script_url('assets/js/notifikasi.js'); ?>"></script>
</body>

</html>
