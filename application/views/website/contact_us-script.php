<script type="text/javascript">
	$(document).ready(function() {
		let form = $('#form');
		var url = {
			save: "<?php echo $this->config->item('api_uri').'/v1/contact_us/contact_us/insert' ?>",
		};

		$('.save').on('click', function(e) {
			e.preventDefault();
			$(form).submit();
		});

		$(form).validate({
		    rules: {},
		    focusInvalid: true,
		    errorPlacement: function(error, element) {
		        var placement = $(element).closest('.input-group');
		        if (placement.length > 0) {
		            error.insertAfter(placement);
		        } else {
		            error.insertAfter($(element));
		        }
		    },
		    submitHandler: function (form) {
		        $('input, textarea, select').prop('disabled', false);

		        blockPage('Sedang diproses ...');
		        var formData = $(form).serialize();
		        $.ajax({
		          data: formData,
		          type: 'POST',
		          dataType: 'JSON', 
		          url: url.save,
		          headers: {
							"Authorization": "PRIMA-uvXL68GB5THBN8cUIFuM"
							},
		          success: function(data){
		              	$.unblockUI();

						$('#AlertModal').modal('show');
						$('#dispNotif').html('Success ...');

		              	setTimeout(function () {
							$('#AlertModal').modal('hide');
				              window.location.reload();
		                }, 3000);
		          },
		          error: function(data){
		              $.unblockUI();

					  $('#AlertModal').modal('show');
					  $('#dispNotif').html('Gagal ...');
					  setTimeout(function () {
						$('#AlertModal').modal('hide');
	                }, 2000);
		          }
		        });
		        return false;
		    }
		});
	});
</script>