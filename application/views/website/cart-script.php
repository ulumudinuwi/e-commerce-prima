<script type="text/javascript">
	$(document).ready(function() {
		$('#tableCheckout').hide();

		$('#tableCart').find('.checkAll').on('click', function(){
			if ($(this).is(':checked')) {
				$('#tableCart').find('.checkList').prop('checked', true);
			}else{
				$('#tableCart').find('.checkList').prop('checked', false);
			}

			var tbody = $("#tableCart tbody"),
				tfoot = $("#tableCart tfoot");

			getCalculatePrice(tbody, tfoot);
		});

		$('#tableCart').on('click', '.checkList', function(){

			var tbody = $("#tableCart tbody"),
				tfoot = $("#tableCart tfoot");

			getCalculatePrice(tbody, tfoot);
		});

	    //btn delete cart
	    $('body').on('click', '.deleteCarts', function(){
	    	let uid = $(this).data('uid');

			$('#dispDelete').html('Are you sure you want to delete this product?');
			$('.btnDelete').data('uid', uid);
			$('.btnDelete').data('mode', 'tableCart');
			$('#confirmationModal').modal('show');
	    });

	    $('body').on('click', '.btnPilih', function(){
	    	let name = $(this).data('name'),
	    		harga = $(this).data('harga'),
	    		estimasi = $(this).data('estimasi');

	    	$('#labelEkspedisi').html(name);
	    	$('#ekspedisi').val(name);
	    	$('#labelHarga').html('Rp. '+harga);
	    	$('#totalOngkir').val(harga);
	    	$('.labelEstimasi').html('Diterima '+estimasi+' hari setelah Paket diserahkan ke kurir .');
	
			var tbody = $("#tableCheckout tbody"),
				tfoot = $("#tableCheckout tfoot");

			getCheckoutprice(tbody, tfoot);
			$('#ongkirModal').modal('hide');	
			tfoot.find('#divOngkir').val('1');
	    });

	    
	    $('body').on('click', '.btnMakeOrder', function(){
	    	let member_id = $('#member_id').val(),
				sales_id = $('#sales_id').val(),
				total_harga = $('#grandTotalPriceHidden').val();
				ekspedisi = $('#ekspedisi').val();
				ongkir = $('#totalOngkir').val();
				valOngkir = $('#divOngkir').val();
				dArray = [];

				$('#tableCheckout tbody').find('tr').each(function(){
					dArray.push({
						uid : $(this).find('.uid').val(),
						qty : $(this).find('.qty').val(),
						harga : $(this).find('.price').val(),
						total_harga : $(this).find('.totalPrice').val(),
						barang_uid : $(this).find('.barang_uid').val(),
						diskon : $(this).find('.product_diskon').val(),
					});
				})

				if (valOngkir == 0) {
					$('#AlertModal').modal('show');
					$('#dispNotif').html('Pilih Ekspedisi');
					return;
				}

			blockPage('Sedang diproses ...');
			$.ajax({
				data: {
					member_id : member_id,
					sales_id : sales_id,
					total_harga : total_harga,
					ekspedisi : ekspedisi,
					ongkir : ongkir,
					detail : dArray,
				},
				type: 'POST',
				dataType: 'JSON', 
				url: url.addCheckout,
				headers: {
					"Authorization": "PRIMA-uvXL68GB5THBN8cUIFuM"
				},
				success: function(data){
					$.unblockUI();

					$('#AlertModal').modal('show');
					$('#dispNotif').html(data.message);

					setTimeout(function () {
						$('#AlertModal').modal('hide');
        				location.assign('<?php echo site_url('akun/profile/pesanan'); ?>');
					}, 3000);
				},
				error: function(data){
					$.unblockUI();
					$('#AlertModal').modal('show');
					$('#dispNotif').html(data.responseJSON.message);
					setTimeout(function () {
						$('#AlertModal').modal('hide');
					}, 2000);
				}
			});

	    });
	});

	function setHeader(xhr) {
		xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
	}

	function fillListCarts(data, tbody) {
		var tbody = $("#tableCart tbody"),
			tfoot = $("#tableCart tfoot");

		var diskon = parseInt(data.harga) * (parseInt(data.product_diskon) / 100);
		var totalDiskon = parseInt(data.harga) - diskon;

		var tr = $("<tr/>")
		.appendTo(tbody);

		var tdCheckbox = $("<td/>")
		.appendTo(tr);
		var dispCheckbox = $("<span/>")
		.html('<input class="form-check-input checkList" type="checkbox" data-uid="'+ data.uid +'">')
		.appendTo(tdCheckbox);


		let deskripsi = data.product_deskripsi.substring(0, 20),
		product = '<div class="media">'
		+'<div class="u-avatar mr-3">'
		+'<img class="img-fluid rounded" src="'+ (data.foto.length > 0 ? data.foto[0].original : "<?php echo base_url('assets/img/others/no_image_available.png'); ?>")  +'">'
		+'</div>'
		+'<div class="media-body">'
		+'<h2 class="h6 mb-0 text-block text-primary productName">'+data.product_nama+'</h2>'
		// +'<small class="d-block text-secondary">'+deskripsi+'</small>'
		+'<small class="text-secondary d-table-cell d-md-none d-lg-none">Rp. '+numeral(totalDiskon).format()+'</small>'
		+'<div class="js-quantity input-group u-quantity mt-2">'
	        +'<input class="js-result form-control u-quantity__input" type="text" value="'+data.qty+'">'
	        +'<div class="u-quantity__arrows">'
	          +'<span class="js-plus u-quantity__arrows-inner fas fa-angle-up"></span>'
	          +'<span class="js-minus u-quantity__arrows-inner fas fa-angle-down"></span>'
	        +'</div>'
	      +'</div>'
		+'</div>'
		+'</div>';


		var tdProduct = $("<td/>")
		.appendTo(tr);
		var dispProduct = $("<span/>")
		.addClass('font-weight-bold')
		.html(product)
		.appendTo(tdProduct);
		$.HSCore.components.HSQantityCounter.init('.js-quantity');

		//harga
		var dispHiddenPrice = $("<input/>")
		.prop('type', 'hidden')
		.addClass('product_harga')
		.val(data.harga)
		.appendTo(tdProduct);

		//total harga
		var dispHiddentotalPrice = $("<input/>")
		.prop('type', 'hidden')
		.addClass('product_total_harga')
		.val(totalDiskon)
		.appendTo(tdProduct);

		//diskon
		var dispHiddendiskon = $("<input/>")
		.prop('type', 'hidden')
		.addClass('product_diskon')
		.val(data.product_diskon)
		.appendTo(tdProduct);

		//productUID
		var dispBarangUid = $("<input/>")
		.prop('type', 'hidden')
		.addClass('product_uid')
		.val(data.product_uid)
		.appendTo(tdProduct);

		let total = totalDiskon * data.qty;

		var dispBerat = $("<input/>")
		.prop('type', 'hidden')
		.addClass('inpBerat')
		.val(data.product_berat)
		.appendTo(tdProduct);
		var dispTotalhidden = $("<input/>")
		.prop('type', 'hidden')
		.addClass('inpHidden')
		.val(total)
		.appendTo(tdProduct);

		// TAMPILAN PC
		var tdPrice = $("<td/>")
		.addClass('text-right d-none d-md-table-cell d-lg-table-cell')
		.appendTo(tr);
		var dispPrice = $("<span/>")
		.addClass('font-weight-bold')
		.html(numeral(totalDiskon).format())
		.appendTo(tdPrice);		

		var tdTotal = $("<td/>")
			.addClass('text-right d-none d-md-table-cell d-lg-table-cell')
			.appendTo(tr);
		var dispTotal = $("<span/>")
		.addClass('font-weight-bold')
		.html(numeral(total).format())
		.appendTo(tdTotal);

		var tdBtnDelete = $("<td/>")
		.addClass('text-right')
		.appendTo(tr);
		var dispBtnDelete = $("<span/>")
		.addClass('font-weight-bold')
		.html('<button type="button" class="btn btn-icon btn-danger btn-sm transition-3d-hover deleteCarts" data-uid="'+data.uid+'">'
			+'<span class="fa fa-trash btn-icon__inner"></span>'
			+'</button>')
		.appendTo(tdBtnDelete);

		var plus = tdProduct.find('.js-plus'),
			minus = tdProduct.find('.js-minus'),
			qty = tdProduct.find('.js-result');

		plus.on('click', function(){
			let total = totalDiskon * qty.val();
			dispTotal.html(numeral(total).format());
			dispTotalhidden.val(total);
			getCalculatePrice(tbody, tfoot);
		})

		minus.on('click', function(){
			let total = totalDiskon * qty.val();
			dispTotal.html(numeral(total).format());
			dispTotalhidden.val(total);
			getCalculatePrice(tbody, tfoot);
		})
		getCalculatePrice(tbody, tfoot);
	};

	function fillFooter(tbody, tfoot){

		var tr = $("<tr/>")
		.appendTo(tfoot);

		var td = $("<td/>")
			.addClass('d-none d-md-table-cell d-lg-table-cell')
			.attr('colspan', '2')
			.appendTo(tr);

		var tdTotalPrice = $("<td/>")
		.attr('colspan', '3')
		.addClass('font-weight-bold text-right')
		.appendTo(tr);
		var dispTotalPrice = $("<div/>")
		.html('<!-- Total -->'
			+'<div class="media-body">'
			+'<h3 class="h5 mb-0">Grand Total Price : <label class="grandTotalPrice">0</label></h3>'
			+'<input type="hidden" id="beratBarang">'
			+'<p class="small mb-0">Subtotal for Products (<label class="itrLabel">0</label> products)</p>'
			+'<p class="small mb-0 text-bold">* Free shipping min. IDR 3,000,000 purchase.</p>'
			+'<button class="btn btn-primary transition-3d-hover btnCheckout mt-2" type="button">Checkout</button>'
			+'</div>'
			+'<!-- End Total -->')
		.appendTo(tdTotalPrice);

		dispTotalPrice.on('click', function(){
			let alamat = $('#aProvinsi').val();

			if ($('.checkList:checked').length <= 0) {
				$('#dispNotif').html('Pilih item terlebih dahulu.');	    
				$('#AlertModal').modal('show');	    
				return;
			}
			
			if (alamat == '') {
				$('#dispNotif').html('Alamat pengiriman belum terisi.');	    
				$('#AlertModal').modal('show');
		    	$('.btnEditProfile').css("display", "block");

				return;
			}

    		blockPage('Loading ...');

			var tableCart = $("#tableCart"),
				tbody = $("#tableCart tbody"),
				totalHarga = 0;


			tbody.find('tr').each(function(){
				var checkbox 	= $(this).find('.checkList');
				if (checkbox.is(':checked')) {
					let uid 	= $(this).find('.checkList').data('uid'),
						product_name = $(this).find('.productName').html(),
						product_qty = $(this).find('.js-result').val(),
						product_harga = $(this).find('.product_harga').val(),
						product_total_harga = $(this).find('.product_total_harga').val(),
						product_diskon = $(this).find('.product_diskon').val(),
						product_uid = $(this).find('.product_uid').val(),

						data = {
								uid : uid,
								product_name : product_name,
								product_qty : product_qty,
								product_harga : product_harga,
								product_total_harga : product_total_harga,
								product_diskon : product_diskon,
								product_uid : product_uid,
								};

						fillListCheckout(data);
				}
			});

			fillFooterCheckout();

			$(this).find('.btnCheckout').prop('disabled', true);
			$(this).find('.btnCheckout').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');

	        setTimeout(()=>{
	            $.unblockUI();
		        tableCart.hide();
				$('.checkoutAddress').show();
	  		    $('#tableCheckout').show();
	            $('#labelCart').html('List Checkout');
	        },2000);
		});
	}

	function fillListCheckout(data){
		var tbody = $("#tableCheckout tbody"),
			tfoot = $("#tableCheckout tfoot");

		let ckProduct = '<div class="media">'
			+'<div class="media-body">'
			+'<h2 class="h6 mb-0 text-block text-primary">'+data.product_name+'</h2>'
			+'<small class="text-secondary mb-3 d-table-cell d-md-none d-lg-none">Rp. '+numeral(data.product_total_harga).format()+'</small>'
		+'</div>';

		var tr = $("<tr/>")
		.appendTo(tbody);

		//Product Name
		var tdProduct = $("<td/>")
		.appendTo(tr);
		var dispProduct = $("<span/>")
		.addClass('font-weight-bold')
		.html(ckProduct)
		.appendTo(tdProduct);
		var dispUidhidden = $("<input/>")
		.prop('type', 'hidden')
		.addClass('uid')
		.val(data.uid.trim() != "" ? data.uid : '-')
		.appendTo(tdProduct);

		//Product Qty
		var tdQty = $("<td/>")
		//.addClass('text-center')
		.appendTo(tr);
		var dispQty = $("<span/>")
		.addClass('font-weight-bold')
		.html(data.product_qty.trim() != "" ? data.product_qty : '-')
		.appendTo(tdQty);
		var dispQtyhidden = $("<input/>")
		.prop('type', 'hidden')
		.addClass('qty')
		.val(data.product_qty.trim() != "" ? data.product_qty : '-')
		.appendTo(tdQty);

		//trHarga
		var tdPrice = $("<td/>")
		.addClass('text-right d-none d-md-table-cell d-lg-table-cell')
		.appendTo(tr);
		var dispPrice = $("<span/>")
		.addClass('font-weight-bold')
		.html(data.product_total_harga.trim() != "" ? numeral(data.product_total_harga).format() : '-')
		.appendTo(tdPrice);

		//harga hidden
		var dispPricehidden = $("<input/>")
		.prop('type', 'hidden')
		.addClass('price')
		.val(data.product_harga)
		.appendTo(tdPrice);

		//totalHarga
		var disptotalPricehidden = $("<input/>")
		.prop('type', 'hidden')
		.addClass('totalPrice')
		.val(data.product_total_harga)
		.appendTo(tdPrice);

		//diskon
		var dispDiskonhidden = $("<input/>")
		.prop('type', 'hidden')
		.addClass('product_diskon')
		.val(data.product_diskon)
		.appendTo(tdPrice);

		//barangUid
		var dispBarangUid = $("<input/>")
		.prop('type', 'hidden')
		.addClass('barang_uid')
		.val(data.product_uid)
		.appendTo(tdPrice);

		let total = parseFloat(data.product_total_harga) * parseFloat(data.product_qty);

		var tdTotal = $("<td/>")
		.addClass('text-right d-none d-md-table-cell d-lg-table-cell')
		.appendTo(tr);
		var dispTotal = $("<span/>")
		.addClass('font-weight-bold')
		.html(numeral(total).format())
		.appendTo(tdTotal);
		var dispTotalhidden = $("<input/>")
		.prop('type', 'hidden')
		.addClass('inpHidden')
		.val(total)
		.appendTo(tdTotal);
	}

	function fillFooterCheckout(){
		var tbody = $("#tableCheckout tbody"),
			tfoot = $("#tableCheckout tfoot");

		var tr = $("<tr/>")
		.appendTo(tfoot);

		var tdlabelOngkir = $("<td/>")
		.addClass('text-secondary')
		.appendTo(tr);
		var displabelOngkir = $("<span/>")
		.html('* Free shipping min. IDR 3,000,000 purchase.')
		.appendTo(tdlabelOngkir);

		var tdOngkir = $("<td/>")
		.addClass('text-secondary')
		.appendTo(tr);
		var dispOngkir = $("<span/>")
		.html('<div class="col-md-12 divOngkir">'
                +'<div class="row">'
                  +'<div class="col-8">'
                    +'<h5 class="h5 mb-1">'
                      +'<span class="fa fa-truck"></span> '
                      +'<label id="labelEkspedisi"></label> - <label id="labelHarga"></label>'
                      +'<input type="hidden" id="ekspedisi"value="">'
                      +'<input type="hidden" id="totalOngkir"value="0">'
                      +'<input type="hidden" id="divOngkir" value="0">'
                    +'</h5>'
                  +'</div>'
                  +'<div class="col-4 text-right">'
                    +'<button type="button" class="btn btn-success btn-sm" id="btnOngkir">Pilih</button>'
                  +'</div>'
                +'</div>'
                +'<div class="mb-1">'
                    +'<label class="text-secondary small labelEstimasi">-</label>'
                +'</div>'
                +'</div>')
		.appendTo(tdOngkir);

		var tdTotalPrice = $("<td/>")
		.attr('colspan', '2')
		.addClass('font-weight-bold text-right')
		.appendTo(tr);
		var dispTotalPrice = $("<div/>")
		.html('<!-- Total -->'
			+'<div class="col-md-12">'
			+'<div class="media-body">'
			+'<div class="mr-4">'
			+'<h3 class="h5 mb-0">Grand Total Price : <label class="grandTotalPrice">0</label></h3>'
            +'<input type="hidden" id="grandTotalPriceHidden" value="0">'
			+'<p class="small mb-0">Subtotal for Products (<label class="itrLabel">0</label> products)</p>'
			+'</div>'
			+'<div class="media-body">'
			+'<button class="btn btn-primary transition-3d-hover btnMakeOrder" type="button">Make Order</button>'
			+'</div>'
			+'</div>'
			+'</div>'
			+'<!-- End Total -->')
		.appendTo(tdTotalPrice);

		dispOngkir.on('click', function(){
			getOngkir(tbody, tfoot);
		});

		getCheckoutprice(tbody, tfoot);
	}

	function getCalculatePrice(tbody, tfoot){
		let totalHarga = 0,
			beratBarang = 0,
			iTr = 0;

		tbody.find('tr').each(function(){
			var checkbox 	= $(this).find('.checkList'),
				total 		= parseFloat($(this).find('.inpHidden').val());
				berat 		= parseFloat($(this).find('.inpBerat').val());

			if (checkbox.is(':checked')) {
				totalHarga += total;
				beratBarang += berat;
				iTr += 1;
			}
		});

		tfoot.find('.grandTotalPrice').html(numeral(totalHarga).format());
		tfoot.find('#beratBarang').val(beratBarang);
		tfoot.find('.itrLabel').html(iTr);
	}

	function getCheckoutprice(tbody, tfoot){
		let totalOngkir = $('#totalOngkir').val(),
		 	totalHarga = 0,
			subtotalHarga = 0,
			iTr = 0;

		tbody.find('tr').each(function(){
			var total = parseFloat($(this).find('.inpHidden').val());
				subtotalHarga += total;
				iTr += 1;
		});
		
		totalHarga = parseFloat(subtotalHarga) + parseFloat(totalOngkir);
		
		if (totalHarga >= '3000000') {
			tfoot.find('#divOngkir').val('1');
			tfoot.find('.divOngkir').hide();
		}

		tfoot.find('.grandTotalPrice').html(numeral(totalHarga).format());
		tfoot.find('#grandTotalPriceHidden').val(totalHarga);
		tfoot.find('.itrLabel').html(iTr);
	}

	function getOngkir(tbody, tfoot){
		//Mengambil value dari option select kota kemudian parameternya dikirim menggunakan ajax 
	    var city = $('#aKota').val();
	    var district = $('#aKecamatan').val();
	    var courier = $('#courier').val();
	    var weight = $('#beratBarang').val();

	    data =  { origin : city,
	    		  originType : 'city',
	    		  destination : district,
	    		  destinationType : 'subdistrict',
	    		  courier : courier,
	    		  weight : weight,
	    		};

	    $.ajax({
	        type : 'POST',
	        url : site_url+'/akun/profile/getCost',
	        data : data,
	        dataType : 'json',
	        success: function (data) {
	          let results = data.rajaongkir.results;

	   		  $('#ongkirModal').find('#paymentDetails').empty();

	          for (var i = 0; i < results.length; i++) {
	            fillOngkir(results[i], i);
	          }


	        }
	    });

		$('#ongkirModal').modal('show');	
	}

	function fillOngkir(data, index){
		let tmp = "";
		if (data) {
			tmp += '<!-- Card -->'
					 +'<div class="card">'
					  +'<div class="card-header card-collapse" id="cardHeadingOne">'
					    +'<h5 class="mb-0">'
					      +'<button class="btn btn-link btn-block card-btn collapsed p-3" role="button"'
					      +'data-toggle="collapse"'
					      +'data-target="#'+data.code+'"'
					      +'aria-expanded="false"'
					      +'aria-controls="'+data.code+'">'
					      +'<span class="row align-items-center">'
					        +'<span class="col-md-10 mb-2 mb-md-0">'
					          +'<span class="media align-items-center">'
					            +'<span class="media-body">'
					              +'<span class="font-size-1">'+ data.code.toUpperCase() +'</span>'
					              +' | <span class="font-size-1">'+ data.name +'</span>'
					            +'</span>'
					          +'</span>'
					        +'</span>'
					        +'<span class="col-2 col-md-2 text-right">'
					          +'<span class="card-btn-arrow">'
					            +'<span class="fas fa-arrow-down small"></span>'
					          +'</span>'
					        +'</span>'
					      +'</span>'
					    +'</button>'
					  +'</h5>'
					+'</div>'

					+'<div id="'+data.code+'" class="collapse" aria-labelledby="cardHeadingOne" data-parent="#paymentDetails">'
					  +'<div class="card-body px-4">'
					  + fillOngkirDetail(data)
					  +'</div>'
					+'</div>'
					+'</div>'
					+'<!-- End Card -->'
		}

		$('#ongkirModal').find('#paymentDetails').append(tmp);
	}

	function fillOngkirDetail(data){
		costs = data.costs;
		data = data;

		tmp = '';

		for (var i = 0; i < costs.length; i++) {  
        	tmp +='<!-- Card Details -->'
			    +'<div class="row mb-1">'
			      +'<div class="col-sm-6 mb-2 mb-sm-0">'
			        +'<span class="h6 mb-1">'+costs[i].service+'</span>'
			        +' <span class="small">('
			          +costs[i].description
			        +')</span>'
			      +'</div>'
					+ fillOngkirHarga(costs[i].cost, data)
			    +'</div>'
			    +'<!-- End Card Details -->';	
      	}

		return tmp;
	}

	function fillOngkirHarga(cost, data){
		tmp = '';

		for (var i = 0; i < cost.length; i++) {  
        	tmp +='<div class="col-sm-6 mb-2 mb-sm-0">'
        			+'<button class="card card-frame btnPilih" data-name="'+data.code.toUpperCase()+'" data-harga="'+cost[i].value+'" data-estimasi="'+cost[i].etd+'">'
			            +'<div class="card-body p-2">'
			              +'<!-- Icon Block -->'
			              +'<div class="media">'
			                +'<div class="media-body">'
			                  +'<p class="small">Diterima '+ cost[i].etd+' hari setelah Paket diserahkan ke kurir.</p>'
				        		+'<h4 class="h6">Rp. '+cost[i].value+'</h4>'
			                +'</div>'
			              +'</div>'
			              +'<!-- End Icon Block -->'
			            +'</div>'
			        +'</button >'	
			      +'</div>';
      	}

		return tmp;
	}

	$('.btnEditProfile').on('click', function(e) {
		e.preventDefault();
		location.assign('<?php echo site_url('akun/profile/alamat'); ?>');
	});

</script>