  	<!-- ========== MAIN ========== -->
  	<main id="content" role="main">
	    <!-- Hero v1 Section -->
	    <div class="u-hero-v1 space-2">
	      <!-- Hero Carousel Main -->
	      <div id="heroNav" class="js-slick-carousel u-slick"
	           data-autoplay="false"
	           data-infinite="true"
	           data-speed="10000"
	           data-adaptive-height="true"
	           data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
	           data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
	           data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
	           data-numbered-pagination="#slickPaging"
	           data-nav-for="#heroNavThumb">

	           <!-- Gluta Optime -->
		       <div class="js-slide">
		          <!-- Slide #1 -->
		          <div class="d-lg-flex align-items-lg-center u-hero-v1__main" style="background-color: white">
		            <div class="container space-top-md-5 space-top-lg-3">
		              <div class="row">
		               
					    <!-- Front in Frames Section -->
					    <div class="overflow-hidden">
					      <div class="container space-1 space-md-2">
					        <div class="row justify-content-between align-items-center">

					          <div class="col-lg-5 position-relative">
					            <!-- Image Gallery -->
					            <div class="row mx-gutters-2">
					              <div class="col-12"  align="center">
					                  <img class="img-fluid rounded" src="<?php echo assets_url('img/others/gluta_optime_bc.png')?>" alt="Image Description">
					              </div>
					            </div>
					            <!-- End Image Gallery -->

					          </div>
					          <div class="col-lg-6 mb-7 mb-lg-0">
					            <div class="pr-md-4">
					              <!-- Title -->
					              <div class="mb-7 text-justify">
						                <h2 class="d-block  mb-2 text-primary text-bold"
					                        data-scs-animation-in="fadeInUp">
					                    	<span class="font-weight-semi-bold">GLUTA OPTIME</span> 
					                  	</h2>
					                
					                  <p class="text-black-70 mb-2"
					                      data-scs-animation-in="fadeInUp"
					                      data-scs-animation-delay="200">
					                    Formulasi yang menyehatkan dari glutation dengan dengan vitamin C, Alpha lipoic acid, Haematoccocus pluvialis sebagai astaxanthin dan zinc. Menawarkan supplement bersinergi yang menghasilkan efek anti virus sambil menjaga tubuh anda detoksifikasi dan sehat.
					                  </p>

					                  <h4 class="d-block mb-2 text-white text-bold"
					                        data-scs-animation-in="fadeInUp">
					                        <span class="btn btn-sm btn-soft-info mb-2 font-weight-semi-bold">POM SD 192 356 031</span>
					                  </h4>

					                  <p data-scs-animation-in="fadeInUp"
					                      data-scs-animation-delay="300"
					                      class="text-center">
					                      <a class="btn btn-sm text-white bg-info btn-wide transition-3d-hover" href="<?php echo site_url('product/explore_product/gluta') ?>">Explore <span class="fas fa-angle-right ml-2"></span></a>
					                  </p>
					              </div>
					            </div>
					          </div>
					        </div>
					      </div>
					    </div>
		              </div>
		            </div>
		          </div>
		          <!-- End Slide #1 -->
		       </div>

	           <!-- Gluta Optime -->
		       <div class="js-slide">
		          <!-- Slide #1 -->
		          <div class="d-lg-flex align-items-lg-center u-hero-v1__main" style="background-color: white">
		            <div class="container space-top-md-5 space-top-lg-3">
		              <div class="row">
		               
					    <!-- Front in Frames Section -->
					    <div class="overflow-hidden">
					      <div class="container space-1 space-md-2">
					        <div class="row justify-content-between align-items-center">

					          <div class="col-lg-5 position-relative">
					            <!-- Image Gallery -->
					            <div class="row mx-gutters-2">
					              <div class="col-12"  align="center">
					                  <img class="img-fluid rounded" src="<?php echo assets_url('img/others/male_optime_bc.png')?>" alt="Image Description">
					              </div>
					            </div>
					            <!-- End Image Gallery -->

					          </div>
					          <div class="col-lg-6 mb-7 mb-lg-0">
					            <div class="pr-md-4">
					              <!-- Title -->
					              <div class="mb-7 text-justify">
					                  <h2 class="d-block  mb-2 text-primary text-bold"
					                        data-scs-animation-in="fadeInUp">
					                    <span class="font-weight-semi-bold">MALE OPTIME</span> 
					                  </h2>
					                  <p class="text-black70 mb-2"
					                      data-scs-animation-in="fadeInUp"
					                      data-scs-animation-delay="200">
					                    Formulasi herbal muira puima, butea superba, dan Horny goat weed untuk membantu meningkatkan stamina dan sperma serta memperlancar sirkulasi darah dan meningkatkan masa otot.
					                  </p>
					                  <h4 class="d-block mb-2 text-danger text-bold"
					                        data-scs-animation-in="fadeInUp">
					                    <span class="btn btn-sm btn-soft-info mb-2 font-weight-semi-bold">POM TR 192 331 911</span> 
					                  </h4>
					                  <p data-scs-animation-in="fadeInUp"
					                      data-scs-animation-delay="300"
					                      class="text-center">
					                      <a class="btn btn-sm text-white bg-info btn-wide transition-3d-hover" href="<?php echo site_url('product/explore_product/male_optime') ?>">Explore <span class="fas fa-angle-right ml-2"></span></a>
					                  </p>
					              </div>
					            </div>
					          </div>
					        </div>
					      </div>
					    </div>
		              </div>
		            </div>
		          </div>
		          <!-- End Slide #1 -->
		       </div>

	           <!-- Fesklin -->
		       <div class="js-slide">
		          <!-- Slide #1 -->
		          <div class="d-lg-flex align-items-lg-center u-hero-v1__main" style="background-color: white">
		            <div class="container space-top-md-5 space-top-lg-3">
		              <div class="row">
		               
					    <!-- Front in Frames Section -->
					    <div class="overflow-hidden">
					      <div class="container space-1 space-md-2">
					        <div class="row justify-content-between align-items-center">

					          <div class="col-lg-5 position-relative">
					            <!-- Image Gallery -->
					            <div class="row mx-gutters-2">
					              <div class="col-12" align="center">
					                  <img class="img-fluid rounded" src="<?php echo assets_url('img/others/fesklin_bc.png')?>" alt="Image Description">
					              </div>
					            </div>
					            <!-- End Image Gallery -->

					          </div>
					          <div class="col-lg-6 mb-7 mb-lg-0">
					            <div class="pr-md-4">
					              <!-- Title -->
					              <div class="mb-7 text-justify">
					                  <h2 class="d-block  mb-2 text-primary text-bold"
				                        data-scs-animation-in="fadeInUp">
				                    <span class="font-weight-semi-bold">Fesklin</span> 
				                  </h2>
				                  <p class="text-black70 mb-2"
				                      data-scs-animation-in="fadeInUp"
				                      data-scs-animation-delay="200">
				                    Formulasi herbal chasteberry, artichoke, kunyit, milk thistle dan brokoli untuk memelihara Kesehatan kulit dan meyembuhkan jerawat. Produk ini dapat menyeimbangkan hormone dan menghambat radikal bebas dari sinar UV.
				                  </p>
				                  <h4 class="d-block mb-2 text-danger text-bold"
				                        data-scs-animation-in="fadeInUp">
				                    <span class="btn btn-sm btn-soft-info mb-2 font-weight-semi-bold">POM TR 182 320 551</span> 
				                  </h4>
				                  <p data-scs-animation-in="fadeInUp"
				                      data-scs-animation-delay="300">
				                      <a class="btn btn-sm text-white bg-info btn-wide transition-3d-hover" href="<?php echo site_url('product/explore_product/fesklin') ?>">Explore <span class="fas fa-angle-right ml-2"></span></a>
				                  </p>
					              </div>
					            </div>
					          </div>
					        </div>
					      </div>
					    </div>
		              </div>
		            </div>
		          </div>
		          <!-- End Slide #1 -->
		       </div>
	      </div>
	      <!-- End Hero Carousel Main -->

	      <!-- Slick Paging -->
	      <div class="container position-relative">
	        <div id="slickPaging" class="u-slick__paging"></div>
	      </div>
	      <!-- End Slick Paging -->

	      <!-- Hero Carousel Secondary -->
	      <div id="heroNavThumb" class="js-slick-carousel u-slick"
	           data-autoplay="true"
	           data-infinite="true"
	           data-speed="10000"
	           data-is-thumbs="true"
	           data-nav-for="#heroNav">

	      </div>
	      <!-- End Hero Carousel Secondary -->
	    </div>
	    <!-- End Hero v1 Section -->

	    <!-- Front in Frames Section -->
	    <div class="overflow-hidden" id="aboutus">
	      <div class="container">
	        <div class="row justify-content-between align-items-center">

	          <div class="col-lg-5 position-relative">
	            <!-- Image Gallery -->
	            <div class="row mx-gutters-2">
	              <div class="col-12">
	                <!-- Fancybox -->
	                <a class="js-fancybox u-media-viewer" href="javascript:;"
	                   data-src="<?php echo assets_url('img/others/home_about.png')?>"
	                   data-fancybox="lightbox-gallery-hidden"
	                   data-caption="Front in frames - image #02"
	                   data-speed="700">
	                  <img class="img-fluid rounded" src="<?php echo assets_url('img/others/home_about.png')?>" alt="Image Description">

	                  <span class="u-media-viewer__container">
	                    <span class="u-media-viewer__icon">
	                      <span class="fas fa-plus u-media-viewer__icon-inner"></span>
	                    </span>
	                  </span>
	                </a>
	                <!-- End Fancybox -->
	              </div>
	            </div>
	            <!-- End Image Gallery -->

	          </div>
	          <div class="col-lg-6 mb-7 mb-lg-0">
	            <div class="pr-md-4">
	              <!-- Title -->
	              <div class="mb-7">
	                <h1 class="font-weight-bold text-primary pCenter">Our Mission</h1>
	                <p class="text-bold ">We deliver our commitment by providing you with the health and beauty solutions to build the best of you. The fate of our well-being can only rests in our own hands and the time has come for you to take control of it.</p>
	                <h4 class="text-bold">Experience the real change in you.</h4>
	              </div>

	            </div>
	          </div>
	        </div>
	      </div>
	    </div>

	    <!-- Blog Grid Section -->
	    <div class="bg-light space-1 space-md-2">
	      <!-- Title -->
	      <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
	        <h2 class="text-primary"><span class="font-weight-semi-bold ">Product</span></h2>
	          <hr class="my-0">
	      </div>
	      <!-- End Title -->

	      <div id="shopItemsContentBpom" class="container">

			  <div class="cbp appenItemLogin"
			       data-layout="grid"
			       data-controls="#filterControls"
			       data-animation="quicksand"
			       data-x-gap="32"
			       data-y-gap="32"
			       data-media-queries='[
			        {"width": 1500, "cols": 4},
			        {"width": 1100, "cols": 3},
			        {"width": 800, "cols": 3},
			        {"width": 480, "cols": 2},
			        {"width": 300, "cols": 1}]'>

			  </div>
	      </div>

	      <!-- End Shop Items Section -->
	    </div>
	    <!-- End Blog Grid Section -->

	    <!-- Team Section -->
	    <div class="container space-1 space-md-2">
	      <!-- Title -->
	      <div class="w-md-80 w-lg-50 text-center mx-md-auto">
	        <h2 class="text-primary"><span class="font-weight-semi-bold">News & Promotions</span></h2>
	          <hr class="my-0">
	      </div>
	      <!-- End Title -->

	      <!-- Slick Carousel -->
	      
	    <!-- Blog Modern Section -->
	    <div class="container space-1">
	      <div class="row mx-gutters-2 appendItem">

	      </div>
	    </div>
	    <!-- End Blog Modern Section -->

	      </div>
	      <!-- End Slick Carousel -->
	    </div>
	    <!-- End Team Section -->
	</main>
	<!-- ========== END MAIN ========== -->

	<?php $this->load->view('website/home-script'); ?>