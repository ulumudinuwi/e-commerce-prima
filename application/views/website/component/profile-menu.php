<style type="text/css">
  #profile-navbar-brand {
    width: 20%;
  }
  @media (max-width: 997px) {
    #profile-navbar-brand {
      width: 30%;
    }
  }
  @media (max-width: 576px) {
    #profile-navbar-brand {
      width: 60%;
    }
  }
</style>
<div class="space-top-lg-0 space-top-md-2 space-top-sm-2 space-top-1 space-bottom-lg-0">
  <div class="bg-header">
    <div class="d-lg-flex justify-content-lg-between align-items-lg-center container">
      <!-- Navbar -->
      <div class="u-header u-header-left-aligned-nav u-header--bg-transparent-lg u-header--white-nav-links z-index-4">
        <div class="u-header__section bg-transparent">
          <nav class="js-breadcrumb-menu navbar navbar-expand-lg u-header__navbar u-header__navbar--no-space space-1 space-lg-0">
            <div id="profile-navbar-brand" class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center">
              <!-- Breadcrumb -->
              <ol class="breadcrumb breadcrumb-white breadcrumb-no-gutter mb-0">
                <li class="breadcrumb-item"><a class="breadcrumb-link" href="#">Akun</a></li>
                <?php if(isset($page_title)): ?>
                  <li class="breadcrumb-item active" aria-current="page"><?php echo $page_title; ?></li>
                <?php endif; ?>
              </ol>
              <!-- End Breadcrumb -->
            </div>
            <div class="d-lg-none">
              <button type="button" class="navbar-toggler btn u-hamburger u-hamburger--white"
                      aria-label="Toggle navigation"
                      aria-expanded="false"
                      aria-controls="breadcrumbNavBar"
                      data-toggle="collapse"
                      data-target="#breadcrumbNavBar">
                <span id="breadcrumbHamburgerTrigger" class="u-hamburger__box">
                  <span class="u-hamburger__inner"></span>
                </span>
              </button>
            </div>

            <div id="breadcrumbNavBar" class="collapse navbar-collapse u-header__navbar-collapse">
              <ul class="navbar-nav u-header__navbar-nav">
                <!-- Akun -->
                <li class="nav-item hs-has-sub-menu u-header__nav-item mt-2 mt-md-0 mt-lg-0"
                    data-event="hover"
                    data-animation-in="slideInUp"
                    data-animation-out="fadeOut">
                  <a id="generalDropdown" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="generalDropdownMenu">
                    Akun Saya
                  </a>

                  <ul id="generalDropdownMenu" class="hs-sub-menu u-header__sub-menu u-header__sub-menu--spacer" style="min-width: 230px;" aria-labelledby="generalDropdown">
                    <li><a class="nav-link u-header__sub-menu-nav-link" href="<?php echo site_url('akun/profile') ?>">Profil</a></li>
                    <li><a class="nav-link u-header__sub-menu-nav-link" href="<?php echo site_url('akun/profile/alamat') ?>">Alamat</a></li>
                    <li><a class="nav-link u-header__sub-menu-nav-link" href="<?php echo site_url('akun/profile/atur_password') ?>">Atur Password</a></li>
                  </ul>
                </li>
                <!-- Akun -->

                <!-- Pesanan -->
                <li class="nav-item u-header__nav-item">
                  <a class="nav-link u-header__nav-link" href="<?php echo site_url('akun/profile/pesanan') ?>" >
                    Pesanan Saya
                  </a>
                </li>
                <!-- Pesanan -->

                <!-- Point -->
                <!-- li class="nav-item u-header__nav-item">
                  <a class="nav-link u-header__nav-link" href="<?php echo site_url('akun/profile/point') ?>" >
                    Point
                  </a>
                </li> -->
                <!-- Point -->

              </ul>
            </div>
          </nav>
        </div>
      </div>
      <!-- End Navbar -->
    </div>
  </div>
</div>