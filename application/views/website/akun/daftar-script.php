<script type="text/javascript">
   let formRegister  = $('#formDaftar');
   
	function imgPreview(el, target) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(target).attr('src', e.target.result)
        };
        reader.readAsDataURL(el[0].files[0]);
    }

	$(document).ready(function() {
		getSales();
	});

	function getSales(){
		$.ajax({
          	url: url.getSales,
          	type: 'GET',
          	dataType: 'json',
          	success: function(res) { 
          		data = res;

          		$('#kode_promo').html('<option value="">Pilih</option>');
          
		          for (var i = 0; i < data.length; i++) {
		            let option = '<option value="'+data[i].kode_promo+'" data-sales_id="'+data[i].id+'">'+data[i].kode_promo+'</option>'
		            $('#kode_promo').append(option);
		          }

          	},
          	error: function() { 
          		console.log('error');
          	},
          	beforeSend: setHeader
        });
	};

	$('#kode_promo').on('change', function(){
		let kode_promo = $(this).val();
			sales_id = $(this).children('option:selected').data('sales_id');
			console.log(sales_id);

        $('#kode_promo_tmp').val(kode_promo);
        $('#sales_id').val(sales_id);
	});
	
	$('#foto_sip').on('change', function() {
		imgPreview($(this), '#disp_foto_sip');
	});

	function setHeader(xhr) {
		xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
	}

	$('.saveDaftar').on('click', function(e) {
		e.preventDefault();
		$(formRegister).submit();
	});

	$(formRegister).validate({
	    rules: {},
	    focusInvalid: true,
	    errorPlacement: function(error, element) {
	        var placement = $(element).closest('.input-group');
	        if (placement.length > 0) {
	            error.insertAfter(placement);
	        } else {
	            error.insertAfter($(element));
	        }
	    },
	    submitHandler: function (formRegister) {
	        $('input, textarea, select').prop('disabled', false);

	        blockPage('Sedang diproses ...');
	        var postData = $(formRegister).serializeArray();
            var formData = new FormData($(formRegister)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'foto_sip') {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

            $.ajax({
	          data: formData,
	          type: 'POST',
	          dataType: 'JSON', 
			  processData: false,
              contentType: false, 
	          url: url.registermember,
	          headers: {
						"Authorization": "PRIMA-uvXL68GB5THBN8cUIFuM"
						},
	          success: function(data){
	              	$.unblockUI();

					$('#AlertModal').modal('show');
					$('#AlertModal').find('#AlertModalLabel').html('Terima kasih.');
					$('#dispNotif').html('Silahkan periksa email anda untuk melakukan konfirmasi pendaftaran.');
	              	setTimeout(function () {
						$('#AlertModal').modal('hide');
			            location.assign("<?php echo site_url() ?>");
	                }, 3000);
	          },
	          error: function(data){
	              $.unblockUI();
	              console.log(data);
				  $('#AlertModal').modal('show');
				  $('#dispNotif').html(data.responseJSON.message);
				  setTimeout(function () {
					$('#AlertModal').modal('hide');
                }, 2000);
	          }
	        });
	        return false;
	    }
	});
</script>