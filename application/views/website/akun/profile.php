<!-- ========== MAIN ========== -->
<main id="content" role="main">
  <div class="container space-top-3 space-bottom-lg-0"></div>
  <!-- Menu Header 2 -->    
  <?php $this->load->view('website/component/profile-menu'); ?>

  <!-- Content Section -->
  <div class="bg-light">
    <form class="js-validate" id="formProfile">
      <div class="container space-2">
        <div class="shadow-primary-lg rounded pt-4 pb-5 px-5 mb-5">
          <div class="row justify-content-md-between">
            <div class="col-md-6 mb-7 mb-md-0">
              <div class="pr-md-4">

                <!-- Update Avatar Form -->
                <div class="media align-items-center mb-7">
                  <div class="u-lg-avatar mr-3">
                    <img class="img-fluid rounded-circle img-profile" src="../../assets/img/others/no_image_available.png" alt="Image Description">
                  </div>

                  <div class="media-body">
                    <div class="media-body mb-2">
                      <h1 class="h3 text-dark font-weight-medium mb-1" id="profileName"></h1>
                      <span class="d-block text-dark" id="profileEmail"></span>
                    </div>
                    <label class="btn btn-sm btn-primary transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1" for="btnImageProfile">
                      Upload New Picture
                      <input id="btnImageProfile" name="image_profile" type="file" class="file-attachment-btn__label">
                    </label>
                  </div>
                </div>
                <!-- End Update Avatar Form -->  
              </div>
            </div>

            <div class="col-md-6 col-lg-4">
              <!-- Stats -->
              <div class="bg-header shadow-primary-lg rounded pt-4 pb-5 px-5">
                <!-- Title & Settings -->
                <div class="d-flex justify-content-between align-items-center">
                  <h4 class="h6 text-white mb-0">Point</h4>
                </div>
                <!-- End Title & Settings -->

                <hr class="opacity-md mt-1 mb-2">

                <!-- Referral Info -->
                <div class="py-2">
                  <div class="row">
                    <div class="col-6">
                      <div class="mb-1">
                        <span class="text-white font-size-3 font-weight-medium text-lh-sm" id="profilePoint"></span>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End Referral Info -->
              </div>
              <!-- End Stats -->
            </div>
          </div>
        </div>

        <!-- Personal Info Form -->
        <div class="shadow-primary-lg rounded pt-4 pb-5 px-5 mb-5">    
          <div class="row">
            <!-- Input -->
            <div class="col-sm-6 mb-1">
              <div class="js-form-message">
                <label id="nameLabel" class="form-label">
                  Name
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <input type="text" class="form-control name" id="name" name="name" value="" placeholder="Nama" aria-label="Nama" required aria-describedby="nameLabel"
                  data-msg="Nama"
                  data-error-class="u-has-error"
                  data-success-class="u-has-success">
                  <small class="form-text text-muted">Displayed on your public profile, notifications and other places.</small>
                </div>
              </div>
            </div>
            <!-- End Input -->

            <!-- Input -->
            <div class="col-sm-6 mb-1">
              <div class="js-form-message">
                <label id="ktpLabel" class="form-label">
                  No. KTP
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <input type="text" class="form-control no_ktp" id="no_ktp" name="no_ktp" value="" placeholder="No. KTP" aria-label="No. KTP" required aria-describedby="ktpLabel"
                  data-msg="No. KTP"
                  data-error-class="u-has-error"
                  data-success-class="u-has-success">
                </div>
              </div>
            </div>
            <!-- End Input -->
          </div>

          <div class="row">
            <!-- Input -->
            <div class="col-sm-6 mb-1">
              <div class="js-form-message">
                <label id="emailLabel" class="form-label">
                  Email address
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <input type="email" class="form-control email" id="email" name="email" value="" placeholder="Email" aria-label="Email" required aria-describedby="emailLabel"
                  data-msg="Email"
                  data-error-class="u-has-error"
                  data-success-class="u-has-success">
                  <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
              </div>
            </div>
            <!-- End Input -->


            <!-- Input -->
            <div class="col-sm-6 mb-1">
              <div class="js-form-message">
                <label id="phoneNumberLabel" class="form-label">
                  Phone number
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <input class="form-control phone_number" id="phone_number" type="tel" name="phone_number" value="" placeholder="No. Handphone" aria-label="No. Handphone" required aria-describedby="phoneNumberLabel"
                  data-msg="No. Handphone"
                  data-error-class="u-has-error"
                  data-success-class="u-has-success">
                </div>
              </div>
            </div>
            <!-- End Input -->
          </div>

          <div class="row">
            <!-- Input -->
            <div class="col-sm-6 mb-1">
              <div class="js-form-message">
                <label id="nama_klinikLabel" class="form-label">
                  Nama Klinik
                </label>

                <div class="form-group">
                  <input type="text" class="form-control nama_klinik" id="nama_klinik" name="nama_klinik" value="" placeholder="Nama Klinik" aria-label="Nama Klinik" required aria-describedby="nama_klinikLabel"
                  data-msg="Nama Klinik"
                  data-error-class="u-has-error"
                  data-success-class="u-has-success">
                </div>
              </div>
            </div>
            <!-- End Input -->

            <!-- Input -->
            <div class="col-sm-6 mb-1">
              <div class="js-form-message">
                <label id="websiteLabel" class="form-label">
                  Jenis Kelamin
                  <span class="text-danger">*</span>
                </label>

                <div class="form-group">
                  <select class="custom-select jenis_kelamin" id="jenis_kelamin" name="jenis_kelamin">
                    <option value="">Pilih Jenis Kelamin</option>
                    <option value="1">Laki - Laki</option>
                    <option value="0">Perempuan</option>
                  </select>
                </div>
              </div>
            </div>
            <!-- End Input -->
          </div>


          <label class="form-label">
            Tempat Tanggal Lahir
            <span class="text-danger">*</span>
          </label>

          <div class="row">
            <!-- Input -->
            <div class="col-md-6 mb-3 mb-sm-6">
              <div class="js-form-message">
                <div class="form-group">
                  <input type="text" class="form-control tempat_lahir" id="tempat_lahir" name="tempat_lahir" value="" placeholder="Tempat Lahir" aria-label="Tempat Lahir" required aria-describedby="tempat_lahirLabel"
                  data-msg="Tempat Lahir"
                  data-error-class="u-has-error"
                  data-success-class="u-has-success">
                </div>
              </div>
            </div>
            <!-- End Input -->

            <!-- Input -->
            <div class="col-md-6 mb-3 mb-sm-6">
              <!-- Datepicker -->
              <div id="datepickerWrapperFrom" class="u-datepicker input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <span class="fas fa-calendar"></span>
                  </span>
                </div>
                <input class="js-range-datepicker form-control bg-transparent rounded-right tanggal_lahir" id="tanggal_lahir" name="tanggal_lahir" type="text" placeholder="Tanggal Lahir" aria-label="Tanggal Lahir"
                data-rp-date-format="d/m/Y">
                <!-- <input type="text" class="form-control bg-transparent rounded-right tanggal_lahir" id="tanggal_lahir" name="tanggal_lahir"> -->
              </div>
              <!-- <small class="text-danger"></small> -->
              <!-- End Datepicker -->
            </div>
            <!-- End Input -->
          </div>

          <div class="row">
            <!-- Input -->
            <div class="col-sm-6 mb-1">
              <div class="js-form-message">
                <label id="no_sipLabel" class="form-label">
                  No. SIP
                </label>

                <div class="form-group">
                  <input type="text" class="form-control no_sip" id="no_sip" name="no_sip" value="" placeholder="No. SIP" aria-label="No. SIP" required aria-describedby="no_sipLabel"
                  data-msg="No. SIP"
                  data-error-class="u-has-error"
                  data-success-class="u-has-success">
                </div>
              </div>
			  <!-- Input -->
				  <div class="js-form-message">
					<label class="form-label">Foto SIP</label>
					<div class="form-group">
					  <input class="form-control" type="file" id="foto_sip" name="foto_sip" value="" style="padding: 0.50rem 0.75rem" />
					  <img id="disp_foto_sip" src="" alt="" style="padding:10px;width:200px;">
					</div>
				  </div>
				<!-- End Input -->
            </div>
            <!-- End Input -->
            <!-- Input -->
            <div class="col-sm-6 mb-1">
              <div class="js-form-message">
                <label id="kodePromolabel" class="form-label">
                  Kode Promo
                </label>

                <div class="form-group">
                  <input type="text" class="form-control kode_promo" id="kode_promo" name="kode_promo" value="" placeholder="Kode Promo" aria-label="Kode Promo" required aria-describedby="kodePromolabel"
                  data-msg="Kode Promo"
                  data-error-class="u-has-error"
                  data-success-class="u-has-success" readonly>
                </div>
              </div>
            </div>
            <!-- End Input -->
          </div>
          <!-- Buttons -->
          <input type="hidden" name="id" id="member_id">
          <button type="button" class="btn btn-sm btn-primary transition-3d-hover mr-1 saveProfile">Save Changes</button>
          <!-- End Buttons -->
        </div>
        <!-- End Personal Info Form -->
      </div>
    </form>
    <!-- End Content Section -->
  </div>
</main>
<!-- ========== END MAIN ========== -->

