<script type="text/javascript">
	//tab 
    var tabOne = $('#pills-one-tab'),
		tableOne = $('#semuaData'),
		tableOneBody = $('#semuaData tbody'),
    	tabtwo = $('#pills-two-tab'),
		tabletwo = $('#BelumbayarData'),
		tabletwoBody = $('#BelumbayarData tbody'),
		tabletwoBadge = $('.badge-belum_bayar'),
    	tabThree = $('#pills-three-tab'),
		tableThree = $('#DikemasData'),
		tableThreeBody = $('#DikemasData tbody'),
		tableThreeBadge = $('.badge-dikemas'),
    	tabFour = $('#pills-four-tab'),
		tableFour = $('#DikirmData'),
		tableFourBody = $('#DikirmData tbody'),
		tableFourBadge = $('.badge-dikirim'),
    	tabFive = $('#pills-five-tab'),
		tableFive = $('#SelesaiData'),
		tableFiveBody = $('#SelesaiData tbody'),
    	tabSix = $('#pills-six-tab'),
		tableSix = $('#BatalData'),
		tableSixBody = $('#BatalData tbody'),
    	tabSeven = $('#pills-seven-tab'),
		tableSeven = $('#KembaliData'),
		tableSevenBody = $('#KembaliData tbody'),
		tableDetail = $('#tableDetail'),
		tableDetailBody = $('#tableDetail tbody'),
		
		modalKonfirmasiTerimaPesanan = '#konfirmasi-pesanan-diterima-modal';

	function listenCheckout(q) {
	    eventSource = new EventSource(url.listenCheckout.replace(':UID', q));
	    eventSource.addEventListener('checkout_event', function(e) {
	    	getListBadge();
	    	
	    	let currTab = $('.nav-link.active').prop('id');
	    	if(currTab == 'pills-one-tab') {
	    		tabOne.click();
	    	}

	    	if(currTab == 'pills-two-tab') {
	    		tabtwo.click();
	    	}
	    	if(currTab == 'pills-three-tab') {
	    		tabThree.click();
	    	}
	    	if(currTab == 'pills-four-tab') {
	    		tabFour.click();
	    	}
	    }, false);
	}

	$(document).ready(function() {
		if(userdata) listenCheckout(btoa(member_id));
	});

	tabOne.on('click', function(){
		if ($.fn.dataTable.isDataTable(tableOne)) {
			tableOne.DataTable().destroy();
		}

		table = tableOne;
		tbody = tableOneBody;
		getList(tbody,table,  '');
	});

	tabtwo.on('click', function(){
		if ($.fn.dataTable.isDataTable(tabletwo)) {
			tabletwo.DataTable().destroy();
		}

		table = tabletwo;
		tbody = tabletwoBody;
		getList(tbody, table, '1');
	});

	tabThree.on('click', function(){
		if ($.fn.dataTable.isDataTable(tableThree)) {
			tableThree.DataTable().destroy();
		}

		table = tableThree;
		tbody = tableThreeBody;
		getList(tbody, table, '2');
	});

	tabFour.on('click', function(){
		if ($.fn.dataTable.isDataTable(tableFour)) {
			tableFour.DataTable().destroy();
		}

		table = tableFour;
		tbody = tableFourBody;
		getList(tbody, table, '3');
	});

	tabFive.on('click', function(){
		if ($.fn.dataTable.isDataTable(tableFive)) {
			tableFive.DataTable().destroy();
		}

		table = tableFive;
		tbody = tableFiveBody;
		getList(tbody, table, '4');
	});

	tabSix.on('click', function(){
		if ($.fn.dataTable.isDataTable(tableSix)) {
			tableSix.DataTable().destroy();
		}

		table = tableSix;
		tbody = tableSixBody;
		getList(tbody, table, '5');
	});

	tabSeven.on('click', function(){
		if ($.fn.dataTable.isDataTable(tableSeven)) {
			tableSeven.DataTable().destroy();
		}

		table = tableSeven;
		tbody = tableSevenBody;
		getList(tbody, table, '6');
	});

	function getListBadge(){
		function setBadge(data, tableBadge) {
			if(data) {
      			tableBadge.removeClass('d-none');
      			tableBadge.html(data);
      		} else {
      			if(!tableBadge.hasClass('d-none'))
      				tableBadge.addClass('d-none');
      		}
		}
		$.ajax({
          	url: url.getCheckoutBadge + `?id=${member_id}`,
          	type: 'GET',
          	dataType: 'json',
          	success: function(res) { 
          		setBadge(res.list.belum_bayar, tabletwoBadge);
          		setBadge(res.list.dikemas, tableThreeBadge);
          		setBadge(res.list.dikirim, tableFourBadge);
          	},
          	error: function() { 
          		console.log('error');
          	},
          	beforeSend: setHeader
        });
	};

	function getList(tbody, table, status){
		let param = `?id=${member_id}&status=${status}`;
		let uriCheckout = url.getCheckout+param;

		$.ajax({
          	url: uriCheckout,
          	type: 'GET',
          	dataType: 'json',
          	success: function(res) { 
          		data = res;
    			blockPage('Loading ...');
				tbody.empty();

				if (data.list.length > 0 ) {
					for (var i = 0; i < res.list.length; i++) {
						fillDatatable(res.list[i], i, tbody);
		          	}
		          	if ($('body').find('.dataTables_paginate').length > 0 ) {
		          		$('body').find('.datatabless').html('');
		          	}

         			setTimeout(()=>{
			          $.unblockUI();
			          $.HSCore.components.HSDatatables.init(table);  		
			        },1000);
				}else{
					tbody.append('<th colspan="5">Tidak Ada Data</th>');
         			setTimeout(()=>{
			          $.unblockUI();
			        },1000);
				}

          	},
          	error: function() { 
          		console.log('error');
          	},
          	beforeSend: setHeader
        });
	};

	function setHeader(xhr) {
		xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
	}

	function fillDatatable(data, index, tbody){
		let total = parseInt(data.total_harga) + parseInt(data.ongkir);
		
		var tr = $("<tr/>")
		.appendTo(tbody);

		var tdTgltransaksi = $("<td/>")
		.addClass('text-left text-md-center text-lg-center')
		.appendTo(tr);
		var btntglTransaksi = $("<a/>")
		.attr('data-uid', data.uid)
		.addClass('lihatDetail text-info transition-3d-hover')
		.html(data.tanggal_transaksi + 
			`<br/><label class="d-table-cell d-md-none d-lg-none text-secondary" style="font-size: 0.7rem">${data.no_invoice}</label>` +
			`<br/><label class="d-table-cell d-md-none d-lg-none text-secondary" style="font-size: 0.7rem">Rp. ${numeral(total).format()}</label>`
		)
		.appendTo(tdTgltransaksi);

		var tdiNoinvoice = $("<td/>")
		.addClass('text-left d-none d-md-table-cell d-lg-table-cell')
		.appendTo(tr);
		var dispiNoinvoice = $("<label/>")
		.html(data.no_invoice)
		.appendTo(tdiNoinvoice);

		var tdTotalHarga = $("<td/>")
		.addClass('text-right d-none d-md-table-cell d-lg-table-cell')
		.appendTo(tr);
		var dispTotalHarga = $("<label/>")
		.html(numeral(total).format())
		.appendTo(tdTotalHarga);

		let status = '';
		switch(parseInt(data.status)) {
		  case 1:
		    	status += 'btn-google'; 
		    break;
		  case 2:
		    	status += 'btn-secondary'; 
		    break;
		  case 3:
		    	status += 'btn-twitter '; 
		    break;
		  case 4:
		    	status += 'btn-success'; 
		    break;
		  case 5:
		    	status += 'btn-google'; 
		    break;
		  case 6:
		    	status += 'btn-google'; 
		    break;
		}

		var tdStatusPembayaran = $("<td/>")
		.addClass('text-center d-none d-md-table-cell d-lg-table-cell')
		.appendTo(tr);
		var dispStatusPembayaran = $("<button/>")
		.addClass('btn transition-3d-hover btn-xs '+status)
		.html(data.status_label)
		.appendTo(tdStatusPembayaran);

		let btnDelete = '',
			btnLihat = '',
			btnUpload = '';

		var tdBuktiPembayaran = $("<td/>")
		.addClass('text-center')
		.appendTo(tr);
		if (data.status == 1) {
			btnUpload = $("<button/>")
			.addClass('btn btn-icon btn-facebook transition-3d-hover btn-xs')
			.data('uid', data.uid)
			.css('margin', '5px')
			.prop('title', 'Upload Bukti Pembayaran')
			.prop('type', 'button')
			.html('<span class="fa fa-pencil-alt btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);
			btnDelete = $("<button/>")
			.addClass('btn btn-icon btn-danger transition-3d-hover btn-xs')
			.css('margin', '5px')
			.prop('title', 'Batal Pembayaran')
			.prop('type', 'button')
			.prop('data-uid', data.uid)
			.html('<span class="fa fa-trash btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);

			btnDelete.on('click', function(){
				$('#dispDelete').html('Are you sure you want to delete this Transaction?');
				$('#modalDelete').find('.btnHapus').data('uid', data.uid);
				$('#modalDelete').modal('show');	    
			});

			btnUpload.on('click', function(){
				$('#uploadModal').find('.btnSimpan').data('uid', data.uid);
				$('#uploadModal').find('#imgFile').prop('src', "<?php echo base_url('assets/img/others/no_image_available.png'); ?>");
				$('#uploadModal').find('.btnHapus').data('uid', data.uid);
				$('#uploadModal').find('#uid').val(data.uid);
				if (data.dataDetail[0].bpom == 1) {
					$('#uploadModal').find('#labelTf').html('Pembayaran melalui BCA No. Rek : 0653717129 Atas Nama PT. PRIMA ESTETIKA RAKSA.');
				}else{
					$('#uploadModal').find('#labelTf').html('Pembayaran melalui BCA No. Rek : 0221888251 Atas Nama Rudiyanto.');
				}
				$('#uploadModal').modal('show');	
				$.HSCore.components.HSFileAttach.init('.js-custom-file-attach');
			});
		}else if(data.status == 6){
			btnUpload = $("<button/>")
			.addClass('btn btn-icon btn-facebook transition-3d-hover btn-xs')
			.data('uid', data.uid)
			.css('margin', '5px')
			.prop('title', 'Upload Bukti Pembayaran')
			.prop('type', 'button')
			.html('<span class="fa fa-pencil-alt btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);

			btnLihat = $("<button/>")
			.addClass('btn btn-icon btn-info transition-3d-hover btn-xs text-white')
			.css('margin', '5px')
			.prop('type', 'button')
			.prop('title', 'Lihat Bukti Pembayaran')
			.prop('data-uid', data.uid)
			.html('<span class="fa fa-eye btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);

			btnLihatAlasan = $("<button/>")
			.addClass('btn btn-icon btn-danger transition-3d-hover btn-xs text-white')
			.css('margin', '5px')
			.prop('type', 'button')
			.prop('title', 'Lihat Alasan Dikembalikan')
			.prop('data-uid', data.uid)
			.html('<span class="fa fa-eye btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);

			
			btnLihat.on('click', function(){
				getImage(data.uid);
			});

			btnLihatAlasan.on('click', function(){
				getAlasan(data.uid);
			});

			btnUpload.on('click', function(){
				$('#uploadModal').find('.btnSimpan').data('uid', data.uid);
				$('#uploadModal').find('#imgFile').prop('src', "<?php echo base_url('assets/img/others/no_image_available.png'); ?>");
				$('#uploadModal').find('.btnHapus').data('uid', data.uid);
				$('#uploadModal').find('#uid').val(data.uid);
				if (data.dataDetail[0].bpom == 1) {
					$('#uploadModal').find('#labelTf').html('Pembayaran melalui BCA No. Rek : 0653717129 Atas Nama PT. PRIMA ESTETIKA RAKSA.');
				}else{
					$('#uploadModal').find('#labelTf').html('Pembayaran melalui BCA No. Rek : 0221888251 Atas Nama Rudiyanto.');
				}
				$('#uploadModal').modal('show');	
				$.HSCore.components.HSDropzone.init('.u-dropzone');
			});
		}else if(data.status == 3){
			btnLihat = $("<button/>")
			.addClass('btn btn-icon btn-info transition-3d-hover btn-xs text-white')
			.css('margin', '5px')
			.prop('type', 'button')
			.prop('title', 'Lihat Bukti Pembayaran')
			.prop('data-uid', data.uid)
			.html('<span class="fa fa-eye btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);

			btnResi = $("<button/>")
			.addClass('btn btn-icon btn-success transition-3d-hover btn-xs text-white')
			.css('margin', '5px')
			.prop('type', 'button')
			.prop('title', 'Cek Resi')
			.prop('data-uid', data.uid)
			.html('<span class="fa fa-truck btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);

			btnPesananDiterima = $("<button/>")
			.addClass('btn btn-icon btn-danger transition-3d-hover btn-xs text-white')
			.css('margin', '5px')
			.prop('type', 'button')
			.prop('title', 'Konfirmasi pesanan diterima')
			.prop('data-uid', data.uid)
			.html('<span class="fa fa-check btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);

			btnLihat.on('click', function(){
				getImage(data.uid);
			});

			btnResi.on('click', function(){
				cekResi(data);
			});

			btnPesananDiterima.on('click', function(){
				$(modalKonfirmasiTerimaPesanan).data('id', data.uid);
				$(modalKonfirmasiTerimaPesanan).modal('show');
			});
		}else if(data.status == 5){
			btnA = $("<label/>")
			.html('-')
			.appendTo(tdBuktiPembayaran);
		}else{
			btnLihat = $("<button/>")
			.addClass('btn btn-icon btn-info transition-3d-hover btn-xs text-white')
			.css('margin', '5px')
			.prop('type', 'button')
			.prop('title', 'Lihat Bukti Pembayaran')
			.prop('data-uid', data.uid)
			.html('<span class="fa fa-eye btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);

			btnPrint = $("<button/>")
			.addClass('btn btn-icon btn-warning transition-3d-hover btn-xs text-white')
			.css('margin', '5px')
			.prop('type', 'button')
			.prop('title', 'Cetak Invoice')
			.prop('data-uid', data.uid)
			.html('<span class="fa fa-print btn-icon__inner"></span>')
			.appendTo(tdBuktiPembayaran);

			btnLihat.on('click', function(){
				getImage(data.uid);
			});

			btnPrint.on('click', function(){
				printInvoice(data.uid);
			});
		}

		btntglTransaksi.on('click', function(){
			getDetail(data.uid);
		});
	}


    //btn delete cart
    $('body').find('#modalDelete').on('click', '.btnHapus', function(){
      let uid = $(this).data('uid'),
          mode = $(this).data('mode');
        $('#modalDelete').modal('hide');  
	    blockPage('Loading ...');
	    $.ajax({
	      url:url.deleteCheckout,
	      method:'POST',
	      dataType: 'json',
	      data: {uid: uid},
	      beforeSend: setHeader,
	      success: function(res) { 
	        setTimeout(()=>{
	          $.unblockUI();
	          window.location.reload();
	        },1000);
	      },
	      error: function(res) {
	        setTimeout(()=>{
	          $.unblockUI();
	        },2000);
	      },
	    });
    });

	function readURL(input) {
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    
	    reader.onload = function(e) {
	      $('#imgFile').attr('src', e.target.result);
	    }
	    
	    reader.readAsDataURL(input.files[0]); // convert to base64 string
	  }
	}

	$("#fileUpload").change(function() {
	  readURL(this);
	});

    //btn save upload
    $('body').find('#uploadModal').on('click', '.btnSimpan', function(){
	    let uid = $(this).data('uid'),
          	file = $('#fileUpload').val();

        var fd = new FormData();
        var files = $('#fileUpload')[0].files[0];
        fd.append('file', files);
        fd.append('uid', uid);
		
		$('#uploadModal').modal('hide');  
	    blockPage('Loading ...');

	    $.ajax({
	      url:url.saveCheckout,
	      method:'POST',
	      dataType: 'json',
	      data: fd,
          contentType: false,
          processData: false,
	      beforeSend: setHeader,
	      success: function(res) { 
	        setTimeout(()=>{
	          $.unblockUI();
	          window.location.reload();
	        },1000);
	      },
	      error: function(err) {
	      	alert(typeof err.responseJSON !== 'undefined' ? err.responseJSON.message : 'Terjadi kesalahan pada server');
          	$.unblockUI();
	      },
	    });
    });

  	function getDetail(uid){
		let param = `?uid=${uid}`;
		let uriDetail = url.getCheckoutDetail+param;

	    $.ajax({
          	url: uriDetail,
          	type: 'GET',
          	dataType: 'json',
          	success: function(res) { 
          		data = res;
    			blockPage('Loading ...');

				tbody = tableDetailBody;
				tbody.empty();

				if (data) {
					let totalPembayaran = parseFloat(data.total_harga) + parseFloat(data.ongkir);
					$('#no_invoice').html(data.no_invoice);
					$('#tanggal_transaksi').html(data.tanggal_transaksi);
					$('#total_harga').html(numeral(data.total_harga).format());
					$('#ekspedisi').html(data.ekspedisi ? data.ekspedisi : '-');
					$('#ongkos_kirim').html(data.ongkir != 0 ? numeral(data.ongkir).format() : '-');
					$('#total_pembayaran').html(numeral(totalPembayaran).format());
					$('#status_pembayaran').html(data.status_label);
					$('#bukti_pembayaran').html(data.bukti_pembayaran);


					for (var i = 0; i < data.dataDetail.length; i++) {
						fillDetail(data.dataDetail[i], i, tbody);
		          	}
				
	     			setTimeout(()=>{
			          $.unblockUI();
						$('#listDetailModal').modal('show');	
			        },1000);
				}
				else{
					tbody.append('<th colspan="5">Tidak Ada Data</th>');
         			setTimeout(()=>{
			          $.unblockUI();
			        },1000);
				}


          	},
          	error: function() { 
          		console.log('aa');
          	},
          	beforeSend: setHeader
        });
  	};

  	function fillDetail(data, index, tbody){
  		var tr = $("<tr/>")
		.appendTo(tbody);

		let hargaDiskon = (parseFloat(data.harga) * (parseFloat(data.diskon) / 100));
		let harga = parseFloat(data.harga) - hargaDiskon;

		let product = data.nama + 
			`<br/><label class="d-table-cell d-md-none d-lg-none text-secondary" style="font-size: 0.7rem">Rp. ${numeral(data.harga).format()} X ${numeral(data.qty).format()}</label>`;

		var tdProduct = $("<td/>")
		.addClass('text-left text-md-center text-lg-center')
		.appendTo(tr);
		var dispProduct = $("<label/>")
		.html(product)
		.appendTo(tdProduct);

		var tdQty = $("<td/>")
		.addClass('text-center d-none d-md-table-cell d-lg-table-cell')
		.appendTo(tr);
		var dispQty = $("<label/>")
		.html(data.qty)
		.appendTo(tdQty);

		var tdPrice = $("<td/>")
		.addClass('text-right d-none d-md-table-cell d-lg-table-cell')
		.appendTo(tr);
		var dispPrice = $("<label/>")
		.html(numeral(harga).format())
		.appendTo(tdPrice);

		let totalPrice = parseInt(data.qty) * parseInt(harga);

		var tdtotalPrice = $("<td/>")
		.addClass('text-right d-none d-md-table-cell d-lg-table-cell')
		.appendTo(tr);
		var disptotalPrice = $("<label/>")
		.html(numeral(totalPrice).format())
		.appendTo(tdtotalPrice);
  	}

  	function getImage(uid){
		let param = `?uid=${uid}`;
		let uriDetail = url.getCheckoutDetail+param;

        $('#EkspedisiModal').modal('show');
        $('#EkspedisiModal').find('#EkspedisiModalLabel').html('Bukti Pembayaran');
		$('#EkspedisiModal').find('.modal-body').html('');

	    $.ajax({
          	url: uriDetail,
          	type: 'GET',
          	dataType: 'json',
          	success: function(res) { 
          		data = res;
    			blockPage('Loading ...');

    			let image = api_base_uri+data.bukti_pembayaran,
    				priview = '<div class="cbp-item">'
						            +'<img class="card-img-top" src="'+image+'" alt="Image Description" height="400px">'
						        +'</div>';
			
			    $('#EkspedisiModal').find('.modal-body').html(priview);
			    $('#EkspedisiModal').find('.modal-body').addClass('text-center');

     			setTimeout(()=>{
		          $.unblockUI();
		        },1000);

          	},
          	error: function() { 
          		console.log('aa');
          	},
          	beforeSend: setHeader
        });
  	};

  	function getAlasan(uid){
		let param = `?uid=${uid}`;
		let uriDetail = url.getCheckoutDetail+param;

        $('#EkspedisiModal').modal('show');
        $('#EkspedisiModal').find('#EkspedisiModalLabel').html('Alasan Dikembalikan');
		$('#EkspedisiModal').find('.modal-body').html('');

	    $.ajax({
          	url: uriDetail,
          	type: 'GET',
          	dataType: 'json',
          	success: function(res) { 
          		data = res;
    			blockPage('Loading ...');

    			let image = api_base_uri+data.bukti_pembayaran,
    				priview = '<div class="cbp-item mb-5">'
						            +'<img class="card-img-top" src="'+image+'" alt="Image Description" height="400px">'
						        +'</div>'
						        +'<div class="cbp-item">'
						            +'<label>Alasan Dikembalikan </label>'
						            +'<p>'+data.alasan_penolakan+'</p>'
						        +'</div>';
			
			    $('#EkspedisiModal').find('.modal-body').html(priview);
			    $('#EkspedisiModal').find('.modal-body').addClass('text-left');

     			setTimeout(()=>{
		          $.unblockUI();
		        },1000);

          	},
          	error: function() { 
          		console.log('aa');
          	},
          	beforeSend: setHeader
        });
  	};

  	function printInvoice(uid){
		let url = api_base_uri+'/api/v1/transaksi/checkout/print_invoice_online?uid='+uid;
		
		blockPage('Loading ...');
        
		$('#PrintModal').find('#modalIframe').prop('src', url);
		setTimeout(()=>{
	      	$.unblockUI();
	        $('#PrintModal').modal('show');
	        $('#PrintModal').find('#PrintModalLabel').html('Cetak faktur');
	    },1000);
  	};

  	function cekResi(data){
		data2 = { 
    		  courier : data.ekspedisi,
    		  no_resi : data.no_resi,
    		};

		$.ajax({
	        type : 'POST',
	        url : site_url+'/akun/profile/getWaybill',
	        data : data2,
	        dataType : 'json',
	        success: function (data) {
	          	let resultD = data.rajaongkir.result;

	          	$('#no_resi_p').html(resultD.details.waybill_number);
				$('#tanggal_pengiriman').html(resultD.details.waybill_date+' '+ resultD.details.waybill_time);
				$('#service').html(resultD.summary.service_code);
				$('#dikirim_oleh').html(resultD.details.shippper_name+' '+resultD.details.origin);
				$('#dikirim_ke').html(resultD.details.receiver_name+' '+resultD.details.receiver_address1+' '+resultD.details.receiver_address2+' '+resultD.details.receiver_address3+' '+resultD.details.receiver_city);
				$('#status_pengiriman').html(resultD.summary.status);
				
				$('#table-pengiriman').find('tbody').empty();

				for (var i = 0, n = resultD.manifest.length; i < n; i++) {
					prosesPengirirman(resultD.manifest[i]);
				}
	        }
	    });
		setTimeout(()=>{
				$.unblockUI();
			$('#detail-modal-lihat').modal('show');
	    },1000);
  	}

	function prosesPengirirman(data){
		data = data || {};

		var tbody = $('#table-pengiriman').find('tbody');

		var tr = $("<tr/>")
		.appendTo(tbody);

		var tdTanggal = $("<td/>")
		.appendTo(tr);
		var inputTanggal = $("<label/>")
		.html(data.manifest_date+' '+ data.manifest_time)
		.appendTo(tdTanggal);

		var tdKetrangan = $("<td/>")
		.appendTo(tr);
		var inputKeterangan = $("<label/>")
		.html(data.manifest_description)
		.appendTo(tdKetrangan);
	}

	// KONFIRMASI PESANAN DITERIMA
	$(modalKonfirmasiTerimaPesanan).on('click', '.btn-konfirmasi_pesanan_diterima', function() {
		const uid = $(modalKonfirmasiTerimaPesanan).data('id');
		$(this).prop('disabled', true);
		$(this).html(`
			<div class="spinner-grow spinner-grow-sm text-primary" role="status">
	        	<span class="sr-only">Loading...</span>
	        </div>
	        <div class="spinner-grow spinner-grow-sm text-secondary" role="status">
	            <span class="sr-only">Loading...</span>
	        </div>
	        <div class="spinner-grow spinner-grow-sm text-success" role="status">
	            <span class="sr-only">Loading...</span>
	        </div>`);

		$.ajax({
	        type : 'POST',
	        url : url.orderReceived,
	        data : {
	        	id: uid,
	        	member_id: member_id
	        },
	        dataType : 'json',
	        beforeSend: setHeader,
	        success: function (data) {
	        	$(this).prop('disabled', false);
	        	$(this).html('Konfirmasi');
	        	$(modalKonfirmasiTerimaPesanan).modal('hide');
	        },
	        error: function (error) {
	        	$(this).prop('disabled', false);
	        	$(this).html('Konfirmasi');
	          	alert('Terdapat kesalahan pada server ketika memproses pesanan');
	        }
	    });
	})
</script>