<style type="text/css">
  th {text-align: center;}
</style>

<!-- ========== MAIN ========== -->
  <main id="content" role="main">
    <!-- Breadcrumb Section -->
    <div class="bg-header">

      <div class="container space-top-3 space-bottom-lg-0">
        <div class="d-lg-flex justify-content-lg-between align-items-lg-center">
          <!-- Navbar -->
          <div class="u-header u-header-left-aligned-nav u-header--bg-transparent-lg u-header--white-nav-links z-index-4">
            <div class="u-header__section bg-transparent">
              <nav class="js-breadcrumb-menu navbar navbar-expand-lg u-header__navbar u-header__navbar--no-space">
                <div id="breadcrumbNavBar" class="collapse navbar-collapse u-header__navbar-collapse">
                  <ul class="navbar-nav u-header__navbar-nav">
                    <!-- Akun -->
                    <li class="nav-item hs-has-sub-menu u-header__nav-item"
                        data-event="hover"
                        data-animation-in="slideInUp"
                        data-animation-out="fadeOut">
                      <a id="generalDropdown" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="generalDropdownMenu">
                        Akun Saya
                      </a>

                      <ul id="generalDropdownMenu" class="hs-sub-menu u-header__sub-menu u-header__sub-menu--spacer" style="min-width: 230px;" aria-labelledby="generalDropdown">
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="<?php echo site_url('akun/profile') ?>">Profil</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="<?php echo site_url('akun/profile/alamat') ?>">Alamat</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="<?php echo site_url('akun/profile/atur_password') ?>">Atur Password</a></li>
                      </ul>
                    </li>
                    <!-- Akun -->

                    <!-- Pesanan -->
                    <li class="nav-item hs-has-sub-menu u-header__nav-item">
                      <a class="nav-link u-header__nav-link" href="<?php echo site_url('akun/profile/pesanan') ?>" >
                        Pesanan Saya
                      </a>
                    </li>
                    <!-- Pesanan -->

                    <!-- Point -->
                    <!-- li class="nav-item hs-has-sub-menu u-header__nav-item">
                      <a class="nav-link u-header__nav-link" href="<?php echo site_url('akun/profile/point') ?>" >
                        Point
                      </a>
                    </li> -->
                    <!-- Point -->

                  </ul>
                </div>
              </nav>
            </div>
          </div>
          <!-- End Navbar -->
          <div class="ml-lg-auto col-sm-3">
            <div class="d-flex d-lg-inline-block justify-content-center justify-content-lg-end align-items-center align-items-lg-start">
              <!-- Breadcrumb -->
              <ol class="breadcrumb breadcrumb-white breadcrumb-no-gutter mb-0">
                <li class="breadcrumb-item"><a class="breadcrumb-link" href="#">Account</a></li>
                <li class="breadcrumb-item active" aria-current="page">Pesanan</li>
              </ol>
              <!-- End Breadcrumb -->

              <!-- Breadcrumb Nav Toggle Button -->
              <div class="d-lg-none">
                <button type="button" class="navbar-toggler btn u-hamburger u-hamburger--white"
                        aria-label="Toggle navigation"
                        aria-expanded="false"
                        aria-controls="breadcrumbNavBar"
                        data-toggle="collapse"
                        data-target="#breadcrumbNavBar">
                  <span id="breadcrumbHamburgerTrigger" class="u-hamburger__box">
                    <span class="u-hamburger__inner"></span>
                  </span>
                </button>
              </div>
              <!-- End Breadcrumb Nav Toggle Button -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Breadcrumb Section -->

    <!-- Content Section -->
    <div class="bg-light">
      <div class="container space-1">

        
        <div class="card">
          <div class="card-body p-4">
            <!-- Nav -->
            <div class="row justify-content-center align-items-center mb-4">

              <div class="col-md-12 order-md-1">
                <!-- Nav Classic -->
                <ul class="nav nav-classic nav-borderless px-0" id="pills-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="pills-one-tab" data-toggle="pill" href="#pills-one" role="tab" aria-controls="pills-one" aria-selected="true">Semua</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-two-tab" data-toggle="pill" href="#pills-two" role="tab" aria-controls="pills-two" aria-selected="false">Belum Bayar</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-three-tab" data-toggle="pill" href="#pills-three" role="tab" aria-controls="pills-three" aria-selected="false">Dikemas</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-four-tab" data-toggle="pill" href="#pills-four" role="tab" aria-controls="pills-four" aria-selected="false">Dikirim</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-five-tab" data-toggle="pill" href="#pills-five" role="tab" aria-controls="pills-five" aria-selected="false">Selesai</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-six-tab" data-toggle="pill" href="#pills-six" role="tab" aria-controls="pills-six" aria-selected="false">Dibatalkan</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-seven-tab" data-toggle="pill" href="#pills-seven" role="tab" aria-controls="pills-seven" aria-selected="false">Dikembalikan</a>
                  </li>
                </ul>
                <!-- End Nav Classic -->
              </div>
            </div>
            <!-- End Nav -->

            <!-- Datatable -->
            <div class="tab-content" id="pills-tabContent">
              <!-- Content One -->
              <div class="tab-pane fade show active" id="pills-one" role="tabpanel" aria-labelledby="pills-one-tab">
                <!-- Transaction Table -->
                <!-- Search -->
                  <div class="js-focus-state input-group input-group-sm mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <span class="fas fa-search"></span>
                      </span>
                    </div>
                    <input id="datatableSearch" class="form-control" type="email" placeholder="Search" aria-label="Search">
                  </div>
                  <!-- End Search -->

                  <div class="table-responsive-md u-datatable">
                    <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5" id="semuaData"
                           data-dt-search="#datatableSearch"
                           data-dt-info="#datatableWithPaginationInfo"
                           data-dt-page-length="5"
                           data-dt-is-show-paging="true"

                           data-dt-pagination="datatablePaginationOne"
                           data-dt-pagination-classes="pagination mb-0"
                           data-dt-pagination-items-classes="page-item"
                           data-dt-pagination-links-classes="page-link"

                           data-dt-pagination-next-classes="page-item"
                           data-dt-pagination-next-link-classes="page-link"
                           data-dt-pagination-next-link-markup='<span aria-hidden="true">»</span>'

                           data-dt-pagination-prev-classes="page-item"
                           data-dt-pagination-prev-link-classes="page-link"
                           data-dt-pagination-prev-link-markup='<span aria-hidden="true">«</span>'>
                      <thead>
                        <tr class="text-uppercase font-size-1">
                          <th scope="col" class="font-weight-medium">
                              Tanggal Transaksi
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Nomor Faktur
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Total Harga (IDR)
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Status
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Upload Bukti Pembayaran
                          </th>
                        </tr>
                      </thead>
                      <tbody class="font-size-1">
                        <tr>
                          <th colspan="5">Tidak Ada Data</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!-- End Transaction Table -->
                <!-- Pagination -->
                  <div class="d-flex justify-content-center align-items-center">
                    <nav id="datatablePaginationOne" class="datatabless" aria-label="Activity pagination"></nav>
                  </div>
                <!-- End Pagination -->
              </div>

              <!-- Content Two -->
              <div class="tab-pane fade" id="pills-two" role="tabpanel" aria-labelledby="pills-two-tab"> 
                <!-- Transaction Table -->
                <!-- Search -->
                  <div class="js-focus-state input-group input-group-sm mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <span class="fas fa-search"></span>
                      </span>
                    </div>
                    <input id="datatableSearchtwo" class="form-control" type="email" placeholder="Search" aria-label="Search">
                  </div>
                  <!-- End Search -->

                  <div class="table-responsive-md u-datatable">
                    <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5" id="BelumbayarData"
                           data-dt-search="#datatableSearchtwo"
                           data-dt-info="#datatableWithPaginationInfo"
                           data-dt-page-length="5"
                           data-dt-is-show-paging="true"

                           data-dt-pagination="datatablePaginationTwo"
                           data-dt-pagination-classes="pagination mb-0"
                           data-dt-pagination-items-classes="page-item"
                           data-dt-pagination-links-classes="page-link"

                           data-dt-pagination-next-classes="page-item"
                           data-dt-pagination-next-link-classes="page-link"
                           data-dt-pagination-next-link-markup='<span aria-hidden="true">»</span>'

                           data-dt-pagination-prev-classes="page-item"
                           data-dt-pagination-prev-link-classes="page-link"
                           data-dt-pagination-prev-link-markup='<span aria-hidden="true">«</span>'>
                      <thead>
                        <tr class="text-uppercase font-size-1">
                          <th scope="col" class="font-weight-medium">
                              Tanggal Transaksi
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Nomor Faktur
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Total Harga
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Status
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Upload Bukti Pembayaran
                          </th>
                        </tr>
                      </thead>
                      <tbody class="font-size-1">                       
                        <tr>
                          <th colspan="5">Tidak Ada Data</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!-- End Transaction Table -->
                <!-- Pagination -->
                  <div class="d-flex justify-content-center align-items-center">
                    <nav id="datatablePaginationTwo" class="datatabless" aria-label="Activity pagination"></nav>
                  </div>
                <!-- End Pagination -->
              </div>
              <!-- End Content Two -->

              <!-- Content Three -->
              <div class="tab-pane fade" id="pills-three" role="tabpanel" aria-labelledby="pills-three-tab">
                <!-- Transaction Table -->
                <!-- Search -->
                  <div class="js-focus-state input-group input-group-sm mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <span class="fas fa-search"></span>
                      </span>
                    </div>
                    <input id="datatableSearchthree" class="form-control" type="email" placeholder="Search" aria-label="Search">
                  </div>
                  <!-- End Search -->

                  <div class="table-responsive-md u-datatable">
                    <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5" id="DikemasData"
                           data-dt-search="#datatableSearchthree"
                           data-dt-info="#datatableWithPaginationInfo"
                           data-dt-page-length="5"
                           data-dt-is-show-paging="true"

                           data-dt-pagination="datatablePaginationThree"
                           data-dt-pagination-classes="pagination mb-0"
                           data-dt-pagination-items-classes="page-item"
                           data-dt-pagination-links-classes="page-link"

                           data-dt-pagination-next-classes="page-item"
                           data-dt-pagination-next-link-classes="page-link"
                           data-dt-pagination-next-link-markup='<span aria-hidden="true">»</span>'

                           data-dt-pagination-prev-classes="page-item"
                           data-dt-pagination-prev-link-classes="page-link"
                           data-dt-pagination-prev-link-markup='<span aria-hidden="true">«</span>'>
                      <thead>
                        <tr class="text-uppercase font-size-1">
                          <th scope="col" class="font-weight-medium">
                              Tanggal Transaksi
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Nomor Faktur
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Total Harga
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Status
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Upload Bukti Pembayaran
                          </th>
                        </tr>
                      </thead>
                      <tbody class="font-size-1">
                        <tr>
                          <th colspan="5">Tidak Ada Data</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!-- End Transaction Table -->
                <!-- Pagination -->
                  <div class="d-flex justify-content-center align-items-center">
                    <nav id="datatablePaginationThree" class="datatabless" aria-label="Activity pagination"></nav>
                  </div>
                <!-- End Pagination -->
              </div>
              <!-- End Content Three -->

              <!-- Content Four -->
              <div class="tab-pane fade" id="pills-four" role="tabpanel" aria-labelledby="pills-four-tab">
                
                <!-- Transaction Table -->
                <!-- Search -->
                  <div class="js-focus-state input-group input-group-sm mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <span class="fas fa-search"></span>
                      </span>
                    </div>
                    <input id="datatableSearchfour" class="form-control" type="email" placeholder="Search" aria-label="Search">
                  </div>
                  <!-- End Search -->

                  <div class="table-responsive-md u-datatable">
                    <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5" id="DikirmData"
                           data-dt-search="#datatableSearchfour"
                           data-dt-info="#datatableWithPaginationInfo"
                           data-dt-page-length="5"
                           data-dt-is-show-paging="true"

                           data-dt-pagination="datatablePaginationFour"
                           data-dt-pagination-classes="pagination mb-0"
                           data-dt-pagination-items-classes="page-item"
                           data-dt-pagination-links-classes="page-link"

                           data-dt-pagination-next-classes="page-item"
                           data-dt-pagination-next-link-classes="page-link"
                           data-dt-pagination-next-link-markup='<span aria-hidden="true">»</span>'

                           data-dt-pagination-prev-classes="page-item"
                           data-dt-pagination-prev-link-classes="page-link"
                           data-dt-pagination-prev-link-markup='<span aria-hidden="true">«</span>'>
                      <thead>
                        <tr class="text-uppercase font-size-1">
                          <th scope="col" class="font-weight-medium">
                              Tanggal Transaksi
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Nomor Faktur
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Total Harga
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Status
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Upload Bukti Pembayaran
                          </th>
                        </tr>
                      </thead>
                      <tbody class="font-size-1">
                        <tr>
                          <th colspan="5">Tidak Ada Data</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!-- End Transaction Table -->
                <!-- Pagination -->
                  <div class="d-flex justify-content-center align-items-center">
                    <nav id="datatablePaginationFour" class="datatabless" aria-label="Activity pagination"></nav>
                  </div>
                <!-- End Pagination -->
              </div>
              <!-- End Content Four -->

              <!-- Content five -->
              <div class="tab-pane fade" id="pills-five" role="tabpanel" aria-labelledby="pills-five-tab">
                
                <!-- Transaction Table -->
                <!-- Search -->
                  <div class="js-focus-state input-group input-group-sm mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <span class="fas fa-search"></span>
                      </span>
                    </div>
                    <input id="datatableSearchfive" class="form-control" type="email" placeholder="Search" aria-label="Search">
                  </div>
                  <!-- End Search -->

                  <div class="table-responsive-md u-datatable">
                    <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5" id="SelesaiData"
                           data-dt-search="#datatableSearchfive"
                           data-dt-info="#datatableWithPaginationInfo"
                           data-dt-page-length="5"
                           data-dt-is-show-paging="true"

                           data-dt-pagination="datatablePaginationFive"
                           data-dt-pagination-classes="pagination mb-0"
                           data-dt-pagination-items-classes="page-item"
                           data-dt-pagination-links-classes="page-link"

                           data-dt-pagination-next-classes="page-item"
                           data-dt-pagination-next-link-classes="page-link"
                           data-dt-pagination-next-link-markup='<span aria-hidden="true">»</span>'

                           data-dt-pagination-prev-classes="page-item"
                           data-dt-pagination-prev-link-classes="page-link"
                           data-dt-pagination-prev-link-markup='<span aria-hidden="true">«</span>'>
                      <thead>
                        <tr class="text-uppercase font-size-1">
                          <th scope="col" class="font-weight-medium">
                              Tanggal Transaksi
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Nomor Faktur
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Total Harga
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Status
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Upload Bukti Pembayaran
                          </th>
                        </tr>
                      </thead>
                      <tbody class="font-size-1">
                        <tr>
                          <th colspan="5">Tidak Ada Data</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!-- End Transaction Table -->
                <!-- Pagination -->
                  <div class="d-flex justify-content-center align-items-center">
                    <nav id="datatablePaginationFive" class="datatabless" aria-label="Activity pagination"></nav>
                  </div>
                <!-- End Pagination -->
              </div>
              <!-- End Content five -->

              <!-- Content six -->
              <div class="tab-pane fade" id="pills-six" role="tabpanel" aria-labelledby="pills-six-tab">
                
                <!-- Transaction Table -->
                <!-- Search -->
                  <div class="js-focus-state input-group input-group-sm mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <span class="fas fa-search"></span>
                      </span>
                    </div>
                    <input id="datatableSearchsix" class="form-control" type="email" placeholder="Search" aria-label="Search">
                  </div>
                  <!-- End Search -->

                  <div class="table-responsive-md u-datatable">
                    <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5" id="BatalData"
                           data-dt-search="#datatableSearchsix"
                           data-dt-info="#datatableWithPaginationInfo"
                           data-dt-page-length="5"
                           data-dt-is-show-paging="true"

                           data-dt-pagination="datatablePaginationSix"
                           data-dt-pagination-classes="pagination mb-0"
                           data-dt-pagination-items-classes="page-item"
                           data-dt-pagination-links-classes="page-link"

                           data-dt-pagination-next-classes="page-item"
                           data-dt-pagination-next-link-classes="page-link"
                           data-dt-pagination-next-link-markup='<span aria-hidden="true">»</span>'

                           data-dt-pagination-prev-classes="page-item"
                           data-dt-pagination-prev-link-classes="page-link"
                           data-dt-pagination-prev-link-markup='<span aria-hidden="true">«</span>'>
                      <thead>
                        <tr class="text-uppercase font-size-1">
                          <th scope="col" class="font-weight-medium">
                              Tanggal Transaksi
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Nomor Faktur
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Total Harga
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Status
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Upload Bukti Pembayaran
                          </th>
                        </tr>
                      </thead>
                      <tbody class="font-size-1">
                        <tr>
                          <th colspan="5">Tidak Ada Data</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!-- End Transaction Table -->
                <!-- Pagination -->
                  <div class="d-flex justify-content-center align-items-center">
                    <nav id="datatablePaginationSix" class="datatabless" aria-label="Activity pagination"></nav>
                  </div>
                <!-- End Pagination -->
              </div>
              <!-- End Content six -->

              <!-- Content seven -->
              <div class="tab-pane fade" id="pills-seven" role="tabpanel" aria-labelledby="pills-seven-tab">
                
                <!-- Transaction Table -->
                <!-- Search -->
                  <div class="js-focus-state input-group input-group-sm mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <span class="fas fa-search"></span>
                      </span>
                    </div>
                    <input id="datatableSearchseven" class="form-control" type="email" placeholder="Search" aria-label="Search">
                  </div>
                  <!-- End Search -->

                  <div class="table-responsive-md u-datatable">
                    <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5" id="KembaliData"
                           data-dt-search="#datatableSearchseven"
                           data-dt-info="#datatableWithPaginationInfo"
                           data-dt-page-length="5"
                           data-dt-is-show-paging="true"

                           data-dt-pagination="datatablePaginationseven"
                           data-dt-pagination-classes="pagination mb-0"
                           data-dt-pagination-items-classes="page-item"
                           data-dt-pagination-links-classes="page-link"

                           data-dt-pagination-next-classes="page-item"
                           data-dt-pagination-next-link-classes="page-link"
                           data-dt-pagination-next-link-markup='<span aria-hidden="true">»</span>'

                           data-dt-pagination-prev-classes="page-item"
                           data-dt-pagination-prev-link-classes="page-link"
                           data-dt-pagination-prev-link-markup='<span aria-hidden="true">«</span>'>
                      <thead>
                        <tr class="text-uppercase font-size-1">
                          <th scope="col" class="font-weight-medium">
                              Tanggal Transaksi
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Nomor Faktur
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Total Harga
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Status
                          </th>
                          <th scope="col" class="font-weight-medium">
                              Upload Bukti Pembayaran
                          </th>
                        </tr>
                      </thead>
                      <tbody class="font-size-1">
                        <tr>
                          <th colspan="5">Tidak Ada Data</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!-- End Transaction Table -->
                <!-- Pagination -->
                  <div class="d-flex justify-content-center align-items-center">
                    <nav id="datatablePaginationseven" class="datatabless" aria-label="Activity pagination"></nav>
                  </div>
                <!-- End Pagination -->
              </div>
              <!-- End Content seven -->
            </div>
            <!-- End Datatable -->
          </div>
        </div>

      </div>
    </div>
    <!-- End Content Section -->
  </main>
  <!-- ========== END MAIN ========== -->


  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalDeleteLabel">Warning !!!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <span id="dispDelete"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-xs btn-danger btnHapus">Delete</button>
          <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>