<!-- ========== MAIN ========== -->
<main id="content" role="main">
  <div class="container space-top-3 space-bottom-lg-0"></div>
  <!-- Menu Header 2 -->    
  <?php $this->load->view('website/component/profile-menu'); ?>

  <!-- Content Section -->
  <div class="bg-light">
    <div class="container space-2">
      <!-- Personal Info Form -->
      <form class="js-validate" id="formAddress">
        <div class="row">

          <!-- Input -->
          <div class="col-sm-6 mb-1">
            <div class="js-form-message">
              <label id="nameLabel" class="form-label">
                Name
              </label>

              <div class="form-group">
                <input type="text" class="form-control name" id="name" name="name" value="" placeholder="Nama" aria-label="Nama" required readonly="readonly" aria-describedby="nameLabel"
                       data-msg="Nama"
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
                <small class="form-text text-muted">Displayed on your public profile, notifications and other places.</small>
              </div>
            </div>
          </div>
          <!-- End Input -->

          <!-- Input -->
          <div class="col-sm-6 mb-1">
            <div class="js-form-message">
              <label id="phoneNumberLabel" class="form-label">
                Phone number
              </label>

              <div class="form-group">
                <input class="form-control phone_number" id="phone_number" type="tel" name="phone_number" value="" placeholder="No. Handphone" aria-label="No. Handphone" required readonly="readonly" aria-describedby="phoneNumberLabel"
                       data-msg="No. Handphone"
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
              </div>
            </div>
          </div>
          <!-- End Input -->
        </div>

        <div class="row">
          <!-- Input -->
          <div class="col-sm-6 mb-1">
            <div class="js-form-message">
              <label id="websiteLabel" class="form-label">
                Provinsi
                <span class="text-danger">*</span>
              </label>

              <div class="form-group">
                <select class="custom-select provinsi select2" id="provinsi" name="provinsi">
                  <option value="">Pilih</option>
                  <?php 
                    foreach ($data['rajaongkir']['results'] as $row) {
                      echo "<option value='".$row['province_id']."'>".$row['province']."</option>";
                    }
                  ?>
                </select>
                <input type="hidden" name="provinsi_text" id="provinsi_text">
              </div>
            </div>
          </div>
          <!-- End Input -->

          <!-- Input -->
          <div class="col-sm-6 mb-1">
            <div class="js-form-message">
              <label id="kodeposLabel" class="form-label">
                Kode Pos
                <span class="text-danger">*</span>
              </label>

              <div class="form-group">
                <input type="text" class="form-control kode_pos" id="kode_pos" name="kode_pos" value="" placeholder="Kode Pos" aria-label="Kode Pos" required aria-describedby="kodeposLabel"
                       data-msg="Kode Pos"
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
              </div>
            </div>
          </div>
          <!-- End Input -->

        </div>

        <div class="row">
          <!-- Input -->
          <div class="col-sm-6 mb-1">
            <div class="js-form-message">
              <label id="websiteLabel" class="form-label">
                Kota
                <span class="text-danger">*</span>
              </label>

              <div class="form-group">
                <select class="custom-select kota" id="kota" name="kota">
                  <option value="">Pilih</option>
                </select>
                <input type="hidden" name="kota_tmp" id="kota_tmp">
                <input type="hidden" name="kota_text" id="kota_text">
              </div>
            </div>
          </div>
          <!-- End Input -->

          <!-- Input -->
          <div class="col-sm-6 mb-1">
            <div class="js-form-message">
              <label id="kodeposLabel" class="form-label">
                Nama Gedung / Nama Jalan
                <span class="text-danger">*</span>
              </label>

              <div class="form-group">
                <input type="text" class="form-control nama_gedung" id="nama_gedung" name="nama_gedung" value="" placeholder="Nama Gedung,  Jalan dll" aria-label="Nama Gedung, Jalan dll" required aria-describedby="jalanLabel"
                       data-msg="Nama Gedung, Jalan dll"
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
              </div>
            </div>
          </div>
          <!-- End Input -->
        </div>

        <div class="row">
          <!-- Input -->
          <div class="col-sm-6 mb-1">
            <div class="js-form-message">
              <label id="websiteLabel" class="form-label">
                Kecamatan
                <span class="text-danger">*</span>
              </label>

              <div class="form-group">
                <select class="custom-select kecamatan" id="kecamatan" name="kecamatan">
                  <option value="">Pilih</option>
                </select>
                <input type="hidden" name="kecamatan_tmp" id="kecamatan_tmp">
                <input type="hidden" name="kecamatan_text" id="kecamatan_text">
              </div>
            </div>
          </div>
          <!-- End Input -->
        </div>
        <!-- Buttons -->
        <input type="hidden" name="id" id="member_id">
        <button type="button" class="btn btn-sm btn-primary transition-3d-hover mr-1 saveAddress">Save Changes</button>
        <!-- End Buttons -->
      </form>
      <!-- End Personal Info Form -->

    </div>
  </div>
  <!-- End Content Section -->
</main>
<!-- ========== END MAIN ========== -->