<!-- ========== MAIN ========== -->
<main id="content" role="main">
  <div class="container space-top-3 space-bottom-lg-0"></div>
  <!-- Menu Header 2 -->    
  <?php $this->load->view('website/component/profile-menu'); ?>

  <!-- Content Section -->
  <div class="bg-light">
    <div class="container space-2">

      <!-- Personal Info Form -->
      <form class="js-validate" id="formPassword">
        
        <!-- Input -->
        <div class="js-form-message mb-6">
          <label class="form-label">
            Password Lama
          </label>

          <div class="form-group">
            <input type="password" class="form-control password_lama" name="password_lama" placeholder="Password Lama ." aria-label="Password Lama ." required
                   data-msg="Password Lama Anda"
                   data-error-class="u-has-error"
                   data-success-class="u-has-success">
          </div>
        </div>
        <!-- End Input -->

        <!-- Input -->
        <div class="mb-6">
          <div class="js-form-message">
            <label class="form-label">
              Password Baru
            </label>

            <div class="form-group">
              <input id="newPassword" type="password" class="form-control" name="password_baru" placeholder="Password Baru" aria-label="Enter your password" required
                     data-msg="Password baru Anda."
                     data-error-class="u-has-error"
                     data-success-class="u-has-success"
                     data-pwstrength-container="#changePasswordForm"
                     data-pwstrength-progress="#passwordStrengthProgress"
                     data-pwstrength-verdict="#passwordStrengthVerdict"
                     data-pwstrength-progress-extra-classes="bg-white height-4">
            </div>
          </div>
        </div>
        <!-- End Input -->

        <!-- Input -->
        <div class="js-form-message mb-6">
          <label class="form-label">
            Confirm password
          </label>

          <div class="form-group">
            <input type="password" class="form-control" name="confirm_password_baru" placeholder="Konfirm Password baru" aria-label="Konfirm Password baru" required
                   data-msg="Konfirm password baru Anda."
                   data-error-class="u-has-error"
                   data-success-class="u-has-success">
          </div>
        </div>
        <!-- End Input -->
        <!-- Buttons -->
        <input type="hidden" name="id" id="member_id">
        <button type="button" class="btn btn-sm btn-primary transition-3d-hover mr-1 savePassword">Save Changes</button>
        <!-- End Buttons -->
      </form>
      <!-- End Personal Info Form -->

    </div>
  </div>
  <!-- End Content Section -->
</main>
<!-- ========== END MAIN ========== -->

