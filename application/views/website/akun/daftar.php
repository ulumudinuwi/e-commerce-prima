<!-- ========== MAIN ========== -->
  <main id="content" role="main">

    <!-- Content Section -->
    <div class="bg-light ">
      <div class="container space-3">

        <div class="card border-0">
          <div class="card-body p-7">
            <!-- Personal Info Form -->
            <form class="js-validate" id="formDaftar">
              <div class="row">
                <!-- Input -->
                <div class="col-sm-6 mb-1">
                  <div class="js-form-message">
                    <label id="nameLabel" class="form-label">
                      Nama
                    </label>

                    <div class="form-group">
                      <input type="text" class="form-control name" id="name" name="name" value="" placeholder="Nama" aria-label="Nama" required aria-describedby="nameLabel"
                             data-msg="Nama"
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                </div>
                <!-- End Input -->

                <!-- Input -->
                <div class="col-sm-6 mb-1">
                  <div class="js-form-message">
                    <label id="emailLabel" class="form-label">
                      Email
                    </label>

                    <div class="form-group">
                      <input type="text" class="form-control email" id="email" name="email" value="" placeholder="Email" aria-label="Email" required aria-describedby="emailLabel"
                             data-msg="Nama"
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                </div>
                <!-- End Input -->

                <!-- Input -->
                <div class="col-sm-6 mb-1">
                  <div class="js-form-message">
                    <label id="usernameLabel" class="form-label">
                      Username
                    </label>

                    <div class="form-group">
                      <input type="text" class="form-control username" id="username" name="username" value="" placeholder="Username" aria-label="Username" required aria-describedby="usernameLabel"
                             data-msg="Username"
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                </div>
                <!-- End Input -->

                <!-- Input -->
                <div class="col-sm-6 mb-1">
                  <div class="js-form-message">
                    <label id="passwordLabel" class="form-label">
                      Password
                    </label>

                    <div class="form-group">
                      <input type="password" class="form-control password" id="password" name="password" value="" placeholder="Password" aria-label="password" required aria-describedby="passwordLabel"
                             data-msg="Password"
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                </div>
                <!-- End Input -->

                <!-- Input -->
                <div class="col-sm-6 mb-1">
                  <div class="js-form-message">
                    <label id="phoneNumberLabel" class="form-label">
                      Nomor Hp
                    </label>

                    <div class="form-group">
                      <input class="form-control phone_number" id="phone_number" type="tel" name="phone_number" value="" placeholder="No. Handphone" aria-label="No. Handphone" required aria-describedby="phoneNumberLabel"
                             data-msg="No. Handphone"
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                </div>
                <!-- End Input -->

                <!-- Input -->
                <div class="col-sm-6 mb-1">
                  <div class="js-form-message">
                    <label id="kodePromoLabel" class="form-label">
                      Kode Promo
                    </label>

                    <div class="form-group">
                        <select class="custom-select kode_promo" id="kode_promo" name="kode_promo">
                          <option value="">Pilih</option>
                        </select>
                        <input type="hidden" name="kode_promo_tmp" id="kode_promo_tmp">
                        <input type="hidden" name="sales_id" id="sales_id">
                    </div>
                  </div>
                </div>
                <!-- End Input -->

                <!-- Input -->
                <div class="col-sm-6 mb-1">
                  <div class="js-form-message">
                    <label id="AlamatKlinikLabel" class="form-label">
                      Alamat Klinik
                    </label>

                    <div class="form-group">
                      <textarea class="form-control alamat" id="alamat" type="text" name="alamat" value="" placeholder="Alamat Klinik" aria-label="Alamat Klinik" required aria-describedby="AlamatKlinikLabel"
                             data-msg="Alamat Klinik"
                             rows="5" 
                             data-error-class="u-has-error"
                             data-success-class="u-has-success"></textarea>
                    </div>
                  </div>
                </div>
                <!-- End Input -->

                <!-- Input -->
                <div class="col-sm-6 mb-1">
                  <div class="js-form-message">
                    <label id="SIPNumberLabel" class="form-label">
                      Nomor SIP
                    </label>

                    <div class="form-group">
                      <input class="form-control no_sip" id="no_sip" type="tel" name="no_sip" value="" placeholder="No. SIP" aria-label="No. SIP" required aria-describedby="SIPNumberLabel"
                             data-msg="No. SIP"
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                  <div class="js-form-message">
                     <label class="form-label">Foto SIP</label>
                     <div class="form-group">
                       <input class="form-control" type="file" id="foto_sip" name="foto_sip" style="padding: 0.50rem 0.75rem" />
                       <img id="disp_foto_sip" src="" alt="" style="padding:10px;width:200px;">
                     </div>
                  </div>
                </div>
                <!-- End Input -->
                
              </div>

              <!-- Buttons -->
              <input type="hidden" name="id" id="id">
              <button type="button" class="btn btn-sm btn-primary transition-3d-hover mr-1 saveDaftar">Daftar</button>
              <!-- End Buttons -->
            </form>
            <!-- End Personal Info Form -->
          </div>
        </div>

      </div>
    </div>
    <!-- End Content Section -->
  </main>
  <!-- ========== END MAIN ========== -->