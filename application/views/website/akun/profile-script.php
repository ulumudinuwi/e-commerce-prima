<script type="text/javascript">
	//$('.tanggal_lahir').formatter({ pattern: '{{99}}/{{99}}/{{9999}}' });
	function imgPreview(el, target) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(target).attr('src', e.target.result)
        };
        reader.readAsDataURL(el[0].files[0]);
    }

	$(document).ready(function() {
		let param = `?id=${member_id}`,
			uriProfile = url.getProfile+param;

        $.ajax({
          	url: uriProfile,
          	type: 'GET',
          	dataType: 'json',
          	success: function(res) { 
          		data = res.data

          		$('#sales_id').val(data.sales_id);
          		$('#dokter_id').val(data.id);
				$('#name').val(data.nama);
				$('#email').val(data.email);
				$('#profilePoint').html(numeral(data.point).format());
				$('#profileName').html(data.nama);
				$('#profileEmail').html(data.email);
				$('#phone_number').val(data.no_hp);
				$('#nama_klinik').val(data.nama_klinik);
				$('#tempat_lahir').val(data.tempat_lahir);
				$('#tanggal_lahir').val(data.tgl_lahir);
				$('#no_ktp').val(data.no_ktp);
				$('#kode_promo').val(data.kode_promo);
				$('#no_sip').val(data.no_sip);
				$('#disp_foto_sip').prop('src', data.url_foto_sip);
				$('#jenis_kelamin').val(data.jenis_kelamin).trigger('change');

				//alamat
				$('#kode_pos').val(data.kode_pos);
				$('#nama_gedung').val(data.gedung_jalan);
				$('#kota_tmp').val(data.kota);
				$('#kecamatan_tmp').val(data.kecamatan);
				$('#provinsi').val(data.provinsi).trigger('change');

				//alamat Hidden
				$('#aProvinsi').val(data.provinsi);
				$('#aKota').val('455'); // 455 (Tangerang)
				$('#aKecamatan').val(data.kecamatan);
				$('#courier').val('jne:tiki');

	            let alamat = data.nama +" ("+ data.no_hp +") , "+data.gedung_jalan+" , "+data.kota_tmp+" - "+data.kecamatan_tmp+" , "+data.provinsi_tmp+" , ( "+data.kode_pos+" )";
	            $("#alamatPengiriman").html(alamat);

				/*if (res.data.foto.length > 0) {
		    		for (var i = 0; i < res.data.foto.length; i++) {
						fillFoto(res.data.foto[i], i);
					}
		    	}*/
    			
    			//$.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');
          	},
          	error: function() { 
          		console.log('aa');
          	},
          	beforeSend: setHeader
        });

        // $('body').on('change keyup', '.tanggal_lahir', function() {
        //     let id = $(this).prop('id');
        //     let parent = $(this).parent().parent();
        //     parent.find('small').text('');
        //     let value = $(this).val();
        //     if(value == '') return;

        //     let day = value.substr(0, 2);
        //     day = isNaN(day) ? 0 : day;
        //     let month = value.substr(3, 2);
        //     month = isNaN(month) ? 0 : month;
        //     let year = value.substr(6, 4);
        //     year = isNaN(year) ? 0 : year;

        //     if(parseInt(day) > 31 || parseInt(month) > 12 || year.length < 4) {
        //         $(".saveProfile").prop('disabled', true);
        //         parent.find('small').text('Format Tanggal Salah');
        //     } else {
        //     	$(this).val(value);
        //         $(".saveProfile").prop('disabled', false);
        //     }
        // });

        $('#btnImageProfile').on('change', function() {
        	imgPreview($(this), '.img-profile');
        });
		
		$('#foto_sip').on('change', function() {
			imgPreview($(this), '#disp_foto_sip');
		});
	});

	function fillFoto(data, index){
    	let appentHtml = ''

    	if (data) {
    		appentHtml += '<div class="js-slide rounded bg-img-hero min-height-300" style="background-image: url('+data.original+');"></div>';
    	}
		$('.appendItem').append(appentHtml);			
    }

	function setHeader(xhr) {
		xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
	}

	$('.saveProfile').on('click', function(e) {
		e.preventDefault();
		$(formProfile).submit();
	});

	$(formProfile).validate({
	    rules: {},
	    focusInvalid: true,
	    errorPlacement: function(error, element) {
	        var placement = $(element).closest('.input-group');
	        if (placement.length > 0) {
	            error.insertAfter(placement);
	        } else {
	            error.insertAfter($(element));
	        }
	    },
	    submitHandler: function (formProfile) {
	        $('input, textarea, select').prop('disabled', false);

	        blockPage('Sedang diproses ...');
	        var postData = $(formProfile).serializeArray();
            var formData = new FormData($(formProfile)[0]);

            for (var i = 0; i < postData.length; i++) {
                if (postData[i].name != 'image_profile' && postData[i].name != 'foto_sip') {
                    formData.delete(postData[i].name);
                    formData.append(postData[i].name, postData[i].value);
                }
            }

	        $.ajax({
	          data: formData,
	          type: 'POST',
	          dataType: 'JSON', 
	          processData: false,
              contentType: false, 
	          url: url.saveProfile,
	          headers: {
						"Authorization": "PRIMA-uvXL68GB5THBN8cUIFuM"
						},
	          success: function(data){
	              	$.unblockUI();

					$('#AlertModal').modal('show');
					$('#dispNotif').html('Success ...');
					
					//set localstorage
                	localStorage.setItem('prima-userdata', JSON.stringify(data.data.data_user)); 

	              	setTimeout(function () {
						$('#AlertModal').modal('hide');
			            window.location.reload();
	                }, 3000);
	          },
	          error: function(data){
	              $.unblockUI();

				  $('#AlertModal').modal('show');
				  $('#dispNotif').html('Gagal ...');
				  setTimeout(function () {
					$('#AlertModal').modal('hide');
                }, 2000);
	          }
	        });
	        return false;
	    }
	});

	$('.saveAddress').on('click', function(e) {
		e.preventDefault();
		$(formAddress).submit();
	});

	$(formAddress).validate({
	    rules: {},
	    focusInvalid: true,
	    errorPlacement: function(error, element) {
	        var placement = $(element).closest('.input-group');
	        if (placement.length > 0) {
	            error.insertAfter(placement);
	        } else {
	            error.insertAfter($(element));
	        }
	    },
	    submitHandler: function (formAddress) {
	        $('input, textarea, select').prop('disabled', false);

	        blockPage('Sedang diproses ...');
			var formData = $(formAddress).serialize();
	        $.ajax({
	          data: formData,
	          type: 'POST',
	          dataType: 'JSON', 
	          url: url.saveAddress,
	          headers: {
						"Authorization": "PRIMA-uvXL68GB5THBN8cUIFuM"
						},
	          success: function(data){
	              	$.unblockUI();

					$('#AlertModal').modal('show');
					$('#dispNotif').html('Success ...');
					
					//set localstorage
                	localStorage.setItem('prima-userdata', JSON.stringify(data.data.data_user)); 

	              	setTimeout(function () {
						$('#AlertModal').modal('hide');
			            window.location.reload();
	                }, 3000);
	          },
	          error: function(data){
	              $.unblockUI();

				  $('#AlertModal').modal('show');
				  $('#dispNotif').html('Gagal ...');
				  setTimeout(function () {
					$('#AlertModal').modal('hide');
                }, 2000);
	          }
	        });
	        return false;
	    }
	});

	$('.savePassword').on('click', function(e) {
		e.preventDefault();
		$(formPassword).submit();
	});

	$(formPassword).validate({
	    rules: {
    		confirm_password_baru: {
                equalTo: "#newPassword"
            }
        },
        messages: {
            confirm_password_baru: " Password Tidak Sama"
        },
	    focusInvalid: true,
	    errorPlacement: function(error, element) {
	        var placement = $(element).closest('.input-group');
	        if (placement.length > 0) {
	            error.insertAfter(placement);
	        } else {
	            error.insertAfter($(element));
	        }
	    },
	    submitHandler: function (formPassword) {
	        $('input, textarea, select').prop('disabled', false);

	        blockPage('Sedang diproses ...');
	        var formData = $(formPassword).serialize();
	        $.ajax({
	          data: formData,
	          type: 'POST',
	          dataType: 'JSON', 
	          url: url.savePassword,
	          headers: {
						"Authorization": "PRIMA-uvXL68GB5THBN8cUIFuM"
						},
	          success: function(data){
	              	$.unblockUI();

					$('#AlertModal').modal('show');
					$('#dispNotif').html('Success ...');
					
					//set localstorage
                	localStorage.setItem('prima-userdata', JSON.stringify(data.data.data_user)); 

	              	setTimeout(function () {
						$('#AlertModal').modal('hide');
			            window.location.reload();
	                }, 3000);
	          },
	          error: function(data){
	              $.unblockUI();

				  $('#AlertModal').modal('show');
				  $('#dispNotif').html('Gagal ...');
				  setTimeout(function () {
					$('#AlertModal').modal('hide');
                }, 2000);
	          }
	        });
	        return false;
	    }
	});
</script>