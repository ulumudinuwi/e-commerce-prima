<!-- ========== MAIN ========== -->
<main id="content " role="main">
    <!-- Hero Section -->
    <div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll"
         data-options='{direction: "normal"}'>
	      <!-- Apply your Parallax background image here -->
	          <div class="d-lg-flex align-items-lg-center u-hero-v1__main" style="background-image: url(../assets/img/others/news_3.jpg);">

	      <!-- Content -->
	      <div class="container position-relative space-2 space-top-md-5 space-bottom-md-3 z-index-2">
		        <div class="w-lg-80 text-center mx-auto">
		          <h1 class="display-3 font-size-md-down-5 text-white font-weight-semi-bold">Contact Us</h1>
		          <p class="lead text-white">For more inquiries you can chat us at</p>
		        </div>
	      </div>
	      <!-- End Content -->
    </div>
    <!-- End Hero Section -->

    <!-- Contacts Info Section -->
    <div class="clearfix">
      <div class="row no-gutters">
        <div class="col-sm-6 col-lg-4 u-ver-divider u-ver-divider--none-lg">
          <!-- Contacts Info -->
          <div class="text-center p-5">
            <figure id="icon8" class="svg-preloader ie-height-56 max-width-8 mx-auto mb-3">
              <img class="js-svg-injector" src="<?php echo assets_url('svg/icons/icon-8.svg') ?>" alt="SVG"
                   data-parent="#icon8">
            </figure>
            <h2 class="h6 mb-0">Address</h2>
            <p class="mb-0">PT PRIMA ESTETIKA RAKSA</p>
            <p class="mb-0">Green Lake City Boulevard </p>
            <p class="mb-0">Rukan Greatwall Blok C17 RT.001 RW.008 Kelurahan Gondrong Kecamatan Cipondoh, Kota Tangerang, Banten 15146</p>
          </div>
          <!-- End Contacts Info -->
        </div>

        <div class="col-sm-6 col-lg-4 u-ver-divider u-ver-divider--none-lg">
          <!-- Contacts Info -->
          <div class="text-center p-5">
            <figure id="icon15" class="svg-preloader ie-height-56 max-width-8 mx-auto mb-3">
              <img class="js-svg-injector" src="<?php echo assets_url('svg/icons/icon-15.svg') ?>" alt="SVG"
                   data-parent="#icon15">
            </figure>
            <h3 class="h6 mb-0">Email</h3>
            <p class="mb-0">ptprimaestetikaraksa@gmail.com</p>
          </div>
          <!-- End Contacts Info -->
        </div>

        <div class="col-sm-6 col-lg-4 u-ver-divider u-ver-divider--none-lg">
          <!-- Contacts Info -->
          <div class="text-center p-5">
            <figure id="icon16" class="svg-preloader ie-height-56 max-width-8 mx-auto mb-3">
              <img class="js-svg-injector" src="<?php echo assets_url('svg/icons/icon-16.svg') ?>" alt="SVG"
                   data-parent="#icon16">
            </figure>
            <h3 class="h6 mb-0">Phone Number</h3>
            <p class="mb-0">021 5431 3244</p>
          </div>
          <!-- End Contacts Info -->
        </div>
      </div>
    </div>
    <!-- End Contacts Info Section -->

    <hr class="my-0">

    <!-- Contact Form Section -->
    <div class="container space-1">
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
        <p>Or you can write to us by filling the following form</p>
      </div>
      <!-- End Title -->

      <div class="w-lg-80 mx-auto">
        <!-- Contacts Form -->
        <form id="form">
          <div class="row">
            <!-- Input -->
            <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label class="form-label">
                   Name
                  <span class="text-danger">*</span>
                </label>

                <input type="text" class="form-control" name="name" placeholder="Name ..." aria-label="Name ..." required
                       data-msg="Please enter your name."
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
              </div>
            </div>
            <!-- End Input -->

            <!-- Input -->
            <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label class="form-label">
                  Your email
                  <span class="text-danger">*</span>
                </label>

                <input type="email" class="form-control" name="email" placeholder="Email ..." aria-label="Email ..." required
                       data-msg="Please enter a valid email address."
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
              </div>
            </div>
            <!-- End Input -->

            <div class="w-100"></div>

            <!-- Input -->
            <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label class="form-label">
                  Subject
                  <span class="text-danger">*</span>
                </label>

                <input type="text" class="form-control" name="subject" placeholder="Subject ..." aria-label="Subject ..." required
                       data-msg="Please enter a subject."
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
              </div>
            </div>
            <!-- End Input -->

            <!-- Input -->
            <div class="col-sm-6 mb-6">
              <div class="js-form-message">
                <label class="form-label">
                  Phone Number
                  <span class="text-danger">*</span>
                </label>

                <input type="number" class="form-control" name="phone_number" placeholder="Phone Number ..." aria-label="Phone Number ..." required
                       data-msg="Please enter a valid phone number."
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
              </div>
            </div>
            <!-- End Input -->
          </div>

          <!-- Input -->
          <div class="js-form-message mb-6">
	            <label class="form-label">
	              Description
	              <span class="text-danger">*</span>
	            </label>

	            <div class="input-group">
	              <textarea class="form-control" rows="4" name="description" placeholder="Description ..." aria-label="Description ..." required
	                        data-msg="Please enter a reason."
	                        data-error-class="u-has-error"
	                        data-success-class="u-has-success"></textarea>
	            </div>
          </div>
          <!-- End Input -->

          <div class="text-center">
            <button type="button" class="btn btn-primary btn-wide transition-3d-hover mb-4 save">Submit</button>
          </div>
        </form>
        <!-- End Contacts Form -->
      </div>
    </div>
    <!-- End Contact Form Section -->
</main>
<!-- ========== END MAIN ========== -->

<?php $this->load->view('website/contact_us-script'); ?>
