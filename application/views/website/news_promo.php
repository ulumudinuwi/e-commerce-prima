<!-- ========== MAIN ========== -->
<main id="content " role="main">
    
    <!-- Blog Grid Section -->
    <div class="container space-4 space-top-md-5 space-top-sm-5 space-top-lg-3">
      <!-- Title -->
      <div class="w-md-80 w-lg-60 mb-9">
        <h1 class="font-weight-normal">Our latest - <span class="text-primary font-weight-semi-bold">News & Promotions</span></h1>
      </div>
      <!-- End Title -->

      <div class="row">
        <div class="col-lg-9 order-lg-2 mb-9 mb-lg-0">
          <div class="card-deck d-block d-md-flex card-md-gutters-2 appendItem">
            
          </div>

        </div>

        <div id="stickyBlockStartPoint" class="col-lg-3 order-lg-1">
          <!-- Sticky Block -->
          <div class="js-sticky-block"
               data-offset-target="#logoAndNav"
               data-parent="#stickyBlockStartPoint"
               data-sticky-view="lg"
               data-start-point="#stickyBlockStartPoint"
               data-end-point="#stickyBlockEndPoint"
               data-offset-top="32"
               data-offset-bottom="170">
            <h3 class="h5 text-primary font-weight-semi-bold mb-4">News</h3>

            <!-- Thumbnail News -->
            <div class="appendItemNews">
              
            </div>
            <!-- End Thumbnail News -->

            <!-- <hr class="my-7">

            <h3 class="h5 text-primary font-weight-semi-bold mb-4">Menu</h3> -->

            <!-- Tags -->
            <!-- <ul class="list-inline mb-0">
              <li class="list-inline-item pb-3">
                <a class="btn btn-xs btn-secondary btn-pill" href="<?php echo base_url() ?>">Home</a>
              </li>
              <li class="list-inline-item pb-3">
                <a class="btn btn-xs btn-secondary btn-pill" href="<?php echo base_url('#aboutus') ?>">About Us</a>
              </li>
              <li class="list-inline-item pb-3">
                <a class="btn btn-xs btn-secondary btn-pill" href="<?php echo site_url('product') ?>">Product</a>
              </li>
              <li class="list-inline-item pb-3">
                <a class="btn btn-xs btn-secondary btn-pill" href="<?php echo site_url('contact_us') ?>">Contact Us</a>
              </li>
            </ul> -->
            <!-- End Tags -->

          </div>
          <!-- End Sticky Block -->
        </div>
      </div>
    </div>
    <!-- End Blog Grid Section -->
</main>
<!-- ========== END MAIN ========== -->

<?php $this->load->view('website/news_promo-script'); ?>