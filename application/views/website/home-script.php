<script type="text/javascript">
	$(document).ready(function() {
		let form = $('#form');

		$(document).ready(function() {
	        /**/
	        if (!userdata) {
	        	fillBarangProductLogin({'bpom' : 1});
	        }else{
	        	fillBarangProductLogin({});
	        }
	        $.ajax({
	          	url: url.getPromo,
	          	type: 'GET',
	          	dataType: 'json',
	          	success: function(res) { 
	          		if (res.list.length > 0) {
			    		for (var i = 0; i < res.list.length; i++) {
							fillPromo(res.list[i], i);
						}
			    	}else{
			    		$('.appendItem').addClass('text-center');
						$('.appendItem').append('<div class="col-md-12 space-1">'
												  +'<span class="u-divider u-divider--text text-dark">Tidak ada Promo tersedia. </span>'
												+'</div>');
			    	}
	          	},
	          	error: function() { 
	          		console.log('error');
	          	},
	          	beforeSend: setHeader
	        });
	    });

	    function setHeader(xhr) {
	        xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
	    }

	    /**/
	    
	    function fillPromo(data, index){
	    	let appentHtml = '',
	    	uriUpload = "<?php echo $this->config->item('api_base_uri')?>";
	    	if (data) {
	    		appentHtml += '<div class="col-md-4 mb-3">'
								  +'<!-- Blog News -->'
								  +'<a class="d-flex align-items-end bg-img-hero gradient-overlay-half-dark-v1 transition-3d-hover rounded-pseudo height-450" style="background-image: url('+uriUpload+'/uploads/promo/'+data.banner_id+');" href="<?php echo site_url('news_promo/detail_promo') ?>'+'/'+data.uid+'">'
								    +'<article class="w-100 text-center p-6">'
								      +'<h2 class="h4 text-white">'+data.nama_barang+'</h2>'
								      +'<div class="mt-4">'
								        +'<div class="u-avatar mx-auto mb-3">'
								          +'<img class="img-fluid " src="'+(data.foto.length > 0 ? data.foto[0].original : "<?php echo base_url('assets/img/others/no_image_available.png'); ?>" ) +'" alt="Image Description">'
								        +'</div>'
								        +'<strong class="d-block text-white-70"> Expired At : '+ data.expired_at +'</strong>'
								      +'</div>'
								    +'</article>'
								  +'</a>'
								  +'<!-- End Blog News -->'
								+'</div>';
	    	}
			
			$('.appendItem').append(appentHtml);
	    }

	});
</script>