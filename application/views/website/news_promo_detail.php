
  
<style type="text/css">
 
 .u-hero-v1__main {
    position: relative;
    z-index: 1; 
    background-size: cover;
    background-repeat: no-repeat;
    background-position: top center;
    margin-top: 7rem;
  }
</style>
<!-- ========== MAIN ========== -->
<main id="content " role="main">
    <!-- Hero Section -->
    <div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll"
         data-options='{direction: "normal"}'>
        <!-- Apply your Parallax background image here -->
            <div class="d-lg-flex align-items-lg-center u-hero-v1__main" id="headerbanner">

        <!-- Content -->
        <div class="container position-relative space-4 mb-5 mb-md-0 mb-lg-0">
            <div class="w-lg-80 text-center mx-auto">
              <h4 class="display-4 font-size-md-down-5 text-white font-weight-semi-bold" id="titlePromo"></h4>
              <p class="lead text-white" id="titleDesc"></p>
            </div>
        </div>
        <!-- End Content -->
    </div>
    <!-- End Hero Section -->

   
    <!-- Images Carousel -->
    <div class="container">
      <div class="w-lg-80 mx-auto space-top-md-1">
        <div class="js-slick-carousel2 u-slick u-slick--gutters-2 appendItem"
             data-infinite="true"
             data-slides-show="1"
             data-slides-scroll="1"
             data-center-mode="true"
             data-center-padding="80px"
             data-pagi-classes="text-center u-slick__pagination mt-5 mb-0"
             data-responsive='[{
               "breakpoint": 992,
               "settings": {
                 "centerPadding": "60px"
               }
             }, {
               "breakpoint": 768,
               "settings": {
                 "centerPadding": "40px"
               }
             }, {
               "breakpoint": 554,
               "settings": {
                 "centerPadding": "20px"
               }
             }]'>
        </div>
      </div>
    </div>
    <!-- End Images Carousel -->

    <!-- Description Section -->
    <div class="container space-top-md-1">
      <div class="w-lg-60 mx-auto">
        <div class="mb-4">
          <span class="text-bold" id="titleDate"></span>
        </div>

        <div class="mb-5 text-black text-justify" id="titlelongDesc">

        </div>
      </div>
    </div>
    <!-- End Description Section -->

</main>
<!-- ========== END MAIN ========== -->

<?php $this->load->view('website/news_promo_detail-script'); ?>