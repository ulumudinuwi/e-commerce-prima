  	<!-- ========== MAIN ========== -->
  	<main id="content" role="main">
		<!-- header -->
  		<!-- Hero v1 Section -->
  		<div class="u-hero-v1 space-3">
  			<!-- Hero Carousel Main -->
  			<div id="heroNav" class="js-slick-carousel u-slick" data-autoplay="false" data-infinite="true" data-speed="10000" data-adaptive-height="true" data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle" data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4" data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4" data-numbered-pagination="#slickPaging" data-nav-for="#heroNavThumb">

	  			<!-- Gluta Optime -->
	  			<div class="js-slide">
	  				<!-- Slide #1 -->
	  				<div class="d-lg-flex align-items-lg-center u-hero-v1__main" style="background-color: white">
	  					<div class="container space-top-md-5 space-top-lg-2">
	  						<div class="row">

	  							<!-- Front in Frames Section -->
	  							<div class="overflow-hidden">
	  								<div class="container space-1 space-md-2">
	  									<div class="row justify-content-between align-items-center">

	  										<div class="col-lg-5 position-relative">
	  											<!-- Image Gallery -->
	  											<div class="row mx-gutters-2 section_gluta" >
	  												<div class="col-12" align="center">
	  													<img class="img-fluid rounded" src="<?php echo assets_url('img/others/gluta_optime_bc.png')?>" alt="Image Description">
	  												</div>
	  											</div>
	  											<div class="row mx-gutters-2 section_male_optime" >
	  												<div class="col-12" align="center">
	  													<img class="img-fluid rounded" src="<?php echo assets_url('img/others/male_optime_bc.png')?>" alt="Image Description">
	  												</div>
	  											</div>
	  											<div class="row mx-gutters-2 section_fesklin" >
	  												<div class="col-12" align="center">
	  													<img class="img-fluid rounded" src="<?php echo assets_url('img/others/fesklin_bc.png')?>" alt="Image Description">
	  												</div>
	  											</div>
	  											<!-- End Image Gallery -->

	  										</div>
	  										<div class="col-lg-7 mb-7 mb-lg-0">
	  											<div class="pr-md-4">
	  												<!-- Title -->
	  												<div class="mb-7 text-justify">
	  													<h1 class="d-block  mb-2 text-primary text-bold text-center" data-scs-animation-in="fadeInUp">
		  													<span class="font-weight-semi-bold"><?php echo $page_title ?></span> 
		  												</h1>

		  												<!-- gluta Optime -->
		  												<p class="text-black-70 mb-2 section_gluta" data-scs-animation-in="fadeInUp" data-scs-animation-delay="200">
			  												Gluta Optime mengandung Glutathione yang merupakan Master Antioksidan dan
			  												Detoksifikasi. Meningkatnya Glutathione dalam tubuh akan meningkatkan sistem imun
			  												secara signifikan dan badan terasa lebih segar dan berenergi.
			  											</p>

			  											<h4 class="d-block mb-2 text-white text-bold " data-scs-animation-in="fadeInUp">
				  											<span class="btn btn-sm btn-soft-info mb-2 font-weight-semi-bold section_gluta">POM SD 192 356 031</span>
			  											</h4>

			  											<!-- male Optime -->
			  											<p class="text-black-70 mb-2 section_male_optime" data-scs-animation-in="fadeInUp" data-scs-animation-delay="200">
														 	Male Optime memiliki manfaat yang sangat baik sebagai suplemen pria dalam keseharian. Seiring bertambahnya usia, kemampuan tubuh untuk mempertahankan massa otot akan semakin berkurang. Lutut ( persendian) akan mengalami rasa nyeri / lemas serta sirkulasi darah yang kurang. Akibatnya secara fisik, kita akan mudah merasa lelah dan kurang berenergi. Male Optime dengan 3 kandungan herbal yang terbukti sangat baik bagi kesehetan pria dapat membantu mengatasi masalah kesehatan tersebut.
														</p>

														<p class="text-black-70 mb-2 section_male_optime" data-scs-animation-in="fadeInUp" data-scs-animation-delay="200">
														 Menjadi sehat adalah berkah bagi anda dan keluarga. Dengan mengkonsumsi Male Optime 2x sehari. Kualitas kesehatan hidup anda menjadi 2x lebih baik.
														</p>

			  											<h4 class="d-block mb-2 text-white text-bold" data-scs-animation-in="fadeInUp">
				  											<span class="btn btn-sm btn-soft-info mb-2 font-weight-semi-bold section_male_optime">POM TR 192 331 911</span>
			  											</h4>

			  											<!-- fesklin -->
			  											<p class="text-black-70 mb-2 section_fesklin" data-scs-animation-in="fadeInUp" data-scs-animation-delay="200">
															Fesklin mengandung Lima bahan alami (monk's pepper (chasteberry), milk thistle, artichoke, brokoli, dan kunyit) yang bekerja sinergis untuk memelihara kesehatan kulit pria dan wanita.
														</p>

														<p class="text-black-70 mb-2 section_fesklin" data-scs-animation-in="fadeInUp" data-scs-animation-delay="200">
															Bagi wanita, formula unik ini akan membantu menyeimbangkan hormon dalam tubuh sehingga mampu membantu mengobati jerawat pra-menstruasi. Mampu menghambat radikal bebas dari paparan sinar UV dan juga mengandung agen antibakteri serta antioksidan alami yang mampu mencegah pertumbuhan bakteri penyebab jerawat.
														</p>

			  											<h4 class="d-block mb-2 text-white text-bold " data-scs-animation-in="fadeInUp">
				  											<span class="btn btn-sm btn-soft-info mb-2 font-weight-semi-bold section_fesklin">POM TR 182 320 551</span>
			  											</h4>
					  								</div>
					  							</div>
					  						</div>
	  									</div>
					  				</div>
					  			</div>
					  		</div>
					  	</div>
  					</div>
				</div>
			</div>
			<!-- End Hero Carousel Main -->
		</div>

		<!-- Manfaat -->
		<!-- Gluta Section -->
		<div class="container space-1 section_gluta">
			<!-- Title -->
			<div class="w-md-80 w-lg-70 text-center mx-md-auto mb-9 text-bold">
				<h3 class="btn btn-lg bg-info btn-pill mb-2 text-white">Manfaat <?php echo $page_title ?></h3>
			</div>
			<!-- End Title -->

			<div class="row">
				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">01.</span>
						</span>
						<div class="media-body">
							<p> Membantu penghancuran Radikal Bebas - Penyebab penyakit seperti Kanker, Hepatitis, dll</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">02.</span>
						</span>
						<div class="media-body">
							<p>Mempercepat regenerasi sel yang rusak</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">03.</span>
						</span>
						<div class="media-body">
							<p>Mencerahkan & Menghaluskan Kulit dengan memproduksi sel kulit baru</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">04.</span>
						</span>
						<div class="media-body">
							<p> Membantu Pembakaran Lemak sehingga Berat Badan Lebih Terkontrol</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-sm-6 col-lg-6 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">05.</span>
						</span>
						<div class="media-body">
							<p>meningkatkan kesehatan secara umum sehingga menurukan resiko auto imun, serangan jantung, strokes, kemandulan, dll.</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<!-- Title -->
				<div class="text-center mx-md-auto mt-6">
					<p>Manfaat lainnya adalah kasiatnya untuk mencerahkan kulit secara sehat & alami. glutaoptime memiliki kandungan 500mg glutathione dalam setiap kapsulnya dan 5 bahan
						penting dan sangat bermanfaat bagi tubuh. Gluta Optime bersertifikat BPOM yang teruji
					aman untuk konsumsi sebagai suplement harian.</p>
				</div>
				<!-- End Title -->
			</div>
		</div>
		<!-- End Gluta Section -->

		<!-- Male Optime Section -->
		<div class="container space-1 section_male_optime">
			<!-- Title -->
			<div class="w-md-80 w-lg-70 text-center mx-md-auto text-bold">
				<h3 class="btn btn-lg bg-info btn-pill mb-2 text-white">Manfaat <?php echo $page_title ?></h3>
			</div>
			<div class="w-md-80 w-lg-70 text-center mx-md-auto mb-9">
				<p>Bagi Pria yang ingin tampil Prima dan pastinya menjaga kesehatan tubuh, Male Optime merupakan jawaban atas keinginan anda itu. Suplemen herbal ini khusus untuk pria dewasa yang ingin</p>
			</div>
			<!-- End Title -->

			<div class="row">
				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">01.</span>
						</span>
						<div class="media-body">
							<p>Meningkatkan Stamina</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">02.</span>
						</span>
						<div class="media-body">
							<p>Meningkatkan kesehatan seksual</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">03.</span>
						</span>
						<div class="media-body">
							<p>Memperbaiki kualitas sperma</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">04.</span>
						</span>
						<div class="media-body">
							<p> Meningkatkan massa otot</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>

				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">05.</span>
						</span>
						<div class="media-body">
							<p>Memperbaiki sirkulasi darah</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>
				<div class="col-sm-6 col-lg-4 mb-7">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">06.</span>
						</span>
						<div class="media-body">
							<p>Menormalkan hormone Kortisol</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>
				<div class="col-sm-6 col-lg-6 mb-7 mb-sm-0">
					<!-- Icon Blocks -->
					<div class="media">
						<span class="btn btn-sm btn-icon btn-soft-primary rounded-circle mr-3">
							<span class="btn-icon__inner font-weight-semi-bold">07.</span>
						</span>
						<div class="media-body">
							<p>Mengurangi Nyeri Lutut</p>
						</div>
					</div>
					<!-- End Icon Blocks -->
				</div>
				<!-- Title -->
				<div class="text-center mx-md-auto mt-6">
					<p>Optime Life menawarkan anda untuk dapat kembali beraktivitas dengan nyaman serta memiliki kualitas kehidupan yang prima.</p>
				</div>
				<!-- End Title -->
			</div>
		</div>
		<!-- End Male Optime Section -->

		<!-- komposisi -->
		<!-- Gluta Section -->
		<div class="container space-1 section_gluta">
			<!-- Title -->
			<div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
				<h3 class="btn btn-lg bg-info btn-pill mb-2 text-white">Komposisi <?php echo $page_title ?></h3>
			</div>
			<!-- End Title -->

			<!-- Gluta -->
			<div class="space-bottom-2">
				<div class="row">
					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">GLUTATHIONE</h3>
						<p>
							Gluta Optime mengandung Glutathione yang merupakan Master Antioksidan dan
							Detoksifikasi. Glutathione atau Gluta dapat diproduksi oleh tubuh kita - namun seiring
							dengan bertambahnya usia serta banyaknya kandungan zat yang bersifat racun dari
							makanan ataupun polusi, produksi Gluta akan menurun sehingga kita membutuhkan
							supplement untuk meningkatkan kadar gluta dalam tubuh.
						</p>
						<p>
							Penurunan produksi ini dapat menyebabkan berbagai macam masalah kesehatan, salah
							satunya seperti kanker. Itu sebabnya setiap Gluta Optime menjadi pilihan supplement yang
							disarankan oleh para dokter. Selain membantu sistem imune, Gluta dapat membantu
							memperbaiki sel kulit yang rusak - efeknya kulit menjadi lebih cerah
						</p>
					</div>

					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">HAEMATOCCOCUS PLUVIALIS</h3>
						<p>
							Haematoccocus Pluvialis adalah sejenis gangang hijau yang dikenal dengan
							kemampuannya memproduksi 95% Astaxanthin - sejenis antioksidan yang karotenoid
							yang memiliki kemampuan alami untuk bertahan melawan efek negatif akibat radikal
							bebas atau oksigen aktif
						</p>
						<p>
							Haematoccocus Pluvialis membantu mengurangi garis-garis halus, bintik-bintik penuaan
							dan menjaga kelembapan kulit. Kandungan Haematoccocus Pluvialis
							dalam glutaoptime akan mencerahkan, melembabkan dan mencegah penuaan dini pada
							kulit.
						</p>
					</div>

					<div class="w-100"></div>

					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">VITAMIN C</h3>
						<p>
							Gluta Optime mengandung Vitamin C yang bisa meningkatkan imun anda, dapan
							menetralkan replikasi virus, vitaminC juga dapat mencegah penuaan dini. Sayangnya, Vit C
							tidak diproduksi oleh tubuh. Untuk mendapat asupan yang mengandung Vitamin C, anda
							dapat mengonsumsi buah dan sayuran segar secara rutin.
						</p>
					</div>

					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">ALPHA LIPOIC ACID (ALA)</h3>
						<p>
							Berperan dalam regenerasi dan perpanjangan masa aktif antioksidan glutathione, koenzim
							Q10, vitamin C, dan vitamin E serta dapat menekan respon inflamasi dengan cara
							menghambat jalur sinyal molekul yang teraktivasi oleh sitokin proinflamatori seperti tumor
							necrosis factor-α, sebagai anti-aging dan agen detoksifikasi. Tak hanya itu, beberapa fungsi lain dari asam alfa lipoat yaitu meringankan gejala dari
							kerusakan saraf dan mengurangi risiko kerusakan mata.
						</p>
					</div>

					<div class="w-100"></div>

					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">ZINC</h3>
						<p>
							Zinc memainkan peran penting dalam pertumbuhan dan perkembangan, respons imun,
							fungsi neurologis, dan reproduksi. Zinc dapat ditemukan dalam makanan seperti daging
							dan kacang -kacangan. Namun karena penyerapannya cukup sulit bagi tubuh, supplement
							sangat disarankan untuk meningkatkan zinc dalam tubuh.

						</p>
					</div>
				</div>
			</div>
			<!-- End Gluta -->

			<!-- Divider -->
			<div class="w-80 mx-auto">
				<hr class="my-0">
			</div>
			<!-- End Divider -->
		</div>
		<!-- End Gluta Section -->

		<!-- Male Optime Section -->
		<div class="container space-1 section_male_optime">
			<!-- Title -->
			<div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
				<h3 class="btn btn-lg bg-info btn-pill mb-2 text-white">Komposisi <?php echo $page_title ?></h3>
			</div>
			<!-- End Title -->

			<!-- Male Optime -->
			<div class="space-bottom-2">
				<div class="row">
					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">MUIRA PUAMA (PTYCHOPETALUM OLACOIDES)</h3>
						<p>
							Tanaman herbal rakyat Amazon yang paling terkenal meningkatkan libido, kekerasan penis dan kinerja seksual. Selain itu dapat mengurangi nyeri lutut.
						</p>
						<p>
							Penurunan produksi ini dapat menyebabkan berbagai macam masalah kesehatan, salah
							satunya seperti kanker. Itu sebabnya setiap Gluta Optime menjadi pilihan supplement yang
							disarankan oleh para dokter. Selain membantu sistem imune, Gluta dapat membantu
							memperbaiki sel kulit yang rusak - efeknya kulit menjadi lebih cerah
						</p>
					</div>

					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">RED KWAO KRUA (BUTEA SUPERBA)</h3>
						<p>
							Merupakan tanaman yang telah lama digunakan dalam pengobatan tradisional di Thailand. Biasanya digunakan dalam pengobatan untuk memicu peningkatan emosional pada respons seksual, jumlah sperma, dan motilitas. Fungsi lainnya mengurangi atau mencegah rasa sakit saat buang air kecil.
						</p>
					</div>

					<div class="w-100"></div>

					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">HORNY GOAT WEED (EPIMEDIUM SAGITATUM)</h3>
						<p>
							Memiliki sejarah panjang digunakan dalam pengobatan Timur tradisional. Tanaman ini berasal dari bagian Cina, Jepang, dan Korea. Horny goat weed bermanfaat meningkatkan ereksi dan produksi hormone testosterone sehingga meningkatkan jumlah sperma. Fungsi lainnya menormalkan hormone kortisol, memperlancar sirkulasi darah, dan meningkatkan masa otot.
						</p>
					</div>

					<div class="col-md-6 mb-5">
						<p>Isi kemasan : 60 Kapsul</p>
						<p>Saran konsumsi : Minum 2 kapsul, 2 kali sehari</p>
					</div>

					<div class="col-md-6 mb-5">
						<p>Takaran Sajian Per Kapsul :</p>
						<p> - Muira puama (Ptychopetalum olacoides) 	100 mg</p>
						<p> - Red kwao krua (Butea superba)		100 mg</p>
						<p> - Horny goat weed (Epimedium sagitatum)	100 mg</p>
					</div>
				</div>
			</div>
			<!-- End Male Optime -->

			<!-- Divider -->
			<div class="w-80 mx-auto">
				<hr class="my-0">
			</div>
			<!-- End Divider -->
		</div>
		<!-- End Male Optime Section -->

		<!-- Male Optime Section -->
		<div class="container space-1 section_fesklin">
			<!-- Title -->
			<div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
				<h3 class="btn btn-lg bg-info btn-pill mb-2 text-white">Komposisi <?php echo $page_title ?></h3>
			</div>
			<!-- End Title -->

			<!-- Male Optime -->
			<div class="space-bottom-2">
				<div class="row">
					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">VITEX AGNUS-CASTUS (CHASTEBERRY)</h3>
						<p>
							Membantu menyeimbangkan hormon di dalam tubuh untuk mencegah timbulnya jerawat.
						</p>
					</div>

					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">CYNARA SCOLYMUS (ARTICHOKE)</h3>
						<p>
							Sebagai agen antimikroba dapat membantu menghambat pertumbuhan bakteri penyebab jerawat.
						</p>
					</div>

					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">CURCUMA LONGA (KUNYIT)</h3>
						<p>
							Sebagai antioksidan, membantu mempercepat penyembuhan luka, sifat anti-inflamasinya membantu terapi pada kondisi kulit berjerawat.
						</p>
					</div>

					<div class="w-100"></div>

					<div class="col-md-12 mb-5">
						<h3 class="h5 font-weight-semi-bold">SILYBUM MARIANUM (MILK THISTLE)  & BRASSICA OLERACEA (BROKOLI)</h3>
						<p>
							Sebagai antioksidan yang membantu menghambat radikal bebas dari paparan sinar UV sehingga kulit tetap terjaga.
						</p>
					</div>

					<div class="col-md-6 mb-5">
						<p>Isi kemasan : 60 Kapsul</p>
						<p>Saran konsumsi : Minum 2 kapsul, 2 kali sehari</p>
					</div>

					<div class="col-md-6 mb-5">
						<p>Takaran Sajian Per Kapsul :</p>
						<p>- Vitex agnus castus fructus 		200 mg </p>
						<p>- Silybum marianum semen		100 mg </p>
						<p>- Cynara scolymus folium 		50 mg</p>
						<p>- Curcuma longa rhizoma 		50 mg </p>
						<p>- Brassica oleracea 			100 mg</p>

					</div>
				</div>
			</div>
			<!-- End Male Optime -->

			<!-- Divider -->
			<div class="w-80 mx-auto">
				<hr class="my-0">
			</div>
			<!-- End Divider -->
		</div>
		<!-- End Male Optime Section -->

	</main>
	<!-- ========== END MAIN ========== -->

	<script type="text/javascript">
		
		$(document).ready(function() {
			let param = "<?php echo $param ?>";
			switch(param) {
			  case 'male_optime':
			  	$('.section_gluta').hide();
			  	$('.section_fesklin').hide();
			    break;
			  case 'gluta':
			  	$('.section_male_optime').hide();
			  	$('.section_fesklin').hide();
			    break;
			  default:
			  	$('.section_gluta').hide();
			  	$('.section_male_optime').hide();
			}
		});

	</script>