
  <!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main">
    <!-- Hero Section -->
    <div class="">
      <div class="container space-2 mt-9">
        <div class="row justify-content-md-between">
          <!-- Item Image -->
          <div class="col-md-6 mb-7 mb-md-0 text-center">
            <img class="img-fluid" id="productImg" src="" alt="Image Description">

            <!-- image item -->
              <div class="js-slick-carousel u-slick appendItemImage"
                   data-slides-show="3"
                   data-slides-scroll="1"
                   data-arrows-classes="d-none d-lg-inline-block u-slick__arrow u-slick__arrow--offset u-slick__arrow-centered--y rounded-circle"
                   data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-inner u-slick__arrow-inner--left"
                   data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-inner u-slick__arrow-inner--right"
                   data-pagi-classes="text-center u-slick__pagination mt-7 mb-0"
                   data-responsive='[{
                     "breakpoint": 992,
                     "settings": {
                       "slidesToShow": 2
                     }
                   }, {
                     "breakpoint": 768,
                     "settings": {
                       "slidesToShow": 1
                     }
                   }, {
                     "breakpoint": 554,
                     "settings": {
                       "slidesToShow": 1
                     }
                   }]'>
                
              </div>
            <!-- end image item -->
          </div>
          <!-- End Item Image -->

          <!-- Content -->
          <div class="col-md-5">
            <div class="mb-4">
              <h1 class="card-text-dark font-weight-normal" id="productName">-</h1>
                <!-- <div class="d-flex justify-content-between col-md-6">
                  <span class="d-block h3 mr-3" id="productHarga">-</span>
                  <span class="d-block h3 mr-3" id="productHargaDiskon"></span>  
                  <div class="h6 discHide"></div>
                </div> -->
              <p class="" id="productDesc">-</p>
            </div>

            <div class="d-flex justify-content-between">
              <label class="h6 small d-block text-uppercase card-text-dark">
                Quantity
              </label>
            </div>

            <div class="d-flex justify-content-left mb-5">
              <div class="js-quantity input-group u-quantity mr-5">
                <input class="js-result form-control u-quantity__input" type="text" id="valQty" value="1">
                <div class="u-quantity__arrows">
                  <span class="js-plus u-quantity__arrows-inner fas fa-angle-up"></span>
                  <span class="js-minus u-quantity__arrows-inner fas fa-angle-down"></span>
                </div>
              </div>

              <label class="h6 small d-block text-secondary card-text-dark mt-3 labelQty">
                -
              </label>
            </div>
          <!-- End Content -->
        </div>
      </div>
    </div>
    <!-- End Hero Section -->

    <div class="container">
        <div class="card">
          <div class="card-body p-4">
            <!-- Nav -->
            <div class="row justify-content-center align-items-center mb-4">

              <div class="col-md-12 order-md-1">
                <!-- Nav Classic -->
                <ul class="nav nav-classic nav-borderless px-0" id="pills-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="pills-one-information" data-toggle="pill" href="#pills-one" role="tab" aria-controls="pills-one" aria-selected="true">Indication</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-two-benefits" data-toggle="pill" href="#pills-two" role="tab" aria-controls="pills-two" aria-selected="false">Packaging</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-three-ingredients" data-toggle="pill" href="#pills-three" role="tab" aria-controls="pills-three" aria-selected="false">Ingredients</a>
                  </li>
                </ul>
                <!-- End Nav Classic -->
              </div>
            </div>
            <!-- End Nav -->

            <!-- Datatable -->
            <div class="tab-content" id="pills-tabContent">
              <!-- Content One -->
              <div class="tab-pane fade show active" id="pills-one" role="tabpanel" aria-labelledby="pills-one-information">
                
                <div class="container textInformation">
                  
                </div>
                
              </div>

              <!-- Content Two -->
              <div class="tab-pane fade" id="pills-two" role="tabpanel" aria-labelledby="pills-two-benefits"> 
                <div class="container textBenefits">
                  
                </div>
              </div>
              <!-- End Content Two -->

              <!-- Content Three -->
              <div class="tab-pane fade" id="pills-three" role="tabpanel" aria-labelledby="pills-three-ingredients">
                <div class="container textIngredients">
                  
                </div>
            </div>
            <!-- End Datatable -->
          </div>
        </div>
      </div>

      <div class="text-left space-1">        
        <h3 class="text-black font-weight-normal h1 ml-2" id="productName2">-</h3>
        <div class="bg-header rounded-pill mt-2 col-md-5">
          <div class="js-slick-carousel u-slick slick-initialized slick-slider" data-infinite="true" data-slides-show="7">
            <div class="slick-list draggable text-center">
              <div class="slick-track">
                <div class="js-slide slick-slide slick-active mr-2" data-slick-index="1" aria-hidden="false" style="width: auto; height: auto;" tabindex="0">
                  <div class="d-flex justify-content-between col-md-12">
                    <span class="text-white text-bold d-block h3 mr-3" id="productHarga">-</span>
                    <span class="text-white text-bold d-block h3 mr-3" id="productHargaDiskon"></span>  
                    <div class="text-white text-bold h6 discHide"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="rounded-pill mt-2 col-md-5 text-center">
          <div class="d-flex justify-content-center">
              <button class="btn btn-soft-facebook transition-3d-hover addTocart mr-2" style="cursor: pointer;">
                <span class="fas fa-cart-arrow-down mr-2"></span>
                Add to Cart
              </button>

              <button class="btn btn-soft-google transition-3d-hover buyNow" style="cursor: pointer;">
                <span class="fas fa-cart-arrow-down mr-2"></span>
                Buy Now
              </button>
          </div>
        </div>
      </div>

      </div>
    </div>

     <!-- Shop Items Section -->
    <div id="shopItemsContent" class="container space-1 space-md-2  text-center">
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto ">
        <h2 class="text-primary"><span class="font-weight-semi-bold ">Product</span></h2>
          <hr class="my-0">
      </div> <!-- Blog Grid Section -->
      <div class="space-1 space-md-2">

          <!-- Content -->
        <div class="cbp appenItem"
             data-layout="grid"
             data-controls="#filterControls"
             data-animation="quicksand"
             data-x-gap="32"
             data-y-gap="32"
             data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 3},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
              {"width": 300, "cols": 1}]'>

        </div>

        <!-- End Shop Items Section -->
      </div>
      <!-- End Blog Grid Section -->

      <button class="btnLoadmore btn text-white btn-primary transition-3d-hover" style="cursor: pointer;">
        Load More
      </button>
    </div>
    <!-- End Shop Items Section -->
  </main>
  <!-- ========== END MAIN CONTENT ========== -->

  <input type="hidden" name="product_uid" id="product_uid">

  <?php $this->load->view('website/product-script'); ?>
  <?php $this->load->view('website/single-product-script'); ?>