<!-- ========== MAIN ========== -->
<main id="content " role="main">
    <!-- Hero Section -->
    <div class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll"
         data-options='{direction: "normal"}'>
	      <!-- Apply your Parallax background image here -->
	          <div class="d-lg-flex align-items-lg-center u-hero-v1__main bg-success">

	      <!-- Content -->
	      <div class="container position-relative space-top-md-5 space-bottom-md-3 z-index-2">
		        <div class="w-lg-80 text-center mx-auto">
		          <h4 class="display-4 font-size-md-down-5 text-white font-weight-semi-bold">Terms & Conditions</h4>
		        </div>
	      </div>
	      <!-- End Content -->
    </div>
    <!-- End Hero Section -->

   
    <!-- Blog Modern Section -->
    <div class="container space-1">
      <div class="row mx-gutters-2 appendItem">
        <ol>
          <li>
            Introduction
            <p>These Website Standard Terms And Conditions (these “Terms” or these “Website Standard Terms And Conditions”) contained herein on this webpage, shall govern your use of this website, including all pages within this website (collectively referred to herein below as this “Website”). These Terms apply in full force and effect to your use of this Website and by using this Website, you expressly accept all terms and conditions contained herein in full. You must not use this Website, if you have any objection to any of these Website Standard Terms And Conditions.This Website is not for use by any minors (defined as those who are not at least 18 years of age), and you must not use this Website if you a minor.</p>
          </li>
          <li>
            Intellectual Property Rights.
            <p>Other than content you own, which you may have opted to include on this Website, under these Terms, [COMPANY NAME] and/or its licensors own all rights to the intellectual property and material contained in this Website, and all such rights are reserved. <br>
            You are granted a limited license only, subject to the restrictions provided in these Terms, for purposes of viewing the material contained on this Website.</p>
          </li>

          <li>
            Restrictions
            <p>You are expressly and emphatically restricted from all of the following:</p>
            <ol class="text-muted">
              <li>
                using this Website in any way that is, or may be, damaging to this Website;
              </li>
              <li>
                using this Website contrary to applicable laws and regulations, or in a way that causes, or may cause, harm to the Website, or to any person or business entity;
              </li>
              <li>
                engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website, or while using this Website;
              </li>
            </ol>
          </li>
          
          <li>
            No warranties.
            <p>This Website is provided “as is,” with all faults, and Prime Aesthetic Pte Ltd makes no express or implied representations or warranties, of any kind related to this Website or the materials contained on this Website. Additionally, nothing contained on this Website shall be construed as providing consult or advice to you.</p>
          </li>
          <li>
            Limitation of liability.
            <p>In no event shall Prime Aesthetic Pte Ltd, nor any of its officers, directors and employees, be liable to you for anything arising out of or in any way connected with your use of this Website, whether such liability is under contract, tort or otherwise, and Prime Aesthetic Pte Ltd, including its officers, directors and employees shall not be liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.</p>
          </li>
          <li>
            Indemnification.
            <p>You hereby indemnify to the fullest extent Prime Aesthetic Pte Ltd from and against any and all liabilities, costs, demands, causes of action, damages and expenses (including reasonable attorney’s fees) arising out of or in any way related to your breach of any of the provisions of these Terms.
            Severability.<br>
            If any provision of these Terms is found to be unenforceable or invalid under any applicable law, such unenforceability or invalidity shall not render these Terms unenforceable or invalid as a whole, and such provisions shall be deleted without affecting the remaining provisions herein.</p>
          </li>
          <li>
            Variation of Terms.
            <p>Prime Aesthetic Pte Ltd is permitted to revise these Terms at any time as it sees fit, and by using this Website you are expected to review such Terms on a regular basis to ensure you understand all terms and conditions governing use of this Website.</p>
          </li>
          <li>
            Assignment.
            <p>Prime Aesthetic Pte Ltd shall be permitted to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification or consent required. However, you shall not be permitted to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.</p>
          </li>
          <li>
            Entire Agreement.
            <p>These Terms, including any legal notices and disclaimers contained on this Website, constitute the entire agreement between Prime Aesthetic Pte Ltd and you in relation to your use of this Website, and supersede all prior agreements and understandings with respect to the same.</p>
          </li>
          <li>
            Governing Law & Jurisdiction.
            <p>These Terms will be governed by and construed in accordance with the laws of Singapore, and you submit to the non-exclusive jurisdiction of the aforesaid for the resolution of any disputes.</p>
          </li>
        </ol> 
      </div>
    </div>
    <!-- End Blog Modern Section -->
</main>
<!-- ========== END MAIN ========== -->