
  
  <!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main">
    <!-- Hero Section -->
    <div class="position-relative overflow-hidden space-3">
      <!-- Item Image -->
      <div class="col-md-6 position-md-absolute top-md-0 left-md-0 content-centered-y--md pl-0 mb-3 mb-md-0 text-center">
        <img class="img-fluid productImage" src="<?php echo base_url('assets/img/others/bg_product.png'); ?>" alt="Image Description">
      </div>
      <!-- End Item Image -->

      <!-- Content -->
      <div class="container space-1 mt-10">
        <div class="row justify-content-md-end">
          <div class="col-md-6">
            <div class="mb-5">
              <h1 class="display-4 font-size-md-down-5 text-primary font-weight-semi-bold productTitle">Our Product</h1>
              <span class="d-block lead text-black mb-2 productDesc">We offer our range off products that are "Made to Quality, Purity and Potency Standars"</span>
            </div>
            <a class="js-go-to btn text-white btn-primary transition-3d-hover" href="#"
               data-target="#shopItemsContent"
               data-type="static">
              Start Shopping
            </a>
          </div>
        </div>
      </div>
      <!-- End Content -->
    </div>
    <!-- End Hero Section -->

    <!-- Shop Items Section -->
    <div id="shopItemsContent" class="container space-1 text-center oProducts">
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto ">
        <h2 class="text-primary"><span class="font-weight-semi-bold ">Our Product</span></h2>
          <hr class="my-0">
      </div> <!-- Blog Grid Section -->
      <div class="space-1 space-md-2">
          <div class="cbp appentProduct"
               data-layout="grid"
               data-controls="#filterControls"
               data-animation="quicksand"
               data-x-gap="32"
               data-y-gap="32"
               data-media-queries='[
                {"width": 1500, "cols": 4},
                {"width": 1100, "cols": 3},
                {"width": 800, "cols": 3},
                {"width": 480, "cols": 2},
                {"width": 300, "cols": 1}]'>
          </div>
      </div>
      <!-- End Blog Grid Section -->
    </div>
    <!-- End Shop Items Section -->

    <!-- Shop Items Section -->
    <div id="shopItemsContent" class="container space-1 space-md-2  text-center">
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto ">
        <h2 class="text-primary"><span class="font-weight-semi-bold ">Hot Product</span></h2>
          <hr class="my-0">
      </div> <!-- Blog Grid Section -->
      <div class="space-1 space-md-2">

          <!-- Content -->
        <div class="cbp appenItem"
             data-layout="grid"
             data-controls="#filterControls"
             data-animation="quicksand"
             data-x-gap="32"
             data-y-gap="32"
             data-media-queries='[
              {"width": 1500, "cols": 4},
              {"width": 1100, "cols": 3},
              {"width": 800, "cols": 3},
              {"width": 480, "cols": 2},
              {"width": 300, "cols": 1}]'>

        </div>

        <div class="notfound">
          
        </div>
        <!-- End Shop Items Section -->
      </div>
      <!-- End Blog Grid Section -->

      <button class="btnLoadmore btn text-white btn-primary transition-3d-hover" style="cursor: pointer;">
        Load More
      </button>
    </div>
    <!-- End Shop Items Section -->

  </main>
  <!-- ========== END MAIN CONTENT ========== -->

  <?php $this->load->view('website/product-script'); ?>
