<script type="text/javascript">
	$(document).ready(function() {
		let form = $('#form');
		var url = {
			getPromo: "<?php echo $this->config->item('api_uri').'/v1/promo/detail?uid='.$uid.'' ?>",
		};

		$(document).ready(function() {
	        $.ajax({
	          	url: url.getPromo,
	          	type: 'GET',
	          	dataType: 'json',
	          	success: function(res) { 
	          		
	          		let urlImage = '<?php echo $this->config->item('api_base_uri')?>'+'/uploads/promo/'+res.data.banner_id;
	          		$('body').find('#headerbanner').css('background-image', 'url(' + urlImage + ')');
	          		$('#titlePromo').html(res.data.nama_barang);
	          		$('#titleDesc').html(res.data.description);
	          		$('#titleDate').html(res.data.expired_at);
	          		$('#titlelongDesc').html(res.data.long_description);
					
					if (res.data.foto.length > 0) {
			    		for (var i = 0; i < res.data.foto.length; i++) {
							fillFoto(res.data.foto[i], i);
						}
			    	}
        			
        			$.HSCore.components.HSSlickCarousel.init('.js-slick-carousel2');
	          	},
	          	error: function() { 
	          		console.log('aa');
	          	},
	          	beforeSend: setHeader
	        });
	    });

	    function setHeader(xhr) {
	        xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
	    }

	    function fillFoto(data, index){
	    	let appentHtml = '';

	    	if (data) {
	    		appentHtml += '<div class="js-slide rounded bg-img-hero min-height-300" style="background-image: url('+data.original+');"></div>';
	    	}
			$('.appendItem').append(appentHtml);			
	    }

	});
</script>