<script type="text/javascript">
	$(document).ready(function() {
		let form = $('#form');
		let stock = 0;
		var url = {
			getDetailProduct: "<?php echo $this->config->item('api_uri').'/v1/gudang_farmasi/barang/detail?uid='.$uid.''?>",
			addTocart: "<?php echo $this->config->item('api_uri').'/v1/transaksi/add_tocart/insert' ?>",
		};
		$(document).ready(function() {
			$.ajax({
				url: url.getDetailProduct,
				type: 'GET',
				dataType: 'json',
				success: function(res) { 
					data = res.data;
					stock = data.stock;
					$('#product_uid').val(data.uid);
					$('#productName').html(data.nama);
					$('#productName2').html(data.nama);
					$('#productDesc').html(data.deskripsi);
					$('.labelQty').html('tersisa '+ data.stock +' buah.');
					$('#productImg').attr('src', data.foto.length > 0 ? data.foto[0].original : "<?php echo base_url('assets/img/others/no_image_available.png'); ?>");
					$('.textInformation').html(data.deskripsi_information);
					$('.textBenefits').html(data.deskripsi_benefits);
					$('.textIngredients').html(data.deskripsi_ingredients);


					if (data.foto.length > 0) {
						for (var i = 0; i < data.foto.length; i++) {
							fillFoto(data.foto[i], i, data.nama);
						}
					}
					
					$('#productHarga').html('Rp. '+numeral(data.harga).format());
					$('.discHide').html('');
					if (data.diskon > 0 ) {
						$('#productHarga').css("text-decoration", "line-through");
						$('#productHarga').css("font-size", "18px");
						$('#productHargaDiskon').css("font-weight", "bold");
						$('#productHargaDiskon').css("font-weight", "bold");

						diskon = parseInt(data.harga) * parseInt(data.diskon) / 100;
						totalDiskon = parseInt(data.harga) - diskon;
						$('.discHide').html('<span class="badge badge-danger labelDiskon">'+numeral(data.diskon).format('00') +' % OFF </span>');
						$('#productHargaDiskon').html('Rp. '+numeral(totalDiskon).format());
					}

					
				},
				error: function() { 
					console.log('aa');
				},
				beforeSend: setHeader
			});
		});

		function setHeader(xhr) {
			xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
		}

		function fillFoto(data, index, nama){
			let appentHtml = '';

			if (data) {
				appentHtml += '<a class="js-slide bg-img-hero"'
				+'href="javascript:;"'
				+'data-src="'+ (data ? data.original : '') +'"'
				+'data-fancybox="lightbox-gallery-hidden"'
				+'data-caption="Product '+ nama +'"'
				+'data-speed="700">'
				+'<label class="u-lg-avatar mr-2" style="cursor:pointer">'
				+'<img class="img-fluid " src="'+ (data ? data.original : '') +'" id="productImg" alt="Image Description">'
				+'</label>'
				+'</a>';
			}
			$('.appendItemImage').append(appentHtml);			
		}

		$('.addTocart').click(function(e){
			if (userdata) {
				let member_id = $('#member_id').val(),
				product_uid = $('#product_uid').val(),
				qty = $('#valQty').val();

				blockPage('Sedang diproses ...');
				$.ajax({
					data: {
						member_id: member_id, 
						product_uid: product_uid,
						qty: qty,
					},
					type: 'POST',
					dataType: 'JSON', 
					url: url.addTocart,
					headers: {
						"Authorization": "PRIMA-uvXL68GB5THBN8cUIFuM"
					},
					success: function(data){
						$.unblockUI();

						$('#AlertModal').modal('show');
						$('#dispNotif').html(data.message);

						setTimeout(function () {
							$('#AlertModal').modal('hide');
							fillCart();
						}, 3000);
					},
					error: function(data){
						$.unblockUI();
						$('#AlertModal').modal('show');
						$('#dispNotif').html(data.responseJSON.message);
						setTimeout(function () {
                  			fillCart();
							$('#AlertModal').modal('hide');
						}, 2000);
					}
				});
			}else{
				$('#AlertModal').modal('show');
				$('#dispNotif').html('Silahkan login atau pesan dari Whatsapp');
				setTimeout(function () {
					$('#AlertModal').modal('hide');
					e.stopPropagation();
					$('#sidebarNavToggler').click();
				}, 2000);
			}
		});

		$('.buyNow').click(function(e){
			if (userdata) {
				let member_id = $('#member_id').val(),
				product_uid = $('#product_uid').val(),
				qty = $('#valQty').val();

				blockPage('Sedang diproses ...');
				$.ajax({
					data: {
						member_id: member_id, 
						product_uid: product_uid,
						qty: qty,
					},
					type: 'POST',
					dataType: 'JSON', 
					url: url.addTocart,
					headers: {
						"Authorization": "PRIMA-uvXL68GB5THBN8cUIFuM"
					},
					success: function(data){
						$.unblockUI();

						setTimeout(function () {
							$('#AlertModal').modal('hide');
							window.location.href = "<?php echo site_url('cart') ?>";
						}, 1000);
					},
					error: function(data){
						$.unblockUI();
						$('#AlertModal').modal('show');
						$('#dispNotif').html(data.responseJSON.message);
						setTimeout(function () {
                  			fillCart();
							$('#AlertModal').modal('hide');
						}, 2000);
					}
				});
			}else{

				$('#AlertModal').modal('show');
				$('#dispNotif').html('Silahkan login atau pesan dari Whatsapp');
				setTimeout(function () {
					$('#AlertModal').modal('hide');
					e.stopPropagation();
					$('#sidebarNavToggler').click();
				}, 2000);

			}
		});

		$('.js-plus').on('click', function(){
			qty = $('#valQty');

			if (qty.val() > stock) {
				$('#AlertModal').modal('show');
				$('#dispNotif').html('Pemesanan barang melebihi stock yang tersedia.');
				qty.val(stock);
			}
		})
	});
</script>