<script type="text/javascript">
	$(document).ready(function() {
		let term = '<?php echo $dHide?>';
		if (userdata && term == 'nothide') {
		    $('.oProducts').css("display", "block");
		}else{
		    $('.oProducts').css("display", "none");
		}

		if (userdata) {
			dataUrl = {
			        'limit' : 6,
			        'offset' : 0,
				      };
	    }else{
	      dataUrl = {
	        'bpom' : 1,
	      };
	      $('.sideProfile').css("display", "none");
	      $('.btnProfile2').css("display", "none");
	    }
		//list barang
		fillBarangProduct(dataUrl);

		$('.btnLoadmore').on('click', function(){
			$('.appenItem').append('');
			$('.appenItem').cubeportfolio('destroy');
			$('.appentProduct').cubeportfolio('destroy');

 			blockPage('Loading ...');
			var page = offset += 1;
			$(this).append(' <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>')
			$(this).attr('disabled', true);

			setTimeout(()=>{
				$(this).find('span').remove();
				$(this).attr('disabled', false);
		        dataUrl = {
			        'limit' : 6,
			        'offset' : page,
			    };

		        $.unblockUI();

				fillBarangProduct(dataUrl);
				$('.itemList').css("border", "groove");
		    },1500);
		});

		$.ajax({
          	url: url.getKelompokbarang,
          	type: 'GET',
          	dataType: 'json',
          	success: function(res) { 
          		if (res.list.length > 0) {
		    		for (var i = 0; i < res.list.length; i++) {
						fillKelompokBarang(res.list[i], i);
					}
		    	}else{
		    		$('.appentProduct').addClass('text-center');
					$('.appentProduct').append('<div class="col-md-12 space-1">'
											  +'<span class="u-divider u-divider--text text-dark">Tidak ada product. </span>'
											+'</div>');
		    	}

			    // initialization of cubeportfolio

          	},
          	error: function() { 
          		console.log('error');
          	},
          	beforeSend: setHeader
        });

        
        setTimeout(()=>{
        	$.HSCore.components.HSCubeportfolio.init('.cbp');
	    },3000);
	});

	function fillKelompokBarang(data, index){
    	let appentHtml = '';
    	
		appentHtml += '<div class="cbp-item rounded graphic itemList" style="border:groove">'
					      +'<a class="cbp-caption" data-id="'+data.id+'" href="<?php echo site_url('product') ?>'+'?uid='+data.uid+'">'
					        +'<div class="cbp-caption-defaultWrap">'
					          +'<img src="'+ (data.foto_id ? url.getImageKelompokBarang +  data.foto_id : "<?php echo base_url('assets/img/others/no_image_available.png'); ?>" ) +'" >'
					        +'</div>'
					        +'<div class="cbp-caption-activeWrap bg-header">'
					          +'<div class="cbp-l-caption-alignCenter">'
					            +'<div class="cbp-l-caption-body">'
					              +'<h4 class="h6 text-white mb-0">'+ data.nama +'</h4>'
					              +'<p class="small text-white-70 mb-0">'
					                +data.deskripsi
					              +'</p>'
					            +'</div>'
					          +'</div>'
					        +'</div>'
					      +'</a>'
					    +'</div>';
	    	
		$('.appentProduct').append(appentHtml);
    }
</script>