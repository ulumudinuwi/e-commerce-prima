<script type="text/javascript">
	$(document).ready(function() {
		let form = $('#form');
		var url = {
			getPromo: "<?php echo $this->config->item('api_uri').'/v1/promo/list' ?>",
		};

		$(document).ready(function() {
	        $.ajax({
	          	url: url.getPromo,
	          	type: 'GET',
	          	dataType: 'json',
	          	success: function(res) { 
	          		if (res.list.length > 0) {
			    		for (var i = 0; i < res.list.length; i++) {
							fillPromo(res.list[i], i);
						}
			    	}else{
			    		$('.appendItem').addClass('text-center');
						$('.appendItem').append('<div class="col-md-12 space-3">'
												  +'<span class="u-divider u-divider--text text-dark">Tidak ada Promo tersedia. </span>'
												+'</div>');
			    	}
	          	},
	          	error: function() { 
	          		console.log('aa');
	          	},
	          	beforeSend: setHeader
	        });
	    });

	    function setHeader(xhr) {
	        xhr.setRequestHeader('Authorization', 'PRIMA-uvXL68GB5THBN8cUIFuM');
	    }

	    function fillPromo(data, index){
	    	let appentHtml = '',
	    		appentHtmlNews = '',
		    	uriUpload = "<?php echo $this->config->item('api_base_uri')?>";

	    	if (data.type == 1) {
	    		appentHtml += '<article class="border-0 shadow-sm mb-3 col-md-4">'
				              +'<div class="card-body p-5">'
				                +'<small class="d-block text-muted mb-2">s/d '+data.expired_at+'</small>'
				                +'<h2 class="h5 font-weight-bold">'
				                  +'<a href="<?php echo site_url('news_promo/detail_promo') ?>'+'/'+data.uid+'">'+data.nama_barang+'</a>'
				                +'</h2>'
				                +'<p class="mb-0 text-muted">'+data.description+'</p>'
				              +'</div>'
				              +'<div class="card-footer pb-5 px-0 mx-5">'
				                +'<div class="media align-items-center">'
				                  +'<div class="u-sidebar--account__holder-img mr-3">'
				                    +'<img class="img-fluid rounded" src="'+(data.foto.length > 0 ? data.foto[0].original : "<?php echo base_url('assets/img/others/no_image_available.png'); ?>" ) +'" alt="Image Description">'
				                  +'</div>'
				                  +'<div class="media-body">'
				                    +'<h4 class="small mb-0">'+data.nama_barang+'</h4>'
				                  +'</div>'
				                +'</div>'
				              +'</div>'
				            +'</article>';

				$('.appendItem').append(appentHtml);
	    	}

	    	if (data.type == 2){
	    		appentHtmlNews += '<article class="card border-0 mb-5">'
					                +'<div class="card-body p-0">'
					                  +'<div class="media">'
					                    +'<div class="u-avatar mr-3">'
				                  		+'<a href="<?php echo site_url('news_promo/detail_promo') ?>'+'/'+data.uid+'">'
				                    	+'<img class="img-fluid rounded" src="'+(data.foto.length > 0 ? data.foto[0].original : "<?php echo base_url('assets/img/others/no_image_available.png'); ?>" ) +'" alt="Image Description">'
				                    	+'</a>'
					                    +'</div>'
					                    +'<div class="media-body">'
					                      +'<h4 class="h6 font-weight-normal mb-0">'
				                  			+'<a href="<?php echo site_url('news_promo/detail_promo') ?>'+'/'+data.uid+'">'+data.nama_barang+'</a>'
					                      +'</h4>'
					                      +'<h4 class="h6 font-weight-normal mb-0">'
					                      	+data.description
					                      +'</h4>'
					                    +'</div>'
					                  +'</div>'
					                +'</div>'
					              +'</article>';

				$('.appendItemNews').append(appentHtmlNews);
	    	}
	    }

	});
</script>