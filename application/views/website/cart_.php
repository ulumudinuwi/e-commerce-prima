
<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main">
  <!-- Cart Section -->
  <div class="bg-light">
    <div class="container space-3">
      <!-- Title -->
      <div class="mb-6">
        <h1 class="h2 font-weight-normal mb-1" id="labelCart">Your cart</h1>
        <div class="media checkoutAddress">
          <figure id="icon44" class="ie-height-56 w-100 max-width-8 mt-2 mr-4" style="">
            <img class="js-svg-injector" src="<?php echo assets_url()?>/svg/icons/icon-8.svg" alt="SVG" data-parent="#icon8">
          </figure>
          <div class="media-body">
            <h4 class="h6">Shipping address . </h4>
            <p class="mb-0" id="alamatPengiriman">-</p>
          </div>
        </div>
      </div>
      <!-- End Title -->

      <form>
        <!-- Table Content -->
        <div class="table-responsive-sm">
          <table class="table " id="tableCart">
            <thead  class="thead-dark">
              <tr>
                <th class="text-center">
                  <input class="form-check-input checkAll" type="checkbox" value="" id="checkAll">
                </th>
                <th class="text-center">Product</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Price (IDR)</th>
                <th class="text-center">Total Price (IDR)</th>
                <th class="text-center"></th>
              </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>

            </tfoot>
          </table>
          <hr class="my-0">
        </div>
        <!-- End Table Content -->

        <!-- Table Content -->
        <div class="table-responsive-sm">
          <table class="table" id="tableCheckout">
            <thead  class="thead-dark">
              <tr>
                <th class="text-center">Product</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Price (IDR)</th>
                <th class="text-center">Total Price (IDR)</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>

            </tfoot>
          </table>
          <hr class="my-0">
        </div>
        <!-- End Table Content -->
      </form>
      
    </div>
  </div>


  <!-- Shop Items Section -->
  <!-- <div class="container text-center ">
    <div class="mb-6">
      <h2 class="font-weight-normal">Related Products</h2>
    </div>

    <div class="row appenItem">

    </div>

    <button class="btnLoadmore btn text-white btn-primary transition-3d-hover" style="cursor: pointer;">
      Load More
    </button>
  </div> -->
  <!-- End Shop Items Section -->
</div>
<!-- End Cart Section -->

</main>
<!-- ========== END MAIN CONTENT ========== -->
