<div id="konfirmasi-pesanan-diterima-modal" class="modal fade"tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body form-horizontal">
        <div class="row mb-20">
          <div class="col-md-12">
            <h3 class="text-bold">Konfirmasi Penerimaan Pesanan</h3>
            <div class="row">
              <div class="col-md-12 text-muted text-secondary">Saya telah memastikan bahwa pesanan saya terima dan tidak ada masalah. Saya tidak dapat mengajukan pengembalian barang atau dana setelah saya mengkonfirmasi</div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer m-t-none">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-danger btn-konfirmasi_pesanan_diterima">
          Konfirmasi
        </button>
      </div>
    </div>
  </div>
</div>