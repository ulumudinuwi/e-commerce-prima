<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->language('welcome');
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->data['page_title'] = 'Product';
		$uid = $this->input->get('uid') ? : '';
		$term = $this->input->get('term') ? : '';
		$this->data['dHide'] = 'nothide';

		$url = $this->config->item('api_uri').'/v1/gudang_farmasi/barang/list';

		if ($uid) {
			$url = $this->config->item('api_uri').'/v1/gudang_farmasi/barang/list?group_uid='.$uid;
		}
		if ($term) {
			$url = $this->config->item('api_uri').'/v1/gudang_farmasi/barang/list?term='.$term;
			$this->data['dHide'] = "hide";
		}

		$this->data['url'] = $url;

		$this->template->
						build('website/product', $this->data);
	}

	public function detail($uid)
	{
		$term = $this->input->get('term') ? : '';
		$this->data['dHide'] = 'nothide';

		$url = $this->config->item('api_uri').'/v1/gudang_farmasi/barang/list';

		if ($uid) {
			$url = $this->config->item('api_uri').'/v1/gudang_farmasi/barang/list?group_uid='.$uid;
		}
		if ($term) {
			$url = $this->config->item('api_uri').'/v1/gudang_farmasi/barang/list?term='.$term;
			$this->data['dHide'] = "hide";
		}

		$this->data['page_title'] = 'Detail Product';
		$this->data['uid'] = $uid;
		$this->template->set_script('website/product-script')
						->build('website/single-product', $this->data);
	}

	public function explore_product($param)
	{
		switch ($param) {
			case 'gluta':
				$page_title = 'GLUTA OPTIME';	
				# code...
				break;
	
			case 'male_optime':
				$page_title = 'MALE OPTIME';
				break;
			
			default:
				$page_title = 'FESKLIN';	
				break;
		}
		$this->data['page_title'] = $page_title;
		$this->data['param'] = $param;
		$this->template->build('website/explore_product', $this->data);
	}

}
