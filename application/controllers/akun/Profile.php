<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->data['page_title'] = 'Profil';
		$this->template
            ->set_js('notifications/bootbox.min', FALSE)
            ->set_js('notifications/sweet_alert.min', FALSE)
            ->build('website/akun/profile', $this->data);
	}
	
	public function alamat()
	{
		$this->data['page_title'] = 'Alamat';
		
		$response = $this->getProvince();
		
		$this->data['data'] = json_decode($response, true);

		$this->template
            ->set_js('notifications/bootbox.min', FALSE)
            ->set_js('notifications/sweet_alert.min', FALSE)
            ->build('website/akun/alamat', $this->data);
	}

	public function atur_password()
	{
		$this->data['page_title'] = 'Atur Password';
		$this->template
            ->set_js('notifications/bootbox.min', FALSE)
            ->set_js('notifications/sweet_alert.min', FALSE)
            ->build('website/akun/atur_password', $this->data);
	}

	public function pesanan()
	{
		$this->data['page_title'] = 'Pesanan';
		$this->template
            ->set_js('notifications/bootbox.min', FALSE)
            ->set_js('notifications/sweet_alert.min', FALSE)
            ->build('website/akun/pesanan', $this->data);
	}

	public function getProvince(){
		$prov_id = $this->input->get('province');

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=$prov_id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: 2a2bd1917c36ad63ca43b88a0bf741e2"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		if (!$this->input->is_ajax_request()){
			return $response;
		} else echo $response;
		
	}

	public function getKab(){
		$prov_id = $this->input->get('prov_id');

		$curl = curl_init();	
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://pro.rajaongkir.com/api/city?province=$prov_id",
 
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: 2a2bd1917c36ad63ca43b88a0bf741e2"
		  ),
		));
	 
		$response = curl_exec($curl);
		$err = curl_error($curl);
	 
		if (!$this->input->is_ajax_request()){
			return $response;
		} else echo $response;
		
	}

	public function getKota(){
		$city_id = $this->input->get('city_id');

		$curl = curl_init();	
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?city=$city_id",
 
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: 2a2bd1917c36ad63ca43b88a0bf741e2"
		  ),
		));
	 
		$response = curl_exec($curl);
		$err = curl_error($curl);
	 
		if (!$this->input->is_ajax_request()){
			return $response;
		} else echo $response;
		
	}

	public function getCost(){
		$origin = $this->input->post('origin');
		$originType = $this->input->post('originType');
		$destination = $this->input->post('destination');
		$destinationType = $this->input->post('destinationType');
		$courier = $this->input->post('courier');
		$weight = $this->input->post('weight');

		$curl = curl_init();	
		
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
 
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "origin=$origin&originType=$originType&destination=$destination&destinationType=$destinationType&weight=$weight&courier=$courier",
		  CURLOPT_HTTPHEADER => array(
		    "key: 2a2bd1917c36ad63ca43b88a0bf741e2"
		  ),
		));
	 
		$response = curl_exec($curl);
		$err = curl_error($curl);
	 
		if (!$this->input->is_ajax_request()){
			return $response;
		} else echo $response;
		
	}

	public function getWaybill(){
		$courier = $this->input->post('courier');
		$no_resi = $this->input->post('no_resi');

		$curl = curl_init();	

		curl_setopt_array($curl, array(
		  	CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
		  	CURLOPT_RETURNTRANSFER => true,
		  	CURLOPT_ENCODING => "",
		  	CURLOPT_MAXREDIRS => 10,
		  	CURLOPT_TIMEOUT => 30,
		  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  	CURLOPT_CUSTOMREQUEST => "POST",
		  	CURLOPT_POSTFIELDS => "waybill=".$no_resi."&courier=".$courier."",
		  	CURLOPT_HTTPHEADER => array(
		    	"key: 2a2bd1917c36ad63ca43b88a0bf741e2"
		  	),
		));
	 
		$response = curl_exec($curl);
		$err = curl_error($curl);
	 
		if (!$this->input->is_ajax_request()){
			return $response;
		} else echo $response;
		
	}

}
